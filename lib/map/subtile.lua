 local anim8 = require("lib.anim8")

 local subtile = {}

 subtile.newGrid = function(...)
 return anim8.newGrid(...) end

 subtile.square4 = function(sample_grid, x, y)
 return sample_grid(x, y, x, (1 + y), (x + 1), y, (1 + x), (1 + y)) end

 subtile.square16 = function(sample_grid, x, y)
 local _local_0_ = {(x + 2), (y + 0)} local a = _local_0_[1] local b = _local_0_[2]
 local _local_1_ = {(x + 4), (y + 0)} local c = _local_1_[1] local d = _local_1_[2]
 local _local_2_ = {(x + 6), (y + 0)} local e = _local_2_[1] local f = _local_2_[2]
 return sample_grid(x, y, x, (1 + y), (x + 1), y, (1 + x), (1 + y), a, b, a, (1 + b), (a + 1), b, (1 + a), (1 + b), c, d, c, (1 + d), (c + 1), d, (1 + c), (1 + d), e, f, e, (1 + f), (e + 1), f, (1 + e), (1 + f)) end




 subtile.square10 = function(sample_grid, x, y)
 local _local_0_ = {(x + 2), (y + 0)} local a = _local_0_[1] local b = _local_0_[2]
 return sample_grid(x, y, x, (1 + y), (1 + x), y, (1 + x), (1 + y), a, b, a, (1 + b), a, (2 + b), (1 + a), b, (1 + a), (1 + b), (1 + a), (2 + b), (2 + a), b, (2 + a), (1 + b), (2 + a), (2 + b)) end




 subtile.rect = function(sample_grid, x, y, w, h)
 local squares = {}
 for i = 0, (w - 1) do
 for j = 0, (h - 1) do
 squares[(1 + #squares)] = (x + i)
 squares[(1 + #squares)] = (y + j) end end
 return sample_grid(unpack(squares)) end


 subtile.batch = function(batchSprite, size, x, y, tile_table, _0_0, _3foffset_x) local _arg_0_ = _0_0 local tr = _arg_0_[1] local tl = _arg_0_[2] local br = _arg_0_[3] local bl = _arg_0_[4]
 local offset_x = (_3foffset_x or 0)
 batchSprite:add(tile_table[tr], (offset_x + ((x * 2) * size)), ((y * 2) * size))

 batchSprite:add(tile_table[tl], (offset_x + (((x * 2) + 1) * size)), ((y * 2) * size))

 batchSprite:add(tile_table[br], (offset_x + ((x * 2) * size)), (((y * 2) + 1) * size))

 return batchSprite:add(tile_table[bl], (offset_x + (((x * 2) + 1) * size)), (((y * 2) + 1) * size)) end


 subtile["batch-rect"] = function(batchSprite, size, x, y, tile_table, w, h, map_width, _3foffset_x)
 local offset_x = (_3foffset_x or 0) local k = 0

 local xp = x
 for i = 0, (w - 1) do
 for j = 0, (h - 1) do
 k = (1 + k)
 if (((x * 2) + i) >= (map_width * 2)) then
 xp = (x - map_width) elseif (((x * 2) + i) < 0) then

 xp = (map_width - x) else
 xp = x end
 batchSprite:add(tile_table[k], (offset_x + (((xp * 2) + i) * size)), (((y * 2) + j) * size)) end end return nil end


 subtile.fixed = function(...)
 return {1, 3, 2, 4} end

 subtile.fence = function(type, neigh)
 local sel
 local function _1_(x, y) local xp if (x == type) then xp = 1 else xp = 0 end local yp
 if (y == type) then yp = 1 else yp = 0 end
 return (1 + xp + (2 * yp)) end sel = _1_

 local a = ({1, 1, 5, 5})[sel(neigh.up, neigh.left)]

 local b = ({3, 3, 7, 7})[sel(neigh.up, neigh.right)]

 local c = ({2, 10, 14, 6})[sel(neigh.down, neigh.left)]

 local d = ({4, 12, 16, 8})[sel(neigh.down, neigh.right)]
 return {a, b, c, d} end


 subtile.blob = function(type, neigh)
 local sel
 local function _1_(x, y, z)
 local xp if ((x == type) or (x == "edge")) then xp = 1 else xp = 0 end local yp
 if ((y == type) or (y == "edge")) then yp = 1 else yp = 0 end local zp
 if ((z == type) or (z == "edge")) then zp = 1 else zp = 0 end
 return (1 + xp + (2 * yp) + (4 * zp)) end sel = _1_

 local a = ({5, 6, 5, 6, 8, 4, 8, 9})[sel(neigh.up, neigh["up-left"], neigh.left)]

 local b = ({11, 12, 11, 12, 8, 2, 8, 9})[sel(neigh.up, neigh["up-right"], neigh.right)]

 local c = ({7, 6, 7, 6, 10, 3, 10, 9})[sel(neigh.down, neigh["down-left"], neigh.left)]

 local d = ({13, 12, 13, 12, 10, 1, 10, 9})[sel(neigh.down, neigh["down-right"], neigh.right)]
 return {a, b, c, d} end



 subtile["neighbour-options"] = function()
 local tab = {}
 for i = 0, 255 do
 tab[i] = {}
 for key, value in ipairs({"right", "left", "up", "down", "up-right", "up-left", "down-right", "down-left"}) do




 tab[i][value] = (math.floor((i / math.pow(2, (key - 1)))) % 2) end end

 return tab end

 return subtile
