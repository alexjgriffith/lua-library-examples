[{:layer-name :ground
  :tiles {:earth {:type :subtile-square4 :grid :tiles :x 0 :y 0}
          :water {:type :subtile-square10 :grid :tiles :x 1 :y 0}
          :field {:type :subtile-square10 :grid :tiles :x 6 :y 0}
          :forest {:type :subtile-square10 :grid :tiles :x 11 :y 0}
          :mountain {:type :subtile-square10 :grid :tiles :x 16 :y 0}
          }
 :dense [[00000]
         [00000]
         [00000]
         [00000]
         [00000]]}
 {:layer :player
  :sparse [{:x 1
            :y 1
            :name :player
            :info {:text "any startup info for player"}}]
  }]


;; Dealing with tiles
(fn tile-four-brush [batchSprite quads tile-size]
  (let [half-size (/ tile-size 2)]
    (fn [[tr tl br bl] i j]
      (let [x (* (- i 1) tile-size)
            y (* (- j 1) tile-size)]
       (: batchSprite :add (. quads tr) x y )
       (: batchSprite :add (. quads tl) (+ x half-size) y)
       (: batchSprite :add (. quads br) x (+ y half-size))
       (: batchSprite :add (. quads bl) (+ x half-size) (+ y half-size))
      ))))

(fn dense-blob [dense]
  (let [h (# dense )
        w (# (. dense 1))
        col []]
    (for [i 1 w]
      (tset col i [])
      (for [j 1 h]
        (let [type       (. dense j          i)
              sel (fn [x y z]
                    (let [xp (if (or (= x type) (= x nil)) 1 0)
                          yp (if (or (= y type) (= y nil)) 1 0)
                          zp (if (or (= z type) (= z nil)) 1 0)]
                      (+ 1 xp (* 2 yp) (* 4 zp))))
              up-right   (?. dense (- j 1) (- i 1))
              up         (?. dense (- j 1)    i)
              up-left    (?. dense (- j 1) (+ i 1))
              left       (?. dense j       (+ i 1))
              down-left  (?. dense (+ j 1) (+ i 1))
              down       (?. dense (+ j 1)    i)
              down-right (?. dense (+ j 1) (- i 1))
              right      (?. dense j       (- i 1))
              a (. [5 6 5 6 8 4 8 9] (sel up up-left left))
              b (. [11 12 11 12 8 2 8 9] (sel up up-right right))
              c (. [7 6 7 6 10  3 10 9] (sel down down-left left))
              d (. [13 12 13 12 10 1 10 9] (sel down down-right right))
              ])
        (tset (. col i) j [a b c d])))
    col))

(fn dense-render [dense col callback-array]
    (let [h (# dense )
        w (# (. dense 1))]
      (for [i 1 w]
        (for [j 1 h]
          (let [index (. dense i j)
                array (. col i j)]
            ((. callback-array index) array i j))))))

{:mousepressed
 :mousereleased
 :draw
 :update}
