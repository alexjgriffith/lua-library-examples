(local noisemap {:_LICENCE "MIT"
                 :_VERSION "0.1.0"
                 :_AUTHOR "Alexander Griffith"
                 :_URL "https://gitlab.com/alexjgriffith/love-libs"
                 :_DESCRIPTION:"Generate a perlin noise map in love."})

(fn hex-to-value [hex]
  (var ret 0)
  (let [len (string.len hex)]
    (for [i 1 len]
      (let [single (string.lower (string.sub hex i i))
            value (match single
                    "0" 0 "1" 1 "2" 2 "3" 3 "4" 4 "5" 5 "6"6 "7" 7 "8" 8 "9" 9
                    "a" 10 "b" 11 "c" 12 "d" 13 "e" 14 "f" 15)]
        (set ret (+ ret (* value (math.pow 16 (- len i))))))))
  ret)

(fn hex-to-rgb [hex]
  (let [r (string.sub hex 2 3)
        g (string.sub hex 4 5)
        b (string.sub hex 6 7)]
    [(/ (hex-to-value r) 256)
     (/ (hex-to-value g) 256)
     (/ (hex-to-value b) 256)]))

(fn tri-level-perlin [key i j f]
  (let [value1 (love.math.noise (+ key (* i f))
                                (+ key (* j f)))
        value2 (love.math.noise (+ key (* i (* f 2)))
                                (+ key (* j (* f 2))))
        value3 (love.math.noise (+ key (* i (* f 4)))
                                (+ key (* j (* f 4))))
        value (+ (* (/ 4 7) value1)
                 (* (/ 2 7 ) value2)
                 (* (/ 1 7) value3))]
    value))

(fn quant [value levels]
  (if levels (/ (math.floor (* value levels)) levels)
      value))

(fn noisemap.generate-gray [key w h f]
    "Generate a Simplex Noise Map."
  (local data (love.image.newImageData w h))
  (for [i 1 w]
    (for [j 1 h]
      (let [value (tri-level-perlin key i j f)]
        (data:setPixel (- i 1) (- j 1) value value value 1))))
  (love.graphics.newImage data))


(fn noisemap.generate-colour [key w h f min max]
    "Generate a Simplex Noise Map.
Try passing in some colours to min and max #2d5e0f #14360A."
    (let [data (love.image.newImageData w h)
          min (hex-to-rgb min)
          max (hex-to-rgb max)
          out []]
      (for [i 1 w]
        (for [j 1 h]
          (let [value [(tri-level-perlin key i j f)
                       (tri-level-perlin (+ 1 key) i j f)
                       (tri-level-perlin (+ 2 key) i j f)]]
            (for [i 1 3]
              (let [l (. min i)
                    u (. max i)
                    d (- u l)]
                (tset out i (+ u (* (quant (. value i) 10) d)))))
            (data:setPixel (- i 1) (- j 1) out 1))))
      (let [image (love.graphics.newImage data)]
        (image:setFilter "nearest" "nearest")
        image)))

noisemap
