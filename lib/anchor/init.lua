 local _NAME = ... local __NAME = _NAME:gsub(".anchor$", "")










 local lg = love.graphics local stack_gutter_x, stack_alignment, stack_dy, stack_l, stack_arrangement, stack_w, stack_h, stack_window_w, stack_viewport_h, stack_gutter_y, stack_anchor, stack_viewport_l, stack_viewport_t, stack_t, stack_dx, stack_viewport_w, stack_window_t, stack_window_h, stack_window_l = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {"top-left", "top-left", "top-left", "top-left", "top-left", "top-left", "top-left", "top-left", "top-left", "top-left", "top-left", "top-left", "top-left", "top-left", "top-left", "top-left", "top-left", "top-left", "top-left", "top-left"}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {"anchor", "anchor", "anchor", "anchor", "anchor", "anchor", "anchor", "anchor", "anchor", "anchor", "anchor", "anchor", "anchor", "anchor", "anchor", "anchor", "anchor", "anchor", "anchor", "anchor"}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {"top-left", "top-left", "top-left", "top-left", "top-left", "top-left", "top-left", "top-left", "top-left", "top-left", "top-left", "top-left", "top-left", "top-left", "top-left", "top-left", "top-left", "top-left", "top-left", "top-left"}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} local stack_depth = 0 local window_depth = 0 local stack_depth_warning = "Surpassed anchor stack depth. Current: %s - Max: %s"















 local anchors = {["top-left"] = {0, 0}, top = {0.5, 0}, ["top-right"] = {1, 0}, ["bottom-left"] = {0, 1}, bottom = {0.5, 1}, ["bottom-right"] = {1, 1}, right = {1, 0.5}, left = {0, 0.5}, center = {0.5, 0.5}}










 local zero = {0, 0}

 local function anchor_to_pos(x, y, anchor)
 local _1_ = type(anchor) if (_1_ == "string") then
 local _let_2_ = (anchors[anchor] or zero) local px = _let_2_[1] local py = _let_2_[2]
 return (x * px), (y * py) elseif (_1_ == "table") then
 local _let_3_ = anchor local px = _let_3_[1] local py = _let_3_[2]
 return (px * x), (py * y) else local _ = _1_
 return x, y end end

 local function coords_transform(l, t, w, h, no_shift) local arrangement, alignment, anchor, gx, gy, dx, dy = stack_arrangement[stack_depth], stack_alignment[stack_depth], stack_anchor[stack_depth], stack_gutter_x[stack_depth], stack_gutter_y[stack_depth], stack_dx[stack_depth], stack_dy[stack_depth] local pl, pt, pw, ph = stack_l[stack_depth], stack_t[stack_depth], stack_w[stack_depth], stack_h[stack_depth]


 local al_x, al_y = anchor_to_pos(w, h, alignment)
 local an_x, an_y = anchor_to_pos(pw, ph, anchor)
 local cl = (l + dx + pl + ( - al_x) + an_x)
 local ct = (t + dy + pt + ( - al_y) + an_y)
 local cw = w
 local ch = h do

 local _5_ = arrangement if (_5_ == "column") then
 local dyp do local _6_ = alignment if (_6_ == "top") then
 dyp = (dy + h + gy) elseif (_6_ == "bottom") then
 dyp = (dy - h - gy) else local _ = _6_
 dyp = (dy + h + gy) end end if not no_shift then stack_dx[stack_depth] = dx stack_dy[stack_depth] = dyp end elseif (_5_ == "row") then

 local dxp do local _9_ = alignment if (_9_ == "left") then
 dxp = (dx + w + gx) elseif (_9_ == "right") then
 dxp = (dx - w - gx) else local _ = _9_
 dxp = (dx + w + gx) end end if not no_shift then stack_dx[stack_depth] = dxp stack_dy[stack_depth] = dy end elseif (_5_ == "grid") then

 local dyp = (dy + h + gy)
 local dxp = (dx + w + gx)
 local new_row = ((dxp + w + gx) > pw) if not no_shift then


 if new_row then stack_dx[stack_depth] = 0 stack_dy[stack_depth] = dyp else stack_dx[stack_depth] = dxp stack_dy[stack_depth] = dy end end end end



 return cl, ct, cw, ch end

 local function set_screen(w_3f, h_3f)
 local w, h = love.window.getMode() stack_depth = 1 do stack_l[stack_depth] = 0 stack_t[stack_depth] = 0 stack_w[stack_depth] = (w_3f or w) do end (stack_h)[stack_depth] = (h_3f or h) end do stack_arrangement[stack_depth] = "anchor" end do stack_alignment[stack_depth] = "top-left" end do stack_anchor[stack_depth] = "top-left" end do stack_gutter_x[stack_depth] = 0 stack_gutter_y[stack_depth] = 0 end do stack_dx[stack_depth] = 0 stack_dy[stack_depth] = 0 end







 return 0, 0, (w_3f or w), (h_3f or h) end

 local function window_push(l, t, w, h, vl, vt, vw, vh) if (stack_depth < 1) then
 set_screen() end local pvl, pvt = stack_viewport_l[window_depth], stack_viewport_t[window_depth], stack_viewport_w[window_depth], stack_viewport_h[window_depth] local pl, pt, pw, ph = stack_l[stack_depth], stack_t[stack_depth], stack_w[stack_depth], stack_h[stack_depth]


 local wl, wt, ww, wh = coords_transform((l or 0), (t or 0), (w or pw), (h or ph), true)

 window_depth = (window_depth + 1) do stack_window_l[window_depth] = wl stack_window_t[window_depth] = wt stack_window_w[window_depth] = ww stack_window_h[window_depth] = wh end stack_viewport_l[window_depth] = vl stack_viewport_t[window_depth] = vt stack_viewport_w[window_depth] = vw stack_viewport_h[window_depth] = vh return nil end local function window_pop()





 assert((window_depth > 0), "Anchor stack: Nothing to pop")
 window_depth = (window_depth - 1) return nil end

 local function stack_push(l, t, w, h, arrangement, alignment, anchor, gx, gy) if (stack_depth < 1) then


 set_screen() end local pl, pt, pw, ph = stack_l[stack_depth], stack_t[stack_depth], stack_w[stack_depth], stack_h[stack_depth]



 local cl, ct, cw, ch = coords_transform((l or 0), (t or 0), (w or pw), (h or ph))

 stack_depth = (stack_depth + 1)
 assert((stack_depth < 20), string.format(stack_depth_warning, stack_depth, 20)) do

 local _17_ = arrangement if (_17_ == "window") then stack_l[stack_depth] = 0 stack_t[stack_depth] = 0 stack_w[stack_depth] = cw stack_h[stack_depth] = ch else local _ = _17_ stack_l[stack_depth] = cl stack_t[stack_depth] = ct stack_w[stack_depth] = cw stack_h[stack_depth] = ch end end do stack_arrangement[stack_depth] = (arrangement or "anchor") end do stack_alignment[stack_depth] = (alignment or "top-left") end do stack_anchor[stack_depth] = (anchor or alignment or "top-left") end do stack_gutter_x[stack_depth] = (gx or 0) do end (stack_gutter_y)[stack_depth] = (gy or 0) end do stack_dx[stack_depth] = 0 stack_dy[stack_depth] = 0 end








 return cl, ct, cw, ch end local function stack_pop()


 assert((stack_depth > 0), "Anchor stack: Nothing to pop")
 stack_depth = (stack_depth - 1) return nil end local function stack_size()

 return stack_depth end local function stack_state()


 return {stack_l[stack_depth], stack_t[stack_depth], stack_w[stack_depth], stack_h[stack_depth]} end local function push_window(wl, wt, canvas, quad) _G.assert((nil ~= quad), "Missing argument quad on anchor.fnl:139") _G.assert((nil ~= canvas), "Missing argument canvas on anchor.fnl:139") _G.assert((nil ~= wt), "Missing argument wt on anchor.fnl:139") _G.assert((nil ~= wl), "Missing argument wl on anchor.fnl:139") local ww, wh = canvas:getDimensions() local vl, vt, vw, vh = quad:getViewport()




 window_push(wl, wt, ww, wh, vl, vt, vw, vh)
 stack_push(0, 0, ww, wh, "window")
 lg.push("all")
 return lg.setCanvas(canvas) end

 local function push_element(l, t, w, h)
 local xp, yp = stack_push(l, t, w, h, "element")
 lg.push("all")
 return lg.translate(xp, yp) end

 local function push_anchor(l, t, w, h, ...)
 return stack_push(l, t, w, h, "anchor", ...) end

 local function push_column(l, t, w, h, _3fdirection, _3fgutter)
 local alignment, anchor = nil, nil do local _19_ = _3fdirection if (_19_ == "top") then
 alignment, anchor = "top", "top" elseif (_19_ == "bottom") then
 alignment, anchor = "bottom", "bottom" else local _ = _19_
 alignment, anchor = "top-left", "top-left" end end
 return stack_push(l, t, w, h, "column", alignment, anchor, _3fgutter, _3fgutter) end


 local function push_row(l, t, w, h, _3fdirection, _3fgutter)
 local alignment, anchor = nil, nil do local _21_ = _3fdirection if (_21_ == "left") then
 alignment, anchor = "left", "left" elseif (_21_ == "right") then
 alignment, anchor = "right", "right" else local _ = _21_
 alignment, anchor = "top-left", "top-left" end end
 return stack_push(l, t, w, h, "row", alignment, anchor, _3fgutter, _3fgutter) end


 local function push_grid(x, y, w, h, _3fgutter_x, _3fgutter_y)
 return stack_push(x, y, w, h, "grid", "top-left", "top-left", _3fgutter_x, (_3fgutter_y or _3fgutter_x)) end local function pop(_3fcanvas, _3fquad) do local _23_ = stack_arrangement[stack_depth] if (_23_ == "window") then







 assert(_3fcanvas, "Need to pass in a canvas when  poping a window")
 assert(_3fquad, "Need to pass in a quad when  poping a window") local xp, yp = stack_window_l[window_depth], stack_window_t[window_depth], stack_window_w[window_depth], stack_window_h[window_depth]

 lg.pop()
 lg.draw(_3fcanvas, _3fquad, xp, yp)
 window_pop() elseif (_23_ == "element") then
 lg.pop() end end
 return stack_pop() end local function over(x, y) _G.assert((nil ~= y), "Missing argument y on anchor.fnl:190") _G.assert((nil ~= x), "Missing argument x on anchor.fnl:190")



 local function point_within_rect(l, t, w, h, x0, y0)
 return (((x0 > l) and (x0 < (l + w))) and ((y0 > t) and (y0 < (t + h)))) end

 local xp, yp = x, y local collide = true


 for i = 1, window_depth do local l, t, w, h = stack_viewport_l[i], stack_viewport_t[i], stack_viewport_w[i], stack_viewport_h[i] local wl, wt = stack_window_l[i], stack_window_t[i], stack_window_w[i], stack_window_h[i]




 collide = (collide and point_within_rect(wl, wt, w, h, xp, yp))

 xp = (xp + ( - wl) + l)
 yp = (yp + ( - wt) + t) end local cl, ct, cw, ch = stack_l[stack_depth], stack_t[stack_depth], stack_w[stack_depth], stack_h[stack_depth]


 collide = (collide and point_within_rect(cl, ct, cw, ch, xp, yp))
 return collide, (xp - cl), (yp - ct) end

 local debug = {} debug["quad-first"] = false


 debug.quad = function(once) if (not once or debug["quad-first"]) then debug["quad-first"] = false


 for i = 1, stack_depth do
 pp({"quad", i, stack_l[i], stack_t[i], stack_w[i], stack_h[i]})
 pp({"properties", i, stack_arrangement[i], stack_alignment[i], stack_anchor[i], stack_gutter_x[i], stack_gutter_y[i], stack_dx[i], stack_dy[i]}) end return nil end end debug["viewport-first"] = false


 debug.window = function(once) if (not once or debug["viewport-first"]) then debug["viewport-first"] = false


 for i = 1, window_depth do
 pp({"viewport", i, stack_viewport_l[i], stack_viewport_t[i], stack_viewport_w[i], stack_viewport_h[i]})
 pp({"window", i, stack_window_l[i], stack_window_t[i], stack_window_w[i], stack_window_h[i]}) end return nil end end

 return {["push-column"] = push_column, ["push-row"] = push_row, ["push-grid"] = push_grid, ["push-anchor"] = push_anchor, ["push-window"] = push_window, ["push-element"] = push_element, ["stack-size"] = stack_size, ["stack-state"] = stack_state, pop = pop, over = over, debug = debug}
