;; MODULE: ANCHOR
;; VERSION: 0.2.0
;; LICENCE: LGPL3
;; AUTHOR: ALEXANDER GRIFFITH
;; DESCRIPTION: A minimalist immediate mode UI implementation for love.

(local _NAME ...)

(local __NAME (_NAME:gsub ".anchor$" ""))

;; (pp name)
(local lg love.graphics)


(import-macros {: create-stacks : get-quad : get-window : get-viewport
                : get-properties : set-quad : set-window : set-viewport
                : set-anchor : set-arrangement : set-alignment : set-dxy
                : set-gutter : max-stack : get-arrangement : stack-last}
               :stack-macros)

(create-stacks)

(var stack-depth 0)
(var window-depth 0)

(local stack-depth-warning "Surpassed anchor stack depth. Current: %s - Max: %s")

(local anchors {:top-left [0 0]
                :top [0.5 0]
                :top-right [1 0]
                :bottom-left [0 1]
                :bottom [0.5 1]
                :bottom-right [1 1]
                :right [1 0.5]
                :left [0 0.5]
                :center [0.5 0.5]
                })

(local zero [0 0])

(fn anchor-to-pos [x y anchor]
  (match (type anchor)
    :string (let [[px py] (or (. anchors anchor) zero)]
              (values (* x px) (* y py)))
    :table (let [[px py] anchor]
             (values (* px x) (* py y)))
    _ (values x y)))

(fn coords-transform [l t w h no-shift]
  (let [(arrangement alignment anchor gx gy dx dy) (get-properties)
        (pl pt pw ph) (get-quad)
        (al-x al-y) (anchor-to-pos w h alignment)
        (an-x an-y) (anchor-to-pos pw ph anchor)
        cl (+ l dx pl (- al-x) an-x) ;; canvas space
        ct (+ t dy pt (- al-y) an-y) ;; canvas space
        cw w ;; canvas space
        ch h ;; canvas space
        ]    
    (match arrangement
      :column (let [dyp (match  alignment
                          :top (+ dy h gy)
                          :bottom (- dy h gy)
                          _ (+ dy h gy))]
                (when (not no-shift) (set-dxy dx dyp)))
      :row (let [dxp (match  alignment
                       :left (+ dx w gx)
                       :right (- dx w gx)
                       _ (+ dx w gx))]
             (when (not no-shift) (set-dxy dxp dy)))
      :grid (let [dyp (+ dy h gy)
                  dxp (+ dx w gx)
                  new-row (> (+ dxp w gx) pw)]

              (when (not no-shift)
                (if new-row
                  (set-dxy 0 dyp)
                  (set-dxy dxp dy)))
              ))
    (values cl ct cw ch)))

(fn set-screen [w? h?]
  (local (w h) (love.window.getMode))
  (set stack-depth 1)  
  (set-quad 0 0 (or w? w) (or h? h))
  (set-arrangement :anchor)
  (set-alignment :top-left)
  (set-anchor :top-left)
  (set-gutter 0 0)
  (set-dxy 0 0)
  (values 0 0 (or w? w) (or h? h)))

(fn window-push [l t w h vl vt vw vh]
  (when (< stack-depth 1) (set-screen))
  (let [(pvl pvt) (get-viewport)
        (pl pt pw ph) (get-quad)
        (wl wt ww wh) (coords-transform (or l 0) (or t 0)
                                        (or w pw) (or h ph) true)]
    (set window-depth (+ window-depth 1))
    (set-window wl wt ww wh) ;; canvas space
    (set-viewport vl vt vw vh)
    ))

(lambda window-pop []
  (assert (> window-depth 0) "Anchor stack: Nothing to pop")
  (set window-depth (- window-depth 1)))

(fn stack-push [l t w h 
                arrangement alignment anchor
                gx gy]
  (when (< stack-depth 1) (set-screen))
  (let [(pl pt pw ph) (get-quad)
        ;; (_ _ vw vh) (get-viewport)
        (cl ct cw ch) 
        (coords-transform (or l 0) (or t 0)
                          (or w pw) (or h ph))]
    (set stack-depth (+ stack-depth 1))
    (assert (< stack-depth (max-stack))
          (string.format stack-depth-warning stack-depth (max-stack)))
    (match arrangement
      :window (set-quad 0 0 cw ch)
      _ (set-quad cl ct cw ch))
    
    (set-arrangement (or arrangement :anchor))
    (set-alignment (or alignment :top-left))
    (set-anchor (or anchor alignment :top-left))
    (set-gutter (or gx 0) (or gy 0))
    (set-dxy 0 0)
    (values cl ct cw ch)))

(lambda stack-pop []
  (assert (> stack-depth 0) "Anchor stack: Nothing to pop")
  (set stack-depth (- stack-depth 1)))

(lambda stack-size [] stack-depth)

(lambda stack-state []
  [(get-quad)])

(lambda push-window [wl wt canvas quad]
  (local (ww wh) (canvas:getDimensions))
  (local (vl vt vw vh) (quad:getViewport))
  (window-push wl wt ww wh vl vt vw vh)
  (stack-push 0 0 ww wh :window)
  (lg.push :all)
  (lg.setCanvas canvas))

(fn push-element [l t w h]
  (local (xp yp) (stack-push l t w h :element))
  (lg.push :all)
  (lg.translate xp yp))

(fn push-anchor [l t w h ...]
  (stack-push l t w h :anchor ...))

(fn push-column [l t w h ?direction ?gutter]
  (let [(alignment anchor) (match ?direction
                             :top (values :top :top)
                             :bottom (values :bottom :bottom)
                             _ (values :top-left :top-left))]
    (stack-push l t w h
                :column alignment anchor ?gutter ?gutter)))

(fn push-row [l t w h ?direction ?gutter]
    (let [(alignment anchor) (match ?direction
                             :left (values :left :left)
                             :right (values :right :right)
                             _ (values :top-left :top-left))]
      (stack-push l t w h
                  :row alignment anchor ?gutter ?gutter)))

(fn push-grid [x y w h ?gutter-x ?gutter-y]
  (stack-push x y w h
              :grid :top-left :top-left
              ?gutter-x (or ?gutter-y ?gutter-x)))

(lambda pop [?canvas ?quad]
  (match (get-arrangement)
    :window
    (do 
      (assert ?canvas "Need to pass in a canvas when  poping a window")
      (assert ?quad "Need to pass in a quad when  poping a window")
      (let [(xp yp) (get-window)]
        (lg.pop)
        (lg.draw ?canvas ?quad xp yp)
        (window-pop)))
    :element (lg.pop))  
  (stack-pop))

;; need to get the xy in element space out of this
(lambda over [x y]
  (fn point-within-rect [l t w h x y]
    (and (and (> x l) (< x (+ l w)))
         (and (> y t) (< y (+ t h)))))
  (var (xp yp) (values x y))
  (var collide true)
  ;; check if in viewports
  (for [i 1 window-depth]
    (let [(l t w h) (get-viewport i)
          (wl wt) (get-window i)          
          ]
      ;; check viewport
      (set collide (and collide (point-within-rect wl wt w h xp yp)))
      ;; shift xp yp to the current view space
      (set xp (+ xp (- wl) l))
      (set yp (+ yp (- wt) t))))
  ;; check if in element
  (local (cl ct cw ch ) (get-quad))
  (set collide (and collide (point-within-rect cl ct cw ch xp yp)))
  (values collide (- xp cl) (- yp ct)))

(local debug {})

(set debug.quad-first false)
(fn debug.quad [once]
  (when  (or (not once) debug.quad-first)
    (set debug.quad-first false)
    (for [i 1 stack-depth]
      (pp [:quad i (get-quad i)])
      (pp [:properties i (get-properties i)]))))

(set debug.viewport-first false)
(fn debug.window [once]
  (when  (or (not once) debug.viewport-first)
    (set debug.viewport-first false)
    (for [i 1 window-depth]
      (pp [:viewport i (get-viewport i)])
      (pp [:window i (get-window i)]))))  

{: push-column
 : push-row
 : push-grid
 : push-anchor
 : push-window
 : push-element
 : stack-size
 : stack-state
 : pop
 : over
 : debug
 }
