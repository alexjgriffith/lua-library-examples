(local fennel (require :lib.fennel))
(global pp (fn [x] (print (fennel.view x))))

(local fml-imui (require :fml-imui))

;; Example - should really learn how to write tests :/
(local sample-ml-1
       [["TEXTBOX" {:anchor :center
                    :padding [5 5 10 5]}
         ["9PATCH" {:image :image-string :quad [0 0 10 10]}
          ["TEXT" {} "Text" "More Text" "Even MORE!"]
          ["TEXT" {} "Text2"]]]
        ["TEXT" {} "Text3"]])

(local dom (fml-imui.ml-to-dom sample-ml-1 500 600))

(pp (. dom 1 :attributes :arrangement))

(pp (. dom :__AUTHOR))
