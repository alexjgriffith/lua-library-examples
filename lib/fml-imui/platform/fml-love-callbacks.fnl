(local fml-love-callbacks
       {:_PLATFORM "LOVE"
        :_PLATFORM_AUTHOR "Alexander Griffith"
        :_PLATFORM_LICENCE "MIT"
        :_PLATFORM_OVERSION "0.1.0"
        :_PLATFORM_DESCRIPTION "Love callbacks and state for fml-imui"})


;; Canvas UI Elements are draw to
(var canvas nil)

;; Index of string to love image resource
(local image-index {})

;; Index of callback-string to click function
(local click-callback-index {})

;; Index of string to focus function
(local focus-callback-index {})

;; Index of string to unfocus function
(local unfocus-callback-index {})

;; Index of string to custom render function
(local custom-render-index {})

;; Canvas Pool {"h,w": LOVE Cavas}
(local canvas-pool {})

;; Mesh Pool {"h,w,r": LOVE MESH}
;; Ment for rectangular meshes only
(local mesh-pool {})

;; Quad Pool {w,h: Love Quad}
(local quad-pool {})

;; MousePressed Bool
(var mousepressed nil)

;; MouseReleased Bool
(var mousereleased nil)

(fn arc-segments [s e w h r index]
  "Return 3 arch segments for a rectangular corner.

Internal to fml-love-callbacks.

S [X Y] start
E [X Y] end
W rectangle width
H rectangle height
R radius of corners
index 1-4"
  (match index
    :1 [s
        [(- (- w r) (* 1 r (math.cos (* 1.40 math.pi))))
         (+ r  (* r (math.sin (* 1.40 math.pi))))]

        [(- (- w r) (* 1 r (math.cos (* 1.25 math.pi))))
         (+ r  (* r (math.sin (* 1.25 math.pi))))]

        [(- (- w r) (* 1 r (math.cos (* 1.1 math.pi))))
         (+ r  (* r (math.sin (* 1.1 math.pi))))]
        e]
    :2 [s

        [(- (- w r) (* 1 r (math.cos (* 0.90 math.pi))))
         (+ (- h r)  (* r (math.sin (* 0.90 math.pi))))]

        [(- (- w r) (* 1 r (math.cos (* 0.75 math.pi))))
         (+ (- h r)  (* r (math.sin (* 0.75 math.pi))))]

        [(- (- w r) (* 1 r (math.cos (* 0.60 math.pi))))
         (+ (- h r)  (* r (math.sin (* 0.60 math.pi))))]

        e]
    :3 [s
        [(- r (* 1 r (math.cos (* 0.4 math.pi))))
           (+ (- h r)  (* r (math.sin (* 0.4 math.pi))))]

        [(- r (* 1 r (math.cos (* 0.25 math.pi))))
         (+ (- h r)  (* r (math.sin (* 0.25 math.pi))))]

        [(- r (* 1 r (math.cos (* 0.1 math.pi))))
           (+ (- h r)  (* r (math.sin (* 0.1 math.pi))))]

        e]
    :4 [s
        [(- r (* 1 r (math.cos (* -0.1 math.pi))))
         (+ r  (* r (math.sin (* -0.1 math.pi))))]

        [(- r (* 1 r (math.cos (* -0.25 math.pi))))
         (+ r  (* r (math.sin (* -0.25 math.pi))))]

        [(- r (* 1 r (math.cos (* -0.40 math.pi))))
         (+ r  (* r (math.sin (* -0.40 math.pi))))]
        e]
    _ [s e]))


(fn make-mesh [w h r]
  "Make a rounded corner rectangle mesh

Internal use only!

W width of rectangle
H height of rectangle"
  (let [array-0 [[0 0 0 0]
                 [w 0 1 0]
                 [w h 1 1]
                 [0 h 0 1]
                   ]
        _array [[r 0] [(- w r) 0]

                 [w r] [w (- h r)]

                 [(- w r) h] [r h]

                 [0 (- h r)] [0 r]
]
        array [(arc-segments [(- w r) 0] [w r] w h r :1)
               (arc-segments  [w (- h r)] [(- w r) h] w h r :2)
               (arc-segments  [r h] [0 (- h r)] w h r :3)
               (arc-segments  [0 r] [r 0] w h r :4)]]
    (var flat [])
    (each [key members (ipairs array)]
      (for [i 1 (# members)]
        (let [to-add (. members i)]
          (table.insert to-add (/ (. to-add 1) w))
          (table.insert to-add (/ (. to-add 2) h))
          (table.insert flat to-add))))
    (love.graphics.newMesh (if (= r 0) array-0 flat) :fan)))

(fn get-quad [w h]
  (let [index (.. w "," h )
        old-quad (. quad-pool index)]
    (if old-quad
        old-quad
        (let [new-quad (love.graphics.newQuad 0 0 0 0 w h)]
          (tset quad-pool index new-quad)
          new-quad))))

(fn get-mesh [w h r]
    "Find or create a rounded corner rectangle mesh.

Look in mesh-cache to see if the mesh has already been made.
If a mesh doesn't exist generate one.
Internal use only!

W width of rectangle
H height of rectangle
R corner radius"
  (let [index (.. w "," h "," r)
        old-mesh (. mesh-pool index)]
    (if old-mesh
        old-mesh
        (let [new-mesh (make-mesh w h r)]
          (tset mesh-pool index new-mesh)
          new-mesh))))

(fn get-canvas [w h]
  "Find or create a canvas.

Look in canvas-cache to see if the canvas has already been made.
If a canvas of W H doesn't exist generate one.
Internal use only!

W width of canvas
H height of canvas"
  (let [index (.. w "," h )
        old-canvas (. canvas-pool index)]
    (if old-canvas
        old-canvas
        (let [new-canvas (love.graphics.newCanvas w h)]
          (tset canvas-pool index new-canvas)
          new-canvas))))

(fn get-image-mesh [image quad r]
  "Find or create a mesh with texture canvas.

Internal use only!

image string of image. Indexes image-index
quad [x y w h]
r radius of image corners"
  (let [[x y w h] quad
        mesh (get-mesh w h r)
        canvas (love.graphics.newCanvas w h);;(get-canvas w h)
        ]
    (love.graphics.push "all")
    (love.graphics.origin)
    (love.graphics.setColor 1 1 1 1)
    (love.graphics.setCanvas canvas)
    (local share-quad (get-quad (image:getWidth)
                                (image:getHeight)
                                ))
    (share-quad:setViewport (unpack quad))
    (love.graphics.draw image)
    (love.graphics.pop)
    ;; (love.graphics.rectangle :fill 0 0 100 100)
    (mesh:setTexture canvas)
    mesh))

;; Callbacks used by fml-imui
;; Required: Push-Offset, debug, Pop-Offset, text, background
(local render-callbacks
 {:push-offset
  (fn [{: t : l}]
    "Set the X Y offset for all following renders"
    (love.graphics.push "all")
    (love.graphics.setCanvas canvas)
    (love.graphics.translate l t))

  :custom-render
  (fn [{: color : custom-render : x : y : w : h} mouse]
    "Custom rendering. Sets colour and calls the custom render
function"
    (love.graphics.setColor color)
    (match (. custom-render-index custom-render)
      nil :pass
      value (value x y w h mouse)))

  :debug
  (fn [render]
    "Debug, draws the margins, padding and content in red blue
and green respectivly."
    (love.graphics.setColor 1 0 0 1)
    (let [{: x : y : w : h} render.margin-box]
      (love.graphics.rectangle :fill x y w h))
    (love.graphics.setColor 0 1 0 1)
    (let [{: x : y : w : h} render.padding-box]
      (love.graphics.rectangle :fill x y w h))
    (love.graphics.setColor 0 0 1 1)
    (let [{: x : y : w : h} render.content-box]
      (love.graphics.rectangle :fill x y w h)))

  :background
  (fn [{: render : color : radius : x : y : w : h}]
    "Callback fro drawing the element background."
    (when render
      (love.graphics.setColor color)
      (love.graphics.rectangle :fill x y w h radius)))

  :outline
  (fn [{: render : color : radius : x : y : w : h}]
    "Callback fro drawing the element outline."
    (when render
      (love.graphics.setColor color)
      (love.graphics.rectangle :line x y w h radius)))

  :shadow
  (fn [{: render : color : radius : dx : dy
        : x : y : w : h}]
    "Callback fro drawing the element shadow."
    (when render
      (love.graphics.setColor color)
      (love.graphics.rectangle :fill (+ dx x) (+ dy y)
                               w h radius)))

  :text
  (fn [{: render : color : font : alignment
        : x : y : w }]
    "Callback fro drawing the element text."
    (when render
      (love.graphics.setColor color)
      (local old-font (love.graphics.getFont))
      (when (not (= font :default))
        (love.graphics.setFont font))
      (love.graphics.printf render x y w alignment)
      (love.graphics.setFont old-font)))

  :image
  (fn [{: render : color : radius : src : quad : image-type
        : x : y}]
    "Callback fro drawing the element image."
    (when (and render image-index)
      (let [image (. image-index src)]
        (when image
          (love.graphics.setColor color)
          (match image-type
            :rounded-quad
            (do
              (let [mesh (get-image-mesh image quad radius)]
                (love.graphics.draw mesh 0 0))
              )
            :quad (do
                    (let [w (image:getWidth)
                          h (image:getHeight)
                          share-quad (get-quad w h)]
                      (share-quad:setViewport (unpack quad))
                    (love.graphics.draw image share-quad x y)))
            :9patch (pp :not-implemented)
            :raw (love.graphics.draw image x y)
            ))
        ))
    )

  :pop-offset
  (fn []
    "Callback for unsetting x y offset."
    (love.graphics.pop))})


(lambda add-image [src ?value ?force-replace]
  "Add an image to image-index. Call in love.init.

The key will be the name of the image. The value will be the loaded
value or ?value (if loaded using cargo for example).

?force-replace will overwrite an existing index value."
  (when (or (not (. image-index src)) ?force-replace)
    (let [new-image (or ?value (love.graphics.newImage src))]
      (tset image-index src new-image)
      new-image)))

(lambda add-click-callback [key fun ?force-replace]
  "Set click-callback-index key to fun. Call in love.init.

keys in elements attributes :on-click will match this key.
fun: (fn callback [element button x y ?ox ?oy])
?force-replace: overwrite an existing index value."
  (when (or (not (. click-callback-index fun)) ?force-replace)
    (tset click-callback-index key fun)
    fun))

(lambda add-focus-callback [key fun ?force-replace]
  "Set focus-callback-index key to fun. Call in love.init.

keys in elements attributes :on-focus will match this key.
fun: (fn callback [element])
?force-replace: overwrite an existing index value."
  (when (or (not (. focus-callback-index fun)) ?force-replace)
    (tset focus-callback-index key fun)
    fun))

(lambda add-unfocus-callback [key fun ?force-replace]
  "Set unfocus-callback-index key to fun. Call in love.init.

keys in elements attributes :on-unfocus will match this key.
fun: (fn [element])
?force-replace will overwrite an existing index value."
  (when (or (not (. unfocus-callback-index fun)) ?force-replace)
    (tset unfocus-callback-index key fun)
    fun))

(lambda add-custom-render [key fun ?force-replace]
  "Set custom-render-index key to fun. Call in love.init.

This fun is called in place of the standard rendering pipeline.
fun: (fn callback [x y w h])
?force-replace: overwrite an existing index value.
"
  (when (or (not (. custom-render-index fun)) ?force-replace)
    (tset custom-render-index key fun)
      fun))

(lambda mousepressed-update [button ?x ?y]
  "Set mousepressed to button. Call in love.mousepressed.

This value is read and reset by fml-imui every frame."
  (set mousepressed button))

(lambda mousereleased-update [button ?x ?y]
  "Set mousereleased to button. Call in love.mousereleased.

This value is read and reset by fml-imui every frame."
  (set mousereleased button ))

(fn mousepressed-reset []
  "Internal"
  (set mousepressed nil))

(fn mousereleased-reset []
  "Internal"
  (set mousereleased nil))

(lambda mouse-callback []
  "Get mouse information. Called by fml-imui.

provides [x y left right mousepressed mousereleased]

resets mousepressd and mousereleased when called"
  (let [(x y) (love.mouse.getPosition)
        right (love.mouse.isDown 2)
        left (love.mouse.isDown 1)
        mousepressed mousepressed
        mousereleased mousereleased]
    (mousepressed-reset)
    (mousereleased-reset)
    (values x y right left mousepressed mousereleased)))

(lambda click-callback [element button x y ?ox ?oy]
  "On click call this function. Called by fml-imui.

element raw ui element table
button 1 = left 2 = right
x = relative x - relative to element tl
y = relative y - relative to element tl
?ox = offset x - takes into account camera offset
?oy = offset y  - takes into account camera offset
"
  (match (. click-callback-index element.on-click)
    nil :pass
    value (value element button x y ?ox ?oy)))

(lambda hover-callback [focus element]
  "On first hover call this fuction. Called by fml-imui.

Looks up either unfocus or hover in the appropriate callback
index. and passess the the element table.

focus :focus :unfocus
element raw ui element table"
  ((. (match focus
        :focus focus-callback-index
        :unfocus unfocus-callback-index)
      (. element (match focus
                   :focus :on-focus
                   :unfocus :on-unfocus)))
   element))



(lambda set-canvas [in-canvas]
  (set canvas in-canvas))

(lambda resize-canvas [w h]
  (when canvas
    (set canvas (love.graphics.newCanvas w h))))

(each [key callback
       (pairs
        {: render-callbacks ;; Called by fml-imgui
         : mouse-callback ;; Called by fml-imui
         : click-callback ;; Called by fml-imui
         : hover-callback ;; Called by fml-imui
         : mousepressed-update ;; Call in love.mousepressed
         : mousereleased-update ;; Call in love.mousereleased
         : add-image ;; Call in love.init
         : add-click-callback ;; Call in love.init
         : add-focus-callback ;; Call in love.init
         : add-unfocus-callback ;; Call in love.init
         : add-custom-render ;; Call in love.init
         : set-canvas ;; Set the default canvas in love.init
         : resize-canvas ;; called bu fml-imui
         })]
  (tset fml-love-callbacks key callback))

fml-love-callbacks
