(local _NAME ...)

(local __NAME (_NAME:gsub ".fml.imui$" ""))

(local fml-imui {:_AUTHOR "Alexander Griffith"
                 :_LICENCE "MIT"
                 :_VERSION "0.1.0"
                 :_DESCRIPTION "A markup defined immediate mode GUI."})

(local default-style
       {;; CHILD POSITIONING
        :arrangement :stacked
        ;; POSITIONING
        :max-width nil
        :max-height nil
        :anchor :center
        :alignment :center
        :overflow [:crop :crop]
        :gutter [0 0]
        ;; RELATIVE POSITIONING
        :x 0
        :y 0
        :width 0
        :height 0
        :padding [0 0 0 0] ;; t l b r
        :margin [0 0 0 0]
        ;; SHADOW
        :shadow-color "#00000033"
        :shadow-color-hover nil
        :shadow-dx 0
        :shadow-dy 0
        :shadow false
        ;; BACKGROUND
        :radius 0
        :color "#FF0000FF"
        :hover-color nil
        :background true
        ;; OUTLINE
        :outline-color "#000000FF"
        :outline-hover-color nil
        :outline-thickness 1
        :outline false
        ;; Image
        :image-type :quad
        :image-type-hover nil
        :image-src false
        :image-src-hover nil
        :image-quad false
        :image-quad-hover nil
        :image false
        :image-color "#FFFFFFFF"
        :image-color-hover nil
        ;; Text
        :text-color "#000000FF"
        :text-hover-color nil
        :text-font :default
        :text-font-hover nil
        :text-alignment :left
        :text-alignment-hover nil
        ;; Can think about scroll-bars buttons and sliders
        ;; CUSTOM CODE
        :on-click nil
        :on-focus nil
        :on-unfocus nil
        :value nil
        :curstom-render nil
        :id nil
        })


(fn define-style [array]
  (local style {})
  (each [class attributes (pairs array)]
    (if (and attributes.inherits (. array attributes.inherits))
        (let [inherits {:__index (. array attributes.inherits)}]
          (setmetatable attributes inherits))
        (setmetatable attributes {:__index default-style}))
    (tset style class {:__index attributes}))
  style)

;; TODO - Implement a bunch of useful styles
(local styles
       (define-style
        {:TEXTBOX
         {:overflow-y :crop
          :padding [5 5 5 5]
          :margin [10 10 10 10]}

         :9PATCH
         {:padding [2 2 2 2]
          :margin [5 5 5 5]
          }

         :TEXT
         {:width 100
          :height 20
          :padding [5 5 5 5]
          :margin [5 5 5 5]}}))

(fn recursive-parent-child-structure
  [[class attributes & children] parent dom]
  (let [element {:class class
                 :attributes attributes
                 :children []
                 :absolute {:t 0 :l 0}
                 :draw-slots
                 {:w 0
                  :h 0
                  :arrangement :stacked
                  :slots []}
                 :parent parent
                 :render {:shadow {}
                          :background {}
                          :outline {}
                          :image {}
                          :text {}}
                 :text ""
                 }]
    (table.insert dom element)
    (local id (# dom))
      (each [i child (ipairs children)]
        (tset element.children i
              (match (type child)
                :table
                (recursive-parent-child-structure child id dom)
                :string (if (= ""element.text)
                            (set element.text child)
                            (set element.text
                             (.. element.text "\n" child)))
                )))
    id))

(fn define-parent-child-structure [ml w h]
  (let [root (when (= :table (type w)) w)
        window  (if root
                    ml
                    ["WINDOW" {:width w :height h
                               :max-wdith w :max-height h}
                     (unpack ml)])
        dom (if root
                [root]
                [])]

    (if root
        (recursive-parent-child-structure window 1 dom)
        (recursive-parent-child-structure window nil dom))
    dom))

(fn inherit-metatable-from-style [dom style]
  (each [_ element (ipairs dom)]
    (let [s (. style (. element :class))]
      (if s
          (setmetatable element.attributes s)
          (setmetatable element.attributes {:__index default-style}))))
  dom)

(macro with [element keys operation]
  `(let [,keys ,element]
     (if ,operation
         ,operation
         ,keys)))

(fn convert-color [hex]
  (fn hex-to-rgb [hex]
    (fn hex-to-value [hex]
      (var ret 0)
      (let [len (string.len hex)]
        (for [i 1 len]
          (let [single (string.lower (string.sub hex i i))
                value (match single
                        "0" 0 "1" 1 "2" 2 "3" 3 "4" 4 "5" 5 "6"6 "7" 7 "8" 8 "9" 9
                        "a" 10 "b" 11 "c" 12 "d" 13 "e" 14 "f" 15)]
            (set ret (+ ret (* value (math.pow 16 (- len i))))))))
      ret)

    (let [r (string.sub hex 2 3)
          g (string.sub hex 4 5)
          b (string.sub hex 6 7)
          a (string.sub hex 8 9)]
      [(/ (hex-to-value r) 256)
       (/ (hex-to-value g) 256)
       (/ (hex-to-value b) 256)
       (/ (if (~= "" a) (hex-to-value a) 256) 256)]))

  (match (type hex)
    :table hex
    :string (hex-to-rgb hex)
    _ [1 1 1 1]))

(fn build-render-description [element]
  (let [at element.attributes
        text element.text
        render element.render]
    (when at.custom-render
      (tset element :custom-render at.custom-render))
    (when at.id
      (tset element :id at.id))
    (when at.on-click
      (tset element :on-click at.on-click))
    (when at.value
      (tset element :value at.value))
    (when at.on-focus
      (tset element :on-focus at.on-focus))
    (when at.on-unfocus
      (tset element :on-focus at.on-unfocus))
    (tset element :hovered false)
    (tset render :offset (with at {: x : y}))

    (tset render
          :margin-box
          (with at
                {: width : height
                 :margin [mt ml mb mr]
                 :padding [t l b r]
                 }
                {:x 0 :y 0
                 :w (+ width ml l mr r)
                 :h (+ height mt t mb b)}))

    (tset render
          :padding-box
          (with at
                {: width : height
                 :margin [mt ml]
                 :padding [t l b r]
                 }
                {:x (+ ml) :y (+ mt)
                 :w (+ width l r) :h (+ height t b)}))

    (tset render
          :content-box
          (with at
                {: width : height
                 :padding [pt pl]
                 :margin [mt ml]
                 }
                {:x (+ pl ml) :y (+ pt mt)
                 :w width :h height}))
    (local {:x cb-x :y cb-y :w cb-w :h cb-h } render.content-box)
    (local {:x pb-x :y pb-y :w pb-w :h pb-h } render.padding-box)
    (tset render
          :custom-render
          (with at {: color : custom-render}
                {: custom-render
                 :color (convert-color color)
                 :x pb-x
                 :y pb-y
                 :w pb-w
                 :h pb-h
                 }))
    (tset render
          :background
          (with at {: color : radius :background render}
                {: render
                 :color (convert-color color)
                 : radius
                 :x pb-x
                 :y pb-y
                 :w pb-w
                 :h pb-h
                 }))
    (tset render
          :background-hover
          (with at {:color-hover hover-color
                    :color color
                    : radius :background render}
                {: render
                 :color (convert-color (or hover-color color))
                 : radius
                 :x pb-x
                 :y pb-y
                 :w pb-w
                 :h pb-h
                 }))
    (tset render
          :outline
          (with at {:outline-color color
                    : radius
                    :outline render
                    :outline-thickness thickness}
                {: render
                 :color (convert-color color)
                 : radius
                 : thickness
                 :x pb-x
                 :y pb-y
                 :w pb-w
                 :h pb-h}))
    (tset render
          :outline-hover
          (with at {:outline-color-hover hover-color
                    :outline-color color
                    : radius
                    :outline render
                    :outline-thickness thickness}
                {: render
                 :color (convert-color (or hover-color color))
                 : radius
                 : thickness
                 :x pb-x
                 :y pb-y
                 :w pb-w
                 :h pb-h}))

    (tset render
          :shadow
          (with at {:shadow-color color
                    : radius
                    :shadow render
                    :shadow-dx dx
                    :shadow-dy dy}
                {: render
                 :color (convert-color color)
                 : radius
                 : dx
                 : dy
                 :x pb-x
                 :y pb-y
                 :w pb-w
                 :h pb-h}))
    (tset render
          :shadow-hover
          (with at {:shadow-color-hover hover-color
                    :shadow-color color
                    : radius
                    :shadow render
                    :shadow-dx dx
                    :shadow-dy dy}
                {: render
                 :color (convert-color (or hover-color color))
                 : radius
                 : dx
                 : dy
                 :x pb-x
                 :y pb-y
                 :w pb-w
                 :h pb-h}))

    (tset render
          :image
          (with at {:image-color color
                    : radius
                    :image-src src
                    :image-quad quad
                    : image-type
                    :image render
                    }
                {: render
                 :color (convert-color color)
                 : radius
                 : src
                 : quad
                 : image-type
                 :x pb-x
                 :y pb-y
                 :w pb-w
                 :h pb-h}))
    (tset render
          :image-hover
          (with at {:image-color color
                    :image-color-hover hover-color
                    : radius
                    :image-src-hover hover-src
                    :image-src src
                    :image-quad-hover hover-quad
                    :image-quad quad
                    :image-type-hover hover-image-type
                    :image-type image-type
                    :image render
                    }
                {: render
                 :color (convert-color (or hover-color color))
                 : radius
                 :src (or hover-src src)
                 :quad (or hover-quad quad)
                 :image-type (or hover-image-type image-type)
                 :x pb-x
                 :y pb-y
                 :w pb-w
                 :h pb-h}))
    (tset render
          :text
          (with at {:text-color color
                    :text-font font
                    :text-alignment alignment}
                {:color (convert-color color) : font : alignment
                 :render (if (and element.text (~= element.text ""))
                             element.text)
                 :x cb-x
                 :y cb-y
                 :w cb-w
                 :h cb-h})
          )
    (tset render
          :text-hover
          (with at {:text-color color
                    :text-color-hover hover-color
                    :text-font font
                    :text-font-hover hover-font
                    :text-alignment-hover hover-alignment
                    :text-alignment alignment}
                {:color (convert-color (or hover-color color))
                 :font (or hover-font font)
                 :alignment (or hover-alignment alignment)
                 :render (if (and element.text (~= element.text ""))
                             element.text)
                 :x cb-x
                 :y cb-y
                 :w cb-w
                 :h cb-h})
          )
    element))

(fn determine-element-size [elements]
  (each [_ element (ipairs elements)]
    (build-render-description element))
  elements)


;; "Draw slots"
;; {
;; :w 100 :h 100
;; :arrangement :stacked ; for column row and grid anchor
;;                      ; and alignment are ignored
;; :slots [{:id :xx :y :w :h :anchor :alignment}]
;; }

(fn insert-element-stacked [index element parent]
  (let [{:w pw :h ph} parent.render.content-box
        {: alignment : anchor} element.attributes
        {:w ew :h eh} element.render.margin-box
        {:x x :y y} element.render.offset
        {: t : l} parent.absolute
        pt (or t 0)
        pl (or l 0)
        slots parent.draw-slots.slots
        pw2 (math.floor (/ pw 2))
        ph2 (math.floor (/ ph 2))
        half 0.5
        anchors {"top" {:x pw2 :y 0}
                 "bottom" {:x pw2 :y ph}
                 "left" {:x 0 :y ph2}
                 "right" {:x pw :y ph2}
                 "center" {:x pw2 :y ph2}
                 "top-left" {:x 0 :y 0}
                 "top-right" {:x pw :y 0}
                 "bottom-left" {:x 0 :y ph}
                 "bottom-right" {:x pw :y ph}}
        alignments {"top" {:x half :y 0}
                    "bottom" {:x half :y 1}
                    "left" {:x 0 :y half}
                    "right" {:x 1 :y half}
                    "center" {:x half :y half}
                    "top-left" {:x 0 :y 0}
                    "top-right" {:x 1 :y 0}
                    "bottom-left" {:x 0 :y 1}
                    "bottom-right" {:x 1 :y 1}}
        align (. alignments alignment)
        anch (. anchors anchor)
        slot {: index
              :w ew
              :h eh
              :x (+
                  x ;; base offset
                  (* ew (* -1 align.x)) ;; alignment
                  anch.x ;; anchoring
                  )
              :y (+
                  y ;; base offset
                  (* eh (* -1 align.y)) ;; alignment
                  anch.y ;; anchoring
                  )
              : alignment
              : anchor
              :arrangement :stacked}
        ]
    (tset element :slots {:w 0
                          :h 0
                          :arrangement :stacked
                          :slots []})

    (table.insert slots slot)
    (tset element :absolute {:t (+ pt slot.y
                                   (. parent :attributes :padding 1)
                                   (. parent :attributes :margin 1)
                                   )

                             :l (+ pl slot.x
                                   (. parent :attributes :padding 2)
                                   (. parent :attributes :margin 2)
                                   )
                             })
    )
  )

(fn insert-element-row [index element parent]
  (let [{:h ph} parent.render.content-box
        {:w ew :h eh} element.render.margin-box
        {:t pt :l pl} parent.absolute
        {:y oy :x ox} element.render.offset
        gutter-x (. parent.attributes.gutter 1)
        slots parent.draw-slots.slots
        alignment element.attributes.alignment
        align (match alignment
                :top 0
                :bottom 1
                :center 0.5
                _ 0
                )
        x (if (> (# slots) 0)
                  (let [{: x : w} (. slots (# slots))]
                    (+ ox x w gutter-x)
                     )
                  ox)
        y (+ oy (* eh -1 align) (* ph align))
        slot {:w ew
              :h eh
              :x x
              :y y
              : alignment
              :arrangement :row}]
    (table.insert slots slot)
    (tset element :absolute {:t (+ pt slot.y
                                   (. parent :attributes :padding 1)
                                   (. parent :attributes :margin 1)
                                   )

                             :l (+ pl slot.x
                                   (. parent :attributes :padding 2)
                                   (. parent :attributes :margin 2)
                                   )
                             })
    )
  )

(fn insert-element-column [index element parent]
  (let [{:w pw} parent.render.content-box
        {:w ew :h eh} element.render.margin-box
        {:t pt :l pl} parent.absolute
        {:y oy :x ox} element.render.offset
        gutter-y (. parent.attributes.gutter 2)
        slots parent.draw-slots.slots
        alignment element.attributes.alignment
                align (match alignment
                :left 0
                :right 1
                :center 0.5
                _ 0
                )
        y (if (> (# slots) 0)
                  (let [{: x : y : w : h} (. slots (# slots))]
                    (+ oy y h gutter-y))
                  oy)
        x (+ ox (* ew -1 align) (* pw align))
        slot {:w ew
              :h eh
              :x x
              :y y
              : alignment
              :arrangement :row}]
    (table.insert slots slot)
    (tset element :absolute {:t (+ pt slot.y
                                   (. parent :attributes :padding 1)
                                   (. parent :attributes :margin 1)
                                   )

                             :l (+ pl slot.x
                                   (. parent :attributes :padding 2)
                                   (. parent :attributes :margin 2)
                                   )
                             })
    ))

(fn insert-element-grid [index element parent]
  (let [{:h ph :w pw} parent.render.content-box
        {:w ew :h eh} element.render.margin-box
        {:t pt :l pl} parent.absolute
        gutter-x (. parent.attributes.gutter 1)
        gutter-y (. parent.attributes.gutter 2)
        slots parent.draw-slots.slots
        [x y] (if (> (# slots) 0)
                  (let [{: x : w : y : h} (. slots (# slots))
                        x-start (+ x w gutter-x)
                        x-end (+ x-start ew)]
                    (if (<= x-end pw)
                        [x-start y]
                        [0 (+ y h gutter-y)]
                        )

                    )
                  [0 0])
        slot {:w ew
              :h eh
              :x x
              :y y
              :arrangement :grid}]
    (table.insert slots slot)
    (tset element :absolute {:t (+ pt slot.y
                                   (. parent :attributes :padding 1)
                                   (. parent :attributes :margin 1)
                                   )

                             :l (+ pl slot.x
                                   (. parent :attributes :padding 2)
                                   (. parent :attributes :margin 2)
                                   )
                             })
    )
  )

(fn set-offset [index element parent]
  (let [{:w pw :h ph} parent.render.content-box
        {: arrangement } element.attributes
        {:w ew :h eh} element.render.margin-box
        slots parent.draw-slots.slots
        parent-arrangement parent.attributes.arrangement]
    ;; Reset the parents slot width and height
    (tset parent.draw-slots :w pw)
    (tset parent.draw-slots :h ph)
    (tset parent.draw-slots :arrangement parent-arrangement)
    ;; fill out element draw-slots
    (tset element.draw-slots :w ew)
    (tset element.draw-slots :h eh)
    (tset element.draw-slots :arrangement arrangement)
    (tset element.draw-slots :slots [])
    ;; fit into parent
    (match parent-arrangement
      :stacked (insert-element-stacked
                index element parent)
      :row (insert-element-row
            index element parent)
      :column (insert-element-column
               index element parent)
      :grid (insert-element-grid
               index element parent)
      )
    ))

(fn determine-element-position [dom]
  (each [index element (ipairs dom)]
    (let [parent (. dom element.parent)]
      (when (> index 1)
        (set-offset index element parent))))
  dom)

(local fml-imui-mt {:__index  fml-imui})

(lambda fml-imui.ml-to-dom [ml w h ?style]
  (let [mstyle (if ?style (define-style ?style) {})]
    (each [name obj (ipairs styles)]
      (when (not (. mstyle name))
        (tset mstyle name obj)))
    (let [elements (-> ml
                       (define-parent-child-structure w h)
                       (inherit-metatable-from-style mstyle)
                       (determine-element-size)
                       (determine-element-position)
                       ;; determine-element-absolute-position
                       )]

      (setmetatable
       {: elements } fml-imui-mt))))

(fn point-within-rect [x y l t w h]
  (and (and (> x l) (< x (+ l w)))
       (and (> y t) (< y (+ t h)))))

(lambda fml-imui.update [dom changelist]
  (local {: render-callbacks : mouse-callback
          : click-callback : hover-callback} dom)
  (var click-caught false)
  (let [(mouse-x mouse-y right left
                 pressed-button
                 released-button)
        (mouse-callback)]
    (each [index element (ipairs dom.elements)]
      (match (and element.id (. changelist element.id))
        ["text" value] (do
                         (tset element :text value)
                         (set element.render.text.render value)
                         (set element.render.text-hover.render value)
                         )
        ["value" value] (do (tset element :value value))
        x (do (pp x))
        ;; move
        ;; replace
        ;; resize
        )
      (when (. changelist element.id)
        (tset changelist element.id nil))
      (when (> index 1)
        (var hovered false)
        (let [{: w : h} element.render.padding-box
              {: t : l} element.absolute]
          (when (point-within-rect mouse-x mouse-y l t w h)
            (set hovered true)
            ;; focus unfocus
            (when (and pressed-button element.on-click
                       (not click-caught))
              (click-callback element pressed-button
                              mouse-x mouse-y)
              (when element.catch-click
                (set click-caught true))
              )))
        (when (~= hovered element.hovered)
          (set element.hovered hovered)
          (if hovered
              (when element.on-focus
                (hover-callback :focus element))
              (when element.on-unfocus
                (hover-callback :unfocus element))))
        (render-callbacks.push-offset element.absolute)

        (if element.custom-render
            (when render-callbacks.custom-render
              (render-callbacks.custom-render
               element.render.custom-render
               {: mouse-x
                : mouse-y
                : left
                : right
                : pressed-button
                : released-button}
               ))
            (do
              (when render-callbacks.shadow
                (render-callbacks.shadow
                 (if hovered
                     element.render.shadow-hover
                     element.render.shadow)))

              (render-callbacks.background
               (if hovered
                   element.render.background-hover
                   element.render.background))

              (when render-callbacks.outline
                (render-callbacks.outline
                 (if hovered
                     element.render.outline-hover
                     element.render.outline)))

              (when render-callbacks.image
                (render-callbacks.image
                 (if hovered
                     element.render.image-hover
                     element.render.image)))

              (if hovered
                  (render-callbacks.text element.render.text-hover)
                  (render-callbacks.text element.render.text))))

        (render-callbacks.pop-offset)))))

(fn fml-imui.draw-debug [dom]
  (let [render-callbacks dom.render-callbacks]
    (each [index element (ipairs dom.elements)]
      (when (> index 1)
        (render-callbacks.push-offset element.absolute)
        (render-callbacks.debug element.render)
        (render-callbacks.text element.render.text)
        (render-callbacks.pop-offset)))))

(lambda fml-imui.init [callbacks]
  (each [key callback (pairs callbacks)]
    (tset fml-imui key callback)))

(lambda fml-imui.resize-index [dom index width height]
  (let [parent (. dom :elements index)
        old-width parent.attributes.width
        old-height parent.attributes.height
        changed (or (~= old-width width) (~= old-height height))]
    (set parent.attributes.width width)
    (set parent.attributes.height height)
    (tset dom.elements 1 (build-render-description parent))
    (set parent.draw-slots {:w 0
                            :h 0
                            :arrangement :stacked
                            :slots []})
    (when (and (= :stacked parent.attributes.arrangement) changed)
      ;; overkill
      (determine-element-position dom.elements))
    ))

(lambda fml-imui.resize-screen [dom canvas width height]
  (fml-imui.resize-index dom 1 width height)
  (fml-imui.set-canvas canvas))

(fn append-stack [dom new-stack parent-index]
  (let [old (. dom :elements)
        parent (. dom :elements parent-index)
        new (. new-stack :elements)
        offset (- (# old) 1)]
    ;; need to shift them in x and y
    (each [index element (ipairs new)]
      (when (> index 1)
        (table.insert old element)
        (for [i 1 (# element.children)]
          (tset element.children i (+ (. element.children i) offset)))
        (if (= 1 element.parent)
            (do
              (tset element :parent parent-index)
              (table.insert  parent.children (# old))
              ;; need to set the draw slots to index 1
              )
            (tset element :parent (+ element.parent offset)))))))

(lambda fml-imui.add-element [dom ml where ?style ?relative]
  (pp ml)
  (let [new-stack
        (dom.ml-to-dom [ml]
                       (. dom :elements 1 :attributes :width)
                       (. dom :elements 1 :attributes :height)
                       ?style)]
    (append-stack dom new-stack 1)))

;; rerender dom -> dom

(lambda fml-imui.remove-element [dom id]
  )

(when _G.love
  (fml-imui.init (require (.. __NAME ".platform.fml-love-callbacks")))
  (tset fml-imui :init nil))

fml-imui
