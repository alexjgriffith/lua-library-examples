(local topdown {})

(macro incf [value ?by]
  `(set ,value (+ ,value (or ,?by 1))))

(macro decf [value ?by]
  `(set ,value (- ,value (or ,?by 1))))

(local controls [[:left] [:right] [:up] [:down]])

(macro get-left []
  `(. controls 1))

(macro set-left [keys]
  `(tset controls 1 ,keys))

(macro get-right []
  `(. controls 2))

(macro set-right [keys]
  `(tset controls 2 ,keys))

(macro get-up []
  `(. controls 3))

(macro set-up [keys]
  `(tset controls 3 ,keys))

(macro get-down []
  `(. controls 4))

(macro set-down [keys]
  `(tset controls 4 ,keys))

(fn topdown.simple [{: x : y : speed}]
  (local {: isDown} love.keyboard)
  (var lr 0)
  (var ud 0)
  (var return-x x)
  (var return-y y)
  (when (isDown (unpack (get-right))) (incf lr))
  (when (isDown (unpack (get-left))) (decf lr))
  (when (isDown (unpack (get-up))) (decf ud))
  (when (isDown (unpack (get-down))) (incf ud))
  (local magnatude (math.sqrt (+ (* lr lr) (* ud ud))))
  (when (~= lr 0)
    (set return-x (+ x (* speed (/ lr magnatude)))))
  (when (~= ud 0)
    (set return-y (+ y  (* speed (/ ud magnatude)))))
  (values return-x return-y))

(fn topdown.set-controls [left right up down]
  (set-left left)
  (set-right right)
  (set-up up)
  (set-down down)
  controls)

topdown
