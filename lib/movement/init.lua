
 local movement = {_DEPENDS = {"lume"}, _DESCRIPTOIN = "2D movement library.", _LICENCE = "\nMIT LICENCE\n\nCopyright (c) 2021 alexjgriffith\n\nPermission is hereby granted, free of charge, to any person obtaining a copy of\nthis software and associated documentation files (the \"Software\"), to deal in\nthe Software without restriction, including without limitation the rights to\nuse, copy, modify, merge, publish, distribute, sublicense, and/or sell copies\nof the Software, and to permit persons to whom the Software is furnished to do\nso, subject to the following conditions:\n\nThe above copyright notice and this permission notice shall be included in all\ncopies or substantial portions of the Software.\n\nTHE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR\nIMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,\nFITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE\nAUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER\nLIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,\nOUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE\nSOFTWARE.", _URL = "https://gitlab.com/alexjgriffith/allibs", _VERSION = "Movement v0.2.0"}


























 local function debug(value) end

 local lume = require("lib.third-party.lume")

 local function check_key_down(key, keys)



 local function _0_(a, b) return (a or b) end if lume.reduce(lume.map(keys[key], love.keyboard.isDown), _0_) then return 1 else return 0 end end



 local function sign0(x)
 if (x == 0) then return 0 elseif (x > 0) then return 1 elseif (x < 0) then return -1 end end



 local function abs_min(_0_0, min) local _arg_0_ = _0_0 local pos = _arg_0_[1] local vel = _arg_0_[2]
 local velp if (math.abs(vel) < min) then velp = 0 else velp = vel end
 return pos, velp end

 local function abs_max(_1_0, max) local _arg_0_ = _1_0 local pos = _arg_0_[1] local vel = _arg_0_[2]
 local velp if (math.abs(vel) > max) then velp = max else velp = vel end
 return pos, velp end


 local function jump_params(vx, h, xh)
 local vel = ((2 * h * vx) / xh)
 local gravity = ((-2 * h * vx * vx) / xh / xh)
 local time = (xh / vx)
 return vel, gravity, time end

 local function update_position(dt, y, vel, acc, accp_3f)

 local accp = (acc or accp_3f)
 local yp = (y + (vel * dt) + (acc * dt * dt * 0.5))
 local velp = (vel + ((acc + accp) * 0.5 * dt))
 return yp, velp, accp end


 local jump = {}





 jump["check-speed"] = function(params)
 return params.vx, params.vy end

 jump.update = function(params, dt, x, y, on_ground_callback, collide_callback)
 local _let_0_ = params local keys = _let_0_["keys"] local keyup = _let_0_["keyup"] local max_vx = _let_0_["max-vx"] local p_state = _let_0_["p-state"] local vx = _let_0_["vx"] local vy = _let_0_["vy"] local x_acc = _let_0_["x-acc"] local x_dec = _let_0_["x-dec"] local yacc = _let_0_["yacc"] local left
 do debug("check-left") left = check_key_down("left", keys) end local right
 do debug("check-left") right = check_key_down("right", keys) end local lr
 do debug("lr") lr = (right - left) end local jump0
 do debug("check-jump") jump0 = check_key_down("jump", keys) end local xp, vxp = nil, nil
 do debug("check-match")
 local _2_0 = lr











































































































































































































































































 if (_2_0 == 0) then xp, vxp = abs_min({update_position(dt, x, vx, (x_dec * ( - sign0(vx))))}, 0.2) elseif (_2_0 == -1) then xp, vxp = abs_max({update_position(dt, x, vx, (x_acc * -1))}, max_vx) elseif (_2_0 == 1) then xp, vxp = abs_max({update_position(dt, x, vx, x_acc)}, max_vx) else xp, vxp = nil end end local timeout do debug("incf-timer") local _2_ do params.timer = (params.timer + (dt or 1)) _2_ = params.timer end local _3_ do local res_0_ = params["p-state"] local function _4_() local res_0_0 = (res_0_).time return (res_0_0 and res_0_0) end _3_ = (res_0_ and _4_()) end timeout = (_2_ > (_3_ or 0)) end local countout do debug("check-count") countout = (params.count > 1) end local on_ground do debug("check-onground") on_ground = on_ground_callback() end local state do debug("determine-state") local _2_0 = {jump0, ((p_state == "jump") or (p_state == "on-ground")), timeout, countout, (vy > 0), on_ground, (keyup or (p_state == "double"))} if ((type(_2_0) == "table") and true and true and true and true and true and ((_2_0)[6] == true) and true) then local _ = (_2_0)[1] local _0 = (_2_0)[2] local _1 = (_2_0)[3] local _2 = (_2_0)[4] local _3 = (_2_0)[5] local _4 = (_2_0)[7] state = "on-ground" elseif ((type(_2_0) == "table") and true and true and true and true and ((_2_0)[5] == false) and ((_2_0)[6] == false) and true) then local _ = (_2_0)[1] local _0 = (_2_0)[2] local _1 = (_2_0)[3] local _2 = (_2_0)[4] local _3 = (_2_0)[7] state = "fall" elseif ((type(_2_0) == "table") and ((_2_0)[1] == true) and ((_2_0)[2] == true) and ((_2_0)[3] == false) and ((_2_0)[4] == false) and true and true and true) then local _ = (_2_0)[5] local _0 = (_2_0)[6] local _1 = (_2_0)[7] state = "jump" elseif ((type(_2_0) == "table") and ((_2_0)[1] == true) and ((_2_0)[2] == false) and ((_2_0)[3] == false) and ((_2_0)[4] == false) and true and true and ((_2_0)[7] == true)) then local _ = (_2_0)[5] local _0 = (_2_0)[6] state = "double" elseif ((type(_2_0) == "table") and ((_2_0)[1] == true) and true and true and ((_2_0)[4] == true) and true and true and true) then local _ = (_2_0)[2] local _0 = (_2_0)[3] local _1 = (_2_0)[5] local _2 = (_2_0)[6] local _3 = (_2_0)[7] state = "release" elseif ((type(_2_0) == "table") and ((_2_0)[1] == true) and true and ((_2_0)[3] == true) and true and true and true and true) then local _ = (_2_0)[2] local _0 = (_2_0)[4] local _1 = (_2_0)[5] local _2 = (_2_0)[6] local _3 = (_2_0)[7] state = "release" else local _ = _2_0 state = "fall" end end local yp, vyp, yaccp = nil, nil, nil do debug("update-postition") if (state == p_state) then yp, vyp, yaccp = update_position(dt, y, vy, yacc) else debug(("Switch State " .. state)) local _let_1_ = params[state] local nacc = _let_1_["gravity"] local nv = _let_1_["vel"] local ret = update_position(dt, y, nv, yacc, nacc) params.yacc = nacc yp, vyp, yaccp = ret end end local hold_p_state = p_state local xa, ya, xva, yva = collide_callback(xp, yp, vxp, vyp) if jump0 then params["key-up"] = false else params["key-up"] = true end do local _3_0 = {state, p_state} if ((type(_3_0) == "table") and ((_3_0)[1] == "jump") and ((_3_0)[2] == "jump")) then elseif ((type(_3_0) == "table") and ((_3_0)[1] == "jump") and ((_3_0)[2] == "double")) then elseif ((type(_3_0) == "table") and ((_3_0)[1] == "double") and ((_3_0)[2] == "double")) then elseif ((type(_3_0) == "table") and true and ((_3_0)[2] == "on-ground")) then local _ = (_3_0)[1] params.count = 0 params.vy = 0 elseif ((type(_3_0) == "table") and ((_3_0)[1] == "jump") and true) then local _ = (_3_0)[2] params.count = (params.count + (nil or 1)) do local _ = params.count end elseif ((type(_3_0) == "table") and ((_3_0)[1] == "double") and true) then local _ = (_3_0)[2] params.count = (params.count + (nil or 1)) do local _ = params.count end end end params.vx = xva params.vy = yva if (state ~= params["p-state"]) then params.timer = 0 end params["p-state"] = state return params, xa, ya, state, hold_p_state end local jump_mt = {__index = jump} jump.init = function(max_vx, x_acc, x_dec, jump_h, jump_xh, release_h_3f, release_xh_3f, fall_h_3f, fall_xh_3f, double_h_3f, double_xh_3f) local release_h = (release_h_3f or jump_h) local release_xh = (release_xh_3f or jump_xh) local fall_h = (fall_h_3f or release_h) local fall_xh = (fall_xh_3f or release_xh) local double_h = (double_h_3f or jump_h) local double_xh = (double_xh_3f or jump_xh) local jump_vel, jump_gravity, jump_time = jump_params(max_vx, jump_h, jump_xh) local release_vel, release_gravity = jump_params(max_vx, release_h, release_xh) local fall_vel, fall_gravity = jump_params(max_vx, fall_h, fall_xh) local double_vel, double_gravity, double_time = jump_params(max_vx, double_h, double_xh) return setmetatable({["key-up"] = true, ["max-vx"] = max_vx, ["on-ground"] = {gravity = 0, vel = 0}, ["p-state"] = "on-ground", ["x-acc"] = x_acc, ["x-dec"] = x_dec, count = 0, double = {gravity = double_gravity, time = double_time, vel = double_vel}, fall = {gravity = fall_gravity, vel = fall_vel}, jump = {gravity = jump_gravity, time = jump_time, vel = jump_vel}, keys = {jump = {"space"}, left = {"left"}, right = {"right"}}, release = {gravity = release_gravity, vel = release_vel}, timer = 0, vx = 0, vy = 0, yacc = 0}, jump_mt) end movement["jump"] = jump return movement
