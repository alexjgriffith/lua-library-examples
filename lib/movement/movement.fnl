(local _NAME ...)

(local __NAME (_NAME:gsub ".movement$" ""))


(local movement
       {:_VERSION "Movement v0.3.0"
        :_DESCRIPTION "2D movement library."
        :_DEPENDS [:lume]
        :_URL "https://gitlab.com/alexjgriffith/lua-library-examples/-/tree/master/lib/movement"
        :_LICENCE "MIT"})

(tset movement :platformer (require (.. __NAME :.platformer)))
(tset movement :topdown (require (.. __NAME :.topdown)))

movement
