(local lume (require :lib.third-party.lume))

(fn calculate-velocity-gravity [dy vx dx]
  (let [vy (/ (* 2 dy (* vx vx)) dx)
        g (/ (* -2 dy (* vx vx)) (* dx dx))]
    (values vy g)))

(fn calculate-velocity-gravity-t [h t]
  (let [v (/ (* 2 h) t)
        g (/ (* -2 h) (* t t))]
    (values v g)))

(fn update-y-pos-vel [pos vel acc dt]
  (let [pos-next (+ pos (*  vel dt) (* 0.5 (- acc) dt dt))
        vel-next (+ vel (* (- acc) dt))]
    (values pos-next vel-next)))

(fn update-x-pos-vel-simple [pos vel dt]
  (values (+ pos (* dt vel)) vel))

{: calculate-velocity-gravity
 : calculate-velocity-gravity-t
 : update-y-pos-vel
 : update-x-pos-vel-simple}
