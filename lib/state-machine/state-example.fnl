(local {: init} (require :state-machine))

(local test-state {:name :test-state
                   :update (fn [state dt])
                   :handle-input
                   (fn [state input]
                     (match input
                       :up (values :replace-state :test-state-2)
                       _ (values nil nil))
                     )})

(local test-state-2 {:name :test-state-2
                     :update (fn [state dt])
                     :handle-input
                     (fn [state input]
                       (match input
                         :down (values :push-state :test-state-3)
                         :up (values :replace-state :test-state)
                         _ (values nil nil))
                       )})

(local test-state-3 {:name :test-state-3
                     :timer 0
                     :update (fn [state dt]
                               (pp "update dt")
                               (pp dt)
                               (tset state :timer (+ state.timer dt)))
                     :exit (fn [state] (tset state :timer 0))
                     :enter (fn [state] (tset state :timer 0))
                     :handle-input
                     (fn [state input]
                       (match input
                         :up (values :pop-state nil)
                         _ (if (> state.timer 1)
                               (values :pop-state nil)
                               (values nil nil)))
                       )
                     })

(var sm (init test-state test-state-2 test-state-3))
(sm:handle-input :up)
(sm:handle-input :down)
(sm:update 0.1)
