(local game-modes {:_AUTHOR "Alexander Griffith"
                   :_COPYRIGHT "(C) Alexander Griffith 2021"
                   :_VERSION "0.1.0"
                   :_DESCRIPTION "A state machine for managing game modes and transitions."
                   :_LICENCE "[GPLV3](https://www.gnu.org/licenses/gpl-3.0.en.html)"})

;; licence has to be GPLV3 since we adapt code from min-love2d-fennel
;; code also adopted from gamestate.lua -- need to think about licence

(local explanation "Press escape to quit.
Press space to return to the previous mode after reloading in the repl.")

(var error-mode-state nil)
(local error-mode {:name :error-mode})

(fn error-mode.draw [mode]
  (love.graphics.clear 0.34 0.61 0.86)
  (love.graphics.setColor 0.9 0.9 0.9)
  (love.graphics.print error-mode-state.msg 10 10)
  (love.graphics.print explanation 15 25)
  (love.graphics.print error-mode-state.traceback 15 50))

(fn error-mode.keypressed [mode key set-mode]
  (match key
    :escape (love.event.quit)
    :space (game-modes.switch error-mode-state.old-mode)))

(fn error-mode.enter [mode _old-mode old-mode msg traceback]
  (print msg)
  (print traceback)
  (set error-mode-state {: old-mode : msg : traceback}))

(fn error-mode.update [mode dt])

(local all-callbacks ["draw" "update"])

(each [key _handler (pairs love.handlers)]
  (table.insert all-callbacks key))

(var mode-name nil)

(local first-state-mt
       {:__index (fn [] (error "Game Modes not initialized"))})

(fn null [])

(local first-state (setmetatable {:leave null} first-state-mt))
(local previously-initialized-states (setmetatable {} {:__mode :k}))
(local state-stack [first-state])
(var dirty-state true)

(var events-registered false)

(fn register-events [?callbacks]
  (let [love-callbacks {}
        callbacks (or ?callbacks all-callbacks)]
    (set events-registered true)
    (each [_ callback (ipairs callbacks)]
      (tset love-callbacks callback (or (. love callback) (fn [])))
      (tset love callback
            (fn [...]
              (let [vals [...]
                    name callback]
                (let [[outcome value]
                      [(xpcall
                        #(do
                           ((. love-callbacks callback) (unpack vals))
                           ((. game-modes callback) (unpack vals)))
                        #(game-modes.switch error-mode
                                            mode-name
                                            $ (if _G.fennel
                                                  (_G.fennel.traceback)
                                                  "")))]]
                  (when (not outcome) (print (.. name " " (or value "missing balue"))))))
              )))))

(fn leave-state [?pre]
  (let [index (# state-stack)
        pre (or ?pre (. state-stack index))]
    ((or pre.leave null) pre)))

(fn initialize-state [to ...]
  (let [fun (or (. previously-initialized-states to)
                to.init
                null)]
    (fun to ...)
    (tset previously-initialized-states to null)))

(fn change-state [change to ...]
  (let [index (# state-stack)
        pre (. state-stack index)]
    (when (not events-registered) (register-events))
    (match change
      :switch (do
                (leave-state)
                (initialize-state to ...)
                (tset state-stack index to)
                ((or to.enter null) to pre ...))
      :push (do
              (initialize-state to ...)
              (tset state-stack (+ (# state-stack) 1) to)
              ((or to.enter null) to pre ...))
      :pop  (do
              (leave-state)
              (tset state-stack index nil)))
    (set dirty-state true)
    (let [current-state (. state-stack (# state-stack))]
      (set mode-name current-state.name)
      current-state)))

(fn push [to-name ...]
  (assert to-name "Missing argument")
  (match (type to-name)
    :string (let [to (require to-name)]
              (tset to :name to-name)
              (change-state :push to ...))
    :table (change-state :push to-name ...)))

(fn pop [_to-name ...]
  (assert (> (# state-stack) 1)  "No State to pop!")
  (change-state :pop nil ...))

(fn switch [to-name ...]
  (assert to-name "Missing argument")
  (match (type to-name)
    :string (let [to (require to-name)]
              (tset to :name to-name)
              (change-state :switch to ...))
    :table (change-state :switch to-name ...)
    _ (error "Expecting string or table.")))

(fn safely [f to-name ...]
  (let [vals [...]]
    (match (pcall #(f to-name (unpack vals)))
      (false msg)
      (do (print (.. "Error Switching to: "
                     (match (type to-name)
                       :string to-name
                       :table to-name.name)) "\n"
                       msg)
          (love.event.quit)))))

(lambda game-modes.pop [...]
  "Pop off current state."
  (safely pop "popping" ...))

(lambda game-modes.switch [to-name ...]
  "Replace state at the top of state stack with TO-NAME.

TO-NAME can be a table or a the name of a module."
  (safely switch to-name ...))

(lambda game-modes.push [to-name ...]
  "Push state TO-NAME to the top of the state stack.

Does not call leave on the previous state.
TO-NAME can be a table or a the name of a module."
  (safely push to-name ...))

(lambda game-modes.current []
  "Return the state on the top of the state-stack."
  (. state-stack (# state-stack)))

(lambda game-modes.register-events [?callbacks]
  "Register love events with game-modes.

By default, all love events are shadowed by game-mode.
If you only want to wrap arround a selection of events,
provide them as an array via ?CALLBACKS."
  (register-events ?callbacks))

(fn pass-through [_ func]
  (if (or (not dirty-state) (= func :update))
      (do (set dirty-state false)
          (fn [...]
            (let [index (# state-stack)
                  current-state (. state-stack index)
                  fun (or (. current-state func) null)]
              (fun current-state ...))))
    null))

(setmetatable game-modes {:__index pass-through})

game-modes
