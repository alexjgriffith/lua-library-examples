(local map {})

(local ic (require :interface-callbacks))

(fn clear-create-cache [cache w h]
  (if (and cache cache.valid)
      cache
      (and cache cache.canvas)
      (do (ic.erase-canvas cache.canvas) cache)
      (do
        {:canvas (ic.create-canvas w h)
         :valid false
         ;; :name (.. zone-name layer)
         })))

(fn clear-create-layer-cache [map layer]
  (if (not map.cache)
      (tset map :cache {}))
  (if (not map.cache.layers)
      (tset map.cache :layers {}))
  (let [cache (. map :cache :layers layer)]
    (tset map.cache.layers layer (clear-create-cache cache map.lw map.lh))))

(fn clear-create-zone-layer-cache [map zone-name layer]
  (if (not map.cache)
      (set map.cache {}))
  (if (not map.cache.zones)
      (set map.cache.zones {}))
  (if (not (. map.cache.zones zone-name))
      (tset map.cache.zones zone-name {}))
  (let [cache (. map :cache :zones zone-name layer)
        zone (. map :cache :zones zone-name)]
    (tset zone layer (clear-create-cache cache map.w map.h))))

(fn render-paint [pallet data tw th]5
  (each [j row (ipairs data)]
    (each [i value (ipairs row)]
      ((. (. pallet i) :render-editor) (. pallet i) i j tw th))))

(fn render-zone-layer [map zone-name layer]
  (let [zone (. map :zones zone-name)
        data (. zone :layers layer)
        meta-data (. map :layers layer)
        {: tw : th} meta-data
        pallet (. map :pallets meta-data.pallet)
        cache (clear-create-zone-layer-cache map zone-name layer)]
    (match cache.valid
      true cache
      false (do
             (ic.set-canvas cache.canvas)
             (match meta-data.type
               :paint (render-paint pallet data tw th))
             (ic.unset-canvas)
             (tset cache :valid true)))
    cache))

(fn map.render-layer [map layer]
  (when map.render-zones
    (let [layer-cache (clear-create-layer-cache map layer)
          {: lx : ly : lw : lh : lox : loy : rw : rh} map]
      (match layer-cache.valid
        true  :pass
        false (do
                (ic.set-canvas layer-cache.canvas)
                (each [_ zone-name (ipairs map.render-zones)]
                  (let [zone-cache (map.render-zone map zone-name layer)
                        fx0 (* map.w (- (map :zone-name :x) 1))
                        fy0 (* map.h (- (map :zone-name :y) 1))]
                    ;; this wont work with objects that go above the
                    ;; tile height
                    (ic.render-canvas-section
                     layer-cache.canvas
                     zone-cache.canvas
                     (+ lx fx0)
                     (- ly fy0)
                     )
                    ))
                (ic.unset-canvas)
                (tset layer-cache :valid true)))
      {:canvas layer-cache.canvas : lx : ly : lw : lh : lox : loy : rw : rh})))

(fn map.render [map ret?]
  (var ret (or ret? {}))
  (each [name _layer (pairs map.layers)]
    (tset ret name (map.render-layer map name)))
  ret)


(fn initialize-layer [layer]
  (var data nil)
  (match layer.type
    :paint (do (set data [])
               (for [j 1 layer.th]
                 (tset data j [])
                 (for [i 1 layer.tw]
                   (tset data j i layer.default))))
    )
  data)

(fn add-zone [map zone-name zx zy]
  (let [zone {:x zx :y zy :layers {}}
        layers (. map :layers)]
    (each [name layer (pairs layers)]
      (tset zone.layers name (initialize-layer layer)))
    (tset map.zones zone-name zone)
    ))

(fn pos-to-zonexy [map pos]
  ;; pos = editor space
  ;; zone,z,y = map space
  (let [l (- map.x map.rw)
        t (- map.y map.rh)
        {: x : y} pos
        px (+ l x)
        py (+ t y)
        zx (math.ceil (/ px map.w))
        zy (math.ceil (/ py map.h))
        zone (.. zx "," zy)
        fx (- px (* map.w (- zx 1)))
        fy (- py (* map.h (- zy 1)))
        ]
    (values (if (. map :zones zone) zone (do (add-zone map zone zx zy) zone)) fx fy)))

(fn xy-to-ij [map x y layer]
  (let [meta (. map :layers layer)
        {: tilesize} meta
        tx (math.ceil (/ x tilesize))
        ty (math.ceil (/ y tilesize))
        ]
    (values tx ty))
  )

(fn colour-to-index [map colour layer]
  (var index nil)
  (let [pallet (. map :pallets (. map :layers layer :pallet))]
    (each [i c (ipairs pallet)]
      (when (= colour c.name)
        (set index i)))
    )
  index)

(fn map.replace [map colour-index layer pos]
  (let [(zone x y) (pos-to-zonexy map pos)
        (px py) (xy-to-ij map x y layer)
        index colour-index]
    (when (and zone index)
      (when (and (. map :zones zone) (. map :zones zone :layers layer))
        (let [location (. map :zones zone :layers layer py)]
          (tset location px index))
        ))))

(fn map.swap-pallet [map pallet])

(fn map.set-render-size [map rw rh]
  (set map.rw rw)
  (set map.rh rh)
  (set map.cache nil)
  map)

(fn map.set-active-size [map aw ah]
  (set map.aw aw)
  (set map.ah ah)
  (set map.cache nil)
  map)

(fn map.move-pointer [map dx dy]
  (if (and map.x map.y)
      (do
        (set map.x (+ map.x dx))
        (set map.y (+ map.y dy)))
      (do
        (set map.x dx)
        (set map.y dy)))
  ;; Identify the bounds of the zones that will be rendered
  (local ll (* map.w (math.floor (/ (- map.x map.rw) map.w))))
  (local lr (* map.w (math.ceil (/ (+ map.x map.rw) map.w))))
  (local lu (* map.h (math.ceil (/ (+ map.y map.rh) map.h))))
  (local ld (* map.h (math.floor (/ (- map.y map.rh) map.h))))
  ;; Determine the width and hight of the renderable area
  (local lw (- lr ll))
  (local lh (- lu ld))
  ;; Determine the offset between the renderable area and
  ;; the area requested
  (local lox (- ll (- map.x map.rw)))
  (local loy (- lu (+ map.y map.rh)))
  (local lx ll)
  (local ly lu)
  ;; save the old lx and ly to see if the layer cache needs
  ;; to be invalidated
  (local old-lx map.lx)
  (local old-ly map.ly)
  (when (and (or (~= lx old-lx) (~= ly old-ly))
             map.cache
             map.cache.layers
             )
    ;; invalidate layer cache if the outer frame has moved
    (each [name cache (pairs (. map :cache :layers))]
      (tset cache :valid false)))
  ;; set map values
  (tset map :lx lx)
  (tset map :ly ly)
  (tset map :lw lw)
  (tset map :lh lh)
  (tset map :lox lox)
  (tset map :loy loy)
  ;; render zones get all zones within render range
  (var new-render-zones [])
  (let [l (math.floor (/ (- map.x map.rw) map.w))
        r (math.ceil (/ (+ map.x map.rw) map.w))
        u (math.ceil (/ (+ map.y map.rh) map.h))
        d (math.floor (/ (- map.y map.rh) map.h))]
    (for [i l r]
      (for [j u d]
           (let [zone-name (.. i "," j)
                 zone (. map :zones zone-name)]
             (when zone
               (table.insert new-render-zones zone-name)
               )))))

  ;; free any zone caches that are no longer active
  (var zone-range-changed false)
  (when (and map.cache map.cache.zones)
    (let [to-invalidate {}]
      (each [_key zone-name (ipairs map.render-zones)]
        (tset to-invalidate zone-name true))
      (each [_key zone-name (ipairs new-render-zones)]
        (tset to-invalidate zone-name nil))
      (each [zone-name _value (pairs to-invalidate)]
        (each [_layer-name cache (pairs (. map :cache :zones zone-name))]
          ;; free each inactive layer
          (set cache.valid false)
          (ic.free-canvas cache.canvas)
          (set cache.canvas nil))
        (set zone-range-changed true))))
  (when zone-range-changed
    (set map.render-zones new-render-zones))

  ;; active zones get all zones within active range
  (var new-active-zones [])
  (let [l (math.floor (/ (- map.x map.aw) map.w))
        r (math.ceil (/ (+ map.x map.aw) map.w))
        u (math.ceil (/ (+ map.y map.ah) map.h))
        d (math.floor (/ (- map.y map.ah) map.h))]
    (for [i l r]
      (for [j u d]
        (let [zone-name (.. i "," j)
              zone (. map :zones zone-name)]
          (when zone (table.insert new-active-zones zone-name))))))
  (set map.active-zones new-active-zones)
  map)

(local map-mt {:__index map})

(fn map.init [state x y rw rh aw ah]
  (let [ret (setmetatable state map-mt)]
    (ret:set-render-size rw rh)
    (if (and aw ah)
        (ret:set-active-size aw ah)
        (ret:set-active-size rw rh))
    (ret:move-pointer x y)
    ret))

map
