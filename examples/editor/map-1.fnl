{:n 1
 :m 1
 :w 208 ;;px
 :h 208 ;;px
 :pallets {:pallet1 (require :pallet1)}
 :layers  {:ground {:pallet :pallet1
                    :static true
                    :compactible true
                    :tilesize 16
                    :tw 13
                    :th 13
                    :default 0
                    :type :paint
                    :dense true}
           :road {:pallet :pallet1
                  :static true
                  :compactible true
                  :tilesize 16
                  :tw 13
                  :th 13
                  :default 0
                  :type :paint
                  :dense true}
  }
 :zones {}
 }
