;; (sm:handle-input (fn [] (values outcome callback _pos)))
;; Examples
;; (sm:handle-input (fn [] (values :click-button (change-brush editor-state) _pos)))
;; (sm:handle-input (fn [] (values :click-map (brush editor-state start end) pos)))
;; (sm:update dt pos)
;; (sm:handle-input (fn [] (values :release nil pos)))

(local state-machine (require :lib.state-machine.state-machine))

;; (local editor-state (require :editor-state))

(fn null-update [_state _dt _pos])

(fn null-input [_state _input])

(fn region-update [state _dt pos]
  (set state.end-pos pos))

(fn open-input [state input]
  (let [(outcome callback pos) (input)]
    (tset state :callback callback)
    (tset state :pos pos)
    (tset state :outcome outcome)
    (match outcome
      :click-button (values :push-state :immediate)
      :click-map (values :replace-state :region)
      :escape (values :replace-state :exit))))

(fn open-enter [state _previous-state]
  (set state.callback nil)
  (set state.pos nil)
  (set state.outcome nil))

(fn open-exit [state]
  (when (= state.outcome :exit)
    (state.callback))
  (set state.oucome nil))

(fn region-input [state input]
  (let [(outcome _callback pos) (input)]
    (match outcome
      :release (do (set state.end-pos pos)
                   (set state.success true)
                   (values :replace-state :open))
      :escape (do
                (set state.success false)
                (values :replace-state :open)))))

(fn region-enter [state previous-state]
  (if previous-state.pos
      (do (set state.start-pos previous-state.pos)
          (set previous-state.pos nil)))
  (if (and previous-state.callback (= (type previous-state.callback) :function))
      (do (set state.callback previous-state.callback)
          (set previous-state.callback nil))))

(fn region-exit [state]
  (when state.success
    (state.callback state.start-pos state.end-pos))
  (set state.pos nil)
  (set state.callback nil)
  (set state.success false))

(fn immediate-input [_state _input] (values :pop-state))

(fn immediate-enter [state previous-state]
  (when previous-state.callback
    (previous-state.callback)
    (set previous-state.callback nil)))

(fn editor-states []
  [{:name :open
    :callback nil
    :pos nil
    :update null-update
    :handle-input open-input
    :enter open-enter
    :exit open-exit
    }
   {:name :region
    :start-pos nil
    :end-pos nil
    :callback nil
    :success false
    :update region-update
    :handle-input region-input
    :enter region-enter
    :exit region-exit
    }
   {:name :immediate
    :update null-update
    :handle-input immediate-input
    :enter immediate-enter
    }
   {:name :exit
    :update null-update
    :handle-input null-input
    }
   ]
  )

(local sm (state-machine.init (unpack (editor-states))))

sm
