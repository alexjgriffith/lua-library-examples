(local defaults {})

(fn defaults.keypressed [state key code]
  (match key
    :escape (love.event.quit)))

defaults
