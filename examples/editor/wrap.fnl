(local repl (require :lib.stdio.stdio))

(global lume (require :lib.third-party.lume))

(local gamestate (require :lib.third-party.gamestate))

(fn love.load []
  (gamestate.registerEvents)
  (gamestate.switch (require :mode-ui-test))
  (repl.start :lib/stdio))
