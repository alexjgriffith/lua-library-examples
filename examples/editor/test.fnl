(local fennel (require :lib.fennel))
(global pp (fn [x] (print (fennel.view x))))
(global love false)

(pp "Testing the editor sans love")

(local editor (require :editor-interactions))
(local sm (require :editor-state-machine))

(fn switch-colour-index [colour-index]
  (fn []
    (values :click-button
            (fn []
              (pp (.. ">> Switching brush to " colour-index))
              (editor:switch-colour colour-index))
            colour-index)))

(fn switch-layer [layer-name]
  (fn []
    (values :click-button
            (fn []
              (pp (.. ">> Switching layer to " layer-name))
              (editor:switch-layer layer-name))
            layer-name)))

(fn draw-region [start-pos]
  (fn []
    (values :click-map
            (fn [start end]
              (pp (.. ">> Draw Region start: " start.x "," start.y " end: " end.x "," end.y))
              (editor:replace-rectangle nil start end))
            start-pos)))

(fn release [end-pos]
  (fn [] (values :release nil end-pos)))

(sm:handle-input (switch-colour-index 2))
(sm:handle-input (release {:x 0 :y 0}))

(sm:handle-input (draw-region {:x 220 :y 240}))
(sm:handle-input (release {:x 240 :y 240}))

(sm:handle-input (switch-layer :road))
(sm:handle-input (release {:x 0 :y 0}))

(sm:handle-input (draw-region {:x 220 :y 240}))
(sm:handle-input (release {:x 240 :y 240}))

(sm:handle-input (switch-layer :ground))
(sm:handle-input (release {:x 0 :y 0}))

(sm:handle-input (switch-colour-index 1))
(sm:handle-input (release {:x 0 :y 0}))

(sm:handle-input (draw-region {:x 220 :y 240}))
(sm:handle-input (release {:x 440 :y 240}))

(pp editor.state.map.zones)
