(fn render-canvas-section [target-canvas source-canvas x y]
  (if love
      (do (love.graphics.push "all")
          (love.graphics.setCanvas target-canvas)
          (love.graphics.draw source-canvas x y)
          (love.graphics.pop))
      (print "Render Canvas Section")))

(fn render-colour-square [rgba]
  (if love
      (fn [i j tw th]
        (love.graphics.push "all")
        (love.graphics.setColor rgba)
        (love.graphics.rectangle :fill (* (- i 1) tw) (* (- j 1) tw) tw th)
        (love.graphics.pop))
      (fn [] (print "Render Colour Square Callback"))))

(fn set-canvas [canvas]
  (if love
      (do (love.graphics.setCanvas canvas))
      (do (print "Set Canvas") nil)))

(fn unset-canvas []
  (if love
      (do (love.graphics.setCanvas))
      (do (print "Unset Canvas"))))

(fn create-canvas []
  (if love
      (love.graphics.newCanvas)
      (do (print "Create Canvas") nil)))

(fn free-canvas [canvas]
  (if love
      (canvas:release)                  ; since a bunch of these canvasas
                                        ; are going to be the same size
                                        ; we should think about pooling
                                        ; zone canvases, and layer canvases
      (print "Free Canvas")))

(fn erase-canvas [canvas]
  (if love
      (let [current-canvas (love.graphics.getCanvas)]
        (love.graphics.setCanvas canvas)
        (love.graphics.clear)
        (love.graphics.setCanvas current-canvas))
      (print "Erase Canvas")))

{: render-colour-square : set-canvas : unset-canvas : create-canvas : erase-canvas
 : render-canvas-section : free-canvas}
