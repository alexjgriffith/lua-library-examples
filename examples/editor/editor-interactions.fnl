(local editor {})

(local state (require :editor-state))

(fn editor.colour-at-pos [pos])

(fn editor.replace [editor colour pos]
  (editor.state.map:replace (or colour editor.state.colour) editor.state.layer pos))

(fn editor.replace-rectangle [editor colour start end]
  ;; needs to convert to tile space first
  (for [px (math.min start.x end.x) (math.max start.x end.x)]
    (for [py (math.min start.y end.y) (math.max start.y end.y)]
      (editor:replace colour {:x px :y py}))))

(fn editor.replace-line [editor colour start end])

(fn editor.replace-fill [editor colour start end])

(fn editor.switch-layer [editor layer]
  (tset editor.state :layer layer))

(fn editor.switch-colour [editor colour]
  (tset editor.state :colour colour))

(fn editor.display-region [editor x y w h]
    (pp editor.map.zones))

(fn editor.xy-to-ij [editor x y])

(fn init [map-data layer]
  (local map (require :map))
  (local which-map 1)
  (set editor.state state)
  (set state.init true)
  (set state.maps [(map.init map-data 0 0 200 200)])
  (set state.map-index 1)
  (set state.map (. state.maps state.map-index))
  (set state.layer layer)
  (set state.tilesize (. state :map :layers layer :tilesize))
  (set state.pallet (. state :map :layers layer :pallet))
  (set state.colour 0)
  editor)

;; need to clean this up
(init (require :map-1) :ground)
