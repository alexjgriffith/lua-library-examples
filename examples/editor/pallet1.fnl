(local ic (require :interface-callbacks))
[{:name "blue"
  :render-editor (ic.render-colour-square [0 0 1 1])}
 {:name "red"
  :render-editor (ic.render-colour-square [1 0 0 1])}
 {:name "green"
  :render-editor (ic.render-colour-square [0 1 0 1])}]
