(local test (lume.clone (require :interface-defaults)))

(local bump (require :lib.third-party.bump))

(local ui ["DIV"
           {:max-width 100
            :color [1 1 1 1]
            :text-color [1 1 1 1]}
           "Some Text Content"])

;; ML to DOM
;; First Pass build parent children associations
;; Second Pass Attributes are inherited from parents
;; Third Pass the sizes and positions of each element are hard coded

;; Render
;; Work Down the parent tree
;; Attributes:

;; needs to be immediate mode for games
;; I want to build in anchoring and centering as first class
;; aspects

;; effective multi linked list of all children to parents
;; work through the children, calculate min / max size vs natural
;; size of text or image
{1 {:tag :div :attributes attributes
    :children [2] :parents [] :contents nil
    :display {:x :y :width :height :padding [] :margin []}}
 2 {:tag :text :attributes nil :parents [2]
    :contents "Some Text Content"
    :display {:x :y :width :height :padding [] :margin []}}}

(fn test.init [test]
  (love.window.setTitle "UI TEST"))

(fn test.draw [test]
  ;; (love.graphics.printf "UI TEST"  0 10 600 :center)
  )

(fn test.update [test dt])

(pp test)

test
