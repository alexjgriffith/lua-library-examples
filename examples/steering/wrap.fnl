(local repl (require :lib.stdio.stdio))
(local lume (require :lib.third-party.lume))

(local steering (require :steering))

(local {:topdown {:simple move}} (require :lib.movement.movement))

(local {: objs : mode : target} (require :state))

(local white  [1 1   1 1])
(local red    [1 0   0 1])
(local yellow [1 0.7 0 1])
(local blue   [0 0   1 1])
(local black  [0 0   0 1])

(fn toggle-mode []  
  (tset mode 1 (match (. mode 1)
                 :add (do (tset target :active true) :move)
                 :move (do (tset target :active false) :add)
                 )))

(fn create-obj [x y colour t]
  (local obj {:colour colour : x : y :r 30 :type t
              :speed 5
              :nonce-x (math.random) :nonce-y (math.random)
              :probes []
              :index 0})  
  (table.insert objs obj)
  (steering.init obj (# objs)))

(fn create-yellow [x y]
  (create-obj x y yellow :yellow))


(fn create-blue [x y]
  (create-obj x y blue :blue))

(fn create-red [x y]  
  (create-obj x y red :red))

(macro incf [x by]
  `(do (set ,x (+ ,x ,(or by 1))) ,x))

(fn move-obj-random [dt obj]  
  (incf obj.x (* 2 (- (love.math.noise (incf obj.nonce-x dt)) 0.5)))
  (incf obj.y (* 2 (- (love.math.noise (incf obj.nonce-y dt)) 0.5))))

(fn move-obj-to-target [dt obj]
  (local {: lerp} lume)
  (let [{: x : y} obj
        {:x tx :y ty} target]
    ;; decelerates because x and tx get
    ;; closer together
    (tset obj :x (lerp x tx (* 3 dt)))
    (tset obj :y (lerp y ty (* 3 dt)))))

(fn move-obj [dt obj]
  (match obj.type
    :yellow (let [(dx dy) (move obj)]
                  (tset obj :x dx)
                  (tset obj :y dy))
    _ (if (and (= :blue obj.type) (= target.active true))
      (move-obj-to-target dt obj)
      (move-obj-random dt obj))))

(fn love.load []
  (create-yellow 100 100)
  (repl.start :lib/stdio))

(fn love.draw []
  (local  {: rectangle : circle : setColor} love.graphics)
  (local (sw sh) (love.window.getMode))
  (setColor white)
  (rectangle :fill 0 0 sw sh)
  (each [index {: x : y : r : colour} (ipairs objs)]
    (setColor colour)
    (rectangle :fill x y r r 5)
    (setColor black)
    (rectangle :line x y r r 5)))

(fn love.update [dt]
  (each [index obj (ipairs objs)]
    (move-obj dt obj)))

(fn love.mousepressed [x y button]
  (match (. mode 1)
    :add (match button
           1 (create-blue (- x 15) (- y 15))
           2 (create-red (- x 15) (- y 15)))
    :move  (do (tset target :x x) (tset target :y y))))

(fn love.keypressed [key code]
  (match key
    :space (toggle-mode)
    :escape (love.event.quit)))
