(local fennel (require :lib.fennel))

(local steering (require :steering))

(var verbose true)

(fn test [name assertion string]
  (match (pcall #(assert assertion))
    false (.. name " (failed) "  (if verbose (or string "") ""))
    true (.. name " (passed) ")))

(fn compare-eq [name a b]
  (pp (test name (= (fennel.view a) (fennel.view b))
            (fennel.view ["=" a b]))))

(fn compare-not-eq [name a b]
  (pp (test name (~= (fennel.view a) (fennel.view b))
            (fennel.view ["~=" a b]))))

(compare-not-eq
 "make-point-not"
 (steering.make-point 1 0 :sample)
 {:group "sample" :pos {:x 0 :y 0}})

(compare-eq
 "make-point"
 (steering.make-point 1 1 :sample)
 {:group "sample" :pos {:x 1 :y 1}})

(test "is-point" (: (steering.make-point 1 2 :sample) :is-point))

(compare-eq
 "make-group"
 (steering.make-group-description :a {:b "#1"} {} {:c "#1"})
 {:avoid-table {:c "#1"} :flee-table {:b "#1"} :goal-table {} :name "a"})

(local group-descriptions
       {:a (steering.make-group-description :a {:b #1} {} {:c #1})
        :b (steering.make-group-description :b {} {:a #1} {:c #1})})

(local identity #$)

(local point (steering.make-point 0 0 :a))

(local other-points [(steering.make-point 10 0 :b)
                     (steering.make-point -10 0 :b)
                     (steering.make-point 0 10 :b)])

(point:get-move-direction other-points group-descriptions identity)
