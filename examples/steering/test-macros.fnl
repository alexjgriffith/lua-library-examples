(fn test [name assertion]  
  `(pp [,name (pcall #(assert ,assertion ,(fennel.view assertion)))]))

{: test}
