(local steering {})

(local vector (require :lib.third-party.vector))

(local lume (require :lib.third-party.lume))

(local zero-vector (vector 0 0))
(local vector-type (getmetatable zero-vector))

(local point {})

(fn map3! [fun t1 t2 t3]
  (for [i 1 (# t1)]
    (tset t1 i (fun (. t1 i) (. t2 i) (. t3 i))))
  t1)

(fn map2! [fun t1 t2]
  (for [i 1 (# t1)]
    (tset t1 i (fun (. t1 i) (. t2 i))))
  t1)

(fn min-mask! [t]
  (var min (. t 1))
  (for [i 2 (# t)]
    (when (< (. t i) min)
      (set min (. t i))))
  (for [i 1 (# t)]
    (if (> (. t i) min)
        (tset t i 0)
        (tset t i 1)))
  t)

(fn invert! [t]
  (for [i 1 (# t)]
    (tset t i (- (. t i))))
  t)

(local directions [[0 1] [1 1] [1 0] [1 -1] [0 -1] [-1 -1] [-1 0] [-1 1]])

(fn probe-dot [p1 p2 weight-fun]
  (let [weight (if weight-fun (weight-fun (p1:distanceSquared p2)) 1)
        v2 (vector.normalized (- p1 p2))]
    (lume.map
     (fn [d]
       (let [v1 (vector.normalized (- p1 d))]
         (* weight (vector.dot v1 v2))))
     directions)))

(fn point.get-move-direction [point other-points group-descriptions shape-fun]  
  (var (goal avoid flee) (values nil nil nil))
  (local group (. group-descriptions point.group))
  (each [_index op (ipairs other-points)]
    (let [probes (probe-dot point op (. group op.group :weight))
          add (fn [a b] (+ a b))]
      (match (. group op.group :name)
        :goal (if goal (map2! add goal probes) (set goal probes))
        :avoid (if avoid (map2! add avoid probes) (set avoid probes))
        :flee (if flee (map2! add flee (invert! probes))
                  (set flee (invert! probes))))))
  (let [probes (shape-fun directions
                          (map3! (fn [a b c] (* c (+ a b)) )
                                 goal flee (min-mask! avoid)))]
    (var index 1)
    (var max (. probes 1))
    (for [i 2 (# probes)]
      (when (> (. probes i) max)
        (set index i)
        (set max (. probes i))))
    (let [[x y] (. directions index)]
      (vector.normalize (vector x y)))))

;; take weighted average for final direction of > +/- 1 top direction

(fn point.is-point [point]
  (and point.group point.pos (= (getmetatable point.pos) vector-type)))

(fn steering.make-point [x y group ?special-goal]
  (setmetatable {:pos (vector x y)  : group :special-goal ?special-goal} {:__index point}))

(fn steering.make-group-description [name flee-table goal-table avoid-table]
  ;; {:group {:name [:goal :avoid :flee] :weightfn (distance2)}
  {: name : flee-table : goal-table : avoid-table})

steering
