
;; Inspiration
;;  https://www.youtube.com/watch?v=6BrZryMz-ac
;; http://www.gameaipro.com/GameAIPro2/GameAIPro2_Chapter18_Context_Steering_Behavior-Driven_Steering_at_the_Macro_Scale.pdf


;; voids and stigs

;; move to this point, keep away from this point, keep in the same direction
;; as these points.

;; dot product between direction of and sensor the target
;; weighing weights, shaping function to dot product
;; Seperation logic, move away form near entity at an slight angle


;; algorithm
;; select all points / lines within bounding box
;; for all targets
;; for all voids,
;; determine the lowest and zero create a mask that zeros out anything above that.

;; (local {: dot : normalized} (require :lib.third-party.vector))

(fn point [obj]
  (values obj.x obj.y))

(fn subtract [ax ay bx by] (values (- ax bx)  (- ay by)))

(fn dot [ax ay bx by] (+ (* ax bx) (* ay by)))

(fn normalize [x y]
  (let [m (^ (+ (^ x 2) (^ y 2)) 0.5)]
    (if (~= m 0)
        (values (/ x m) (/ y m))
        (values 0 0))))

(fn norm-vector [a b]
  (let [(ax ay) (point a)
        (bx by) (point b)]
    (normalize (subtract ax ay bx by))))

(fn dot [ax ay bx by] (+ (* ax bx) (* ay by)))

(let [(ax ay) (norm-vector {:x 10 :y 10} {:x 11 :y 10})
      (bx by) (norm-vector {:x 10 :y 10} {:x 11 :y 11})]
  (dot ax ay bx by))

(fn reduce [fun table init]
  (var accum init)
  (each [_ value (ipairs table)]
    (set accum (fun accum value)))
  accum)

(fn dot-sum [a b-col indices direction]
  (let [b-count (# indices)
        (cx cy) (norm-vector a direction)]
    (reduce (fn [accum index]
              (+ accum (/ (dot cx cy (norm-vector a (. b-col index))) b-count)))
            indices 0)))

(fn dot-sum-norm [norm-col norm-direction]
  (let [b-count (# norm-col)
        [cx cy] norm-direction]
    (reduce (fn [accum [bx by]]
              (+ accum (/ (dot cx cy bx by) b-count)))
            norm-col 0)))

(local directions [[0 1] [1 1] [1 0] [1 -1] [0 -1] [-1 -1] [-1 0] [-1 1]])

(fn mask [goal mask]
  (var min (. mask 1))
  (each [i value (ipairs mask)]
    (when (< value max)
      (set max value)))
  (each [i value (ipairs mask)]
    (when (> value min)
      (tset goal i 0)))
  goal)

(fn update-probes [b-col groups]
  (each [_ col (ipairs b-col)]
    (let [probes []
          avoid []
          flee []]
      (each [index direction (ipairs directions)]        
        (table.insert avoid (dot-sum col b-col (. groups col.avoid) direction))
        (table.insert flee (invert (dot-sum col b-col (. groups col.flee) direction)))
        (table.insert probes (dot-sum col b-col (. groups col.goal) direction)))
    (tset col :probes (mask probes flee)))))

;; this algorithm seems super slow.
;; Is there any way to deal with lines? Think bounding box, or building wall
;; use weighting fn for distance for flee
;; give each object a type then probes will be like
;; {:type-a [] :type-b []}
;; we can then say use type-a as a mask type-b as the goal
;; we can also wave shape the goal and flee types
;; {:mask :goal :flee}
;; [direction] -> pass through shaping function? eg abs perpindicular

(update-probes [{:x 0 :y 0 :flee :a :approach :c :type :a}
                {:x 0 :y 1 :flee :a :approach :c :type :a}
                {:x 0 :y 2 :flee :a :approach :c :type :a}
                {:x 0 :y 4 :flee :a :approach :c :type :a}
                {:x 5 :y 5 :flee :a}])

(fn init [obj index]
  (tset obj :index index)
  (for [i 1 8]
    (table.insert obj index 0)))

(fn update [objs]
  ;; n^2 algo
  (each [index obj (ipairs objs)]
    (when (~= obj.index index)
      :pass
      ;; update the 8 probes + for goal - for obstacle / boundary
      )
    )
  )

{ : init}
