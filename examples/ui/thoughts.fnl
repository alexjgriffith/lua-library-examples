;; this would be cool, but a library would be better

(local ui {:type :container
           :parameters {:x 50 :y 50 :w 128 :h 128}
           :children [
                      {:type :text :value "Hello"}
                      ]
           })

(local styles {:text {:margin {:l 5 :r 5 :t 5 :b 5}
                      :padding {:l 0 :r 0 :t 0 :b 0}
                      :align :center
                      :border true}})

(fn build [element parent-parameters window-parameters dx dy]
  ;; we always need w,h,x,y,padding,margin,border
  (let [pp parent-parameters
        wp window-parameters
        p element.parameters
        padding (or p.padding style.padding {:l 0 :r 0 :t 0 :b 0})
        margin (or p.margin style.margin {:l 0 :r 0 :t 0 :b 0})
        w (if p.w p.w
              pp.w (- pp.w padding.l padding.r margin.l margin.r)
              style.w
              wp.w
              )
        h (if p.h p.h
              pp.h (- pp.h padding.t padding.b margin.t margin.b)
              style.h
              wp.h
              )]

    (when element.children
      (each [_ child (ipairs element.children)]
        (build child p)
        )))
  )

(fn draw-text [text parent-parameters]
  (let [pp parent-parameters
        style (. styles :text)
        p text.parameters
        text text.value
        align (or p.align style.align)
        padding (or p.padding style.padding)
        margin (or p.margin style.margin)
        w (if p.w p.w
              pp.w (- pp.w padding.l padding.r margin.l margin.r)
              style.w
              )
        h (if p.h p.h
              pp.h (- pp.h padding.l padding.r margin.l margin.r)
              style.w
              )
        ]
    (love.graphics.push)
    (love.graphics.printf text x y w align)
    (love.graphics.pop)))

(local draw-dispatch {:container draw-container
                      :text draw-text})

(fn draw-container [container parent-parameters]
  (let [p container.parameters
        c container.children]
    (love.graphics.push)
    (love.graphics.translate p.x p.y)
    (each [key value (ipairs c)]
      ((. draw-dispatch c.type) value p))
    (love.grapihcs.pop))
  )
