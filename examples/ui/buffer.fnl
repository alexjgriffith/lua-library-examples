;; (macro apply [t fun] `(,fun ,((or unpack table.unpack) t)))

;; (fn reduce [t fun init?]
;;   (let [init (or init? (. t 1))
;;         start (if init? 1 2)]
;;     (var accu init)
;;     (for [i start (# t)]
;;       (set accu (fun accu (. t i))))
;;     accu))

;; (fn map [t fun]
;;   (let [ret []]
;;     (for [i 1 (# t)]
;;       (table.insert ret (fun (. t i))))
;;     ret))

;; (fn range [start? end? t? fun?]
;;     (let [start (if end? (or start? 1) 1)
;;           end (or end? start? (# t))
;;           ret []]
;;       (for [i start end]
;;         (table.insert ret (if (and t? fun?) (fun? (. t? i))
;;                               t? (t? i)
;;                               i)))
;;       ret))

;; (fn slice [t start? end?]
;;   (let [start (if end? (or start? 1) 1)
;;         end (or end? start? (# t))
;;         ret []]
;;     (for [i start end]
;;       (table.insert ret (. t i))
;;     )
;;   ret))

;; (fn concat [t]
;;   (reduce t (fn [x y] (.. x y))))


;; (local buffer {})

;; (fn gen-chars [text font point max-width]
;;   (var line-width 0)
;;   (var line 0)
;;   (var x 0)
;;   (let [ret []
;;         temp []
;;         height (font:getHeight text)]
;;     ;; what do we do with newlines?
;;     (each [char _value (string.gmatch text ".")]
;;       (when key
;;         (match key
;;           " "
;;           "\n"
;;           _ (let [width (font:getWidth key)]
;;               (table.insert ret {:x x :y 0 :w width :h height :c key})
;;               (set x (+ x width))
;;           ))
;;         )
;;       )

;;   ))

;; (local buffer-mt {:__index buffer})

;; (fn buffer.init [font text? point?]
;;   (let [rendered (or text? "")
;;         point (or point? 1)]
;;     (setmetatable {: font
;;                    : rendered
;;                    :chars (gen-chars text font point)
;;                    : point}
;;                   buffer-mt)))

;; buffer


;; text-box
;; (local textbox {})

;; (tset textbox :style {:colour [1 0 0 1]
;;                       :colour-focus [0.5 0 0 1]
;;                       :colour-background [0 0 0 1]
;;                       :colour-text [1 1 1 1]
;;                       :radius 5
;;                       :padding 5
;;                       :w 232 :h 64 :x 0 :y 0
;;                       :focused false
;;                       :selected false
;;                       :visible true
;;                       :buffer-front []
;;                       :buffer-back []
;;                       :text ""
;;                       :font false
;;                       :type :text-box})

;; (fn textbox.draw [self]
;;   (when self.visible
;;     (local (r g b a) (love.graphics.getColor))
;;     (love.graphics.setColor self.background-colour)
;;     (love.graphics.rectangle :fill self.x self.y (+ self.w (* 2 self.padding)) (+ (* 2 self.padding) self.h))
;;     (local font (love.graphics.getFont))
;;     (when self.font (love.graphics.setFont self.font))
;;     (love.graphics.setColor self.text-colour)
;;     (love.graphics.print self.text (+ self.x self.padding) (+ self.y self.padding))
;;     (love.graphics.setFont font)
;;     (love.graphics.setColor r g b a)))

;; (fn textbox.moveto [self x y]
;;   (set self.x x)
;;   (set self.y y))

;; (fn textbox.update [self dt])

;; (fn textbox.click [self x y]
;;   (tset self :selected true)
;;   (let [dx (- (+ self.x self.padding) x)
;;         dy (- (+ self.y self.padding) y)]
;;     (self.font:getWidth (.. (unpack )))
;;     )
;;   )

;; (fn textbox.release [self ...]
;;   (tset self :selected false))

;; (fn textbox.focus [self]
;;   (set self.focused true))

;; (fn textbox.unfocus [self]
;;   (set self.focused false))

;; (local textbox-mt {:__index textbox})

;; (fn textbox.init [b]
;;   (let [style (overwrite textbox.style (or b.style []))
;;         ret (setmetatable (overwrite style (if b.init (b:init)  b)) textbox-mt)]
;;     ret))
