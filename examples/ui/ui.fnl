;; goal, super simple buttons sliders text with fades

(local bump (require :lib.third-party.bump))
(local patchy (require :lib.third-party.patchy))
(local flux (require :lib.third-party.flux))

;; utility
(fn overwrite [a b]
  (local ret {})
  (each [key value (pairs a)]
    (tset ret key value))
  (each [key value (pairs b)]
    (tset ret key value))
  ret)

;; button
(local button {})

(tset button :style {:colour [1 0 0 1]
                     :colour-focus [0.5 0 0 1]
                     :text-colour [1 1 1 1]
                     :text-colour-focus [1 1 1 1]
                     :radius 5
                     :padding 5
                     :align :center
                     :w 128 :h 64 :x 0 :y 0
                     :callback (fn [] :nil) :focused false :text :Button
                     :type :button
                     :visible true})

(fn button.draw [self]
  ;; draw button
  (when self.visible
    (local (r g b a) (love.graphics.getColor))
    (local font (love.graphics.getFont))
    (love.graphics.setColor (if self.focused self.colour-focus self.colour))
    (love.graphics.rectangle :fill self.x self.y self.w self.h self.radius)
    ;; draw text
    (when self.font (love.graphics.setFont self.font))
    (love.graphics.setColor (if self.focused self.text-colour-focus self.text-colour))
    (love.graphics.printf self.text self.x (+ self.y self.padding) self.w self.align)
    (love.graphics.setFont font)
    (love.graphics.setColor r g b a)))

(fn button.update [self dt]
  (when self.flux
    (self.flux:update dt)))

(fn button.click [self ...]
  (self:callback ...))

(fn button.release [self ...])

(fn button.moveto [self x y]
  (set self.x x)
  (set self.y y))

(fn button.move [self ...]
  (self:focus))

(fn button.focus [self]
  (when (and (not self.focused) self.focus-callback)
    (self:focus-callback))
  (set self.focused true))

(fn button.unfocus [self]
  (set self.focused false))

(local button-mt {:__index button})

(fn button.init [b]
  (let [style (overwrite button.style (or b.style []))
        ret (setmetatable (overwrite style (if b.init (b:init)  b)) button-mt)]
    ;; (when (and ret.target-x ret.target-x ret.ticks)
    ;;   (set ret.flux (flux.group))
    ;;   (let [transition (ret.flux:to ret (* 60 ret.ticks) {:x ret.target-x :y ret.target-x})]
    ;;     (transition:ease "linear"))
    ;;   )
    ret))


;; text
(local text {})

(tset text :style {:colour [0 0 0 1]
                   :w 128 :h 64 :x 0 :y 0
                   :align :left
                   :font false
                   :focused false
                   :padding 5
                   :text :Text
                   :visible true
                   :type :text})

(fn text.draw [self]
  (when self.visible
    (local (r g b a) (love.graphics.getColor))
    (local font (love.graphics.getFont))
    (when self.font (love.graphics.setFont self.font))
    (love.graphics.setColor self.colour)

    (love.graphics.printf self.text self.x (+ self.y self.padding) self.w self.align)
    (love.graphics.setFont font)
    (love.graphics.setColor r g b a)))

(fn text.update [self dt])

(fn text.click [self ...])

(fn text.release [self ...])

(fn text.moveto [self x y]
  (set self.x x)
  (set self.y y))

(fn text.move [self ...])

(fn text.focus [self]
  (set self.focused true))

(fn text.unfocus [self]
  (set self.focused false))

(local text-mt {:__index text})

(fn text.init [t]
  (let [style (overwrite text.style (or t.style []))
        ret (setmetatable (overwrite style (if t.init (t:init)  t)) text-mt)]
    ret))

;; slider
(local slider {})

(tset slider :style {:colour [1 0 0 1]
                     :colour-focus [0.5 0 0 1]
                     :colour-background [0 0 0 1]
                     :radius 5
                     :padding 5
                     :max 100
                     :min 1
                     :value 1
                     :position 0
                     ;; :range-min 32
                     ;; :range-width 2
                     :cw 32 :ch 64
                     :w 232 :h 64 :x 0 :y 0
                     :callback (fn [] :nil)
                     :focused false
                     :selected false
                     :visible true
                     :type :slider})

(fn slider.draw [self]
  (when self.visible
    (local (r g b a) (love.graphics.getColor))
    (local font (love.graphics.getFont))
    ;; (print (fennel.view self))
    ;; draw outside
    (love.graphics.setColor (if (or self.selected self.focused)
                                self.colour-focus self.colour))
    (love.graphics.rectangle :fill self.x self.y self.w self.h self.radius)
    ;; clear center
    (love.graphics.setColor self.colour-background)
    (love.graphics.rectangle :fill (+ self.padding self.x)
                             (+ self.padding self.y)
                             (- self.w (* 2 self.padding))
                             (- self.h (* 2 self.padding)) self.radius)
    ;; draw mid
    (love.graphics.setColor (if (or self.selected self.focused)
                                self.colour-focus self.colour))
    (love.graphics.rectangle :fill (+ self.position self.x)
                             (+ self.y (math.floor (/ (- self.h self.ch) 2)))
                             self.cw self.ch self.radius)
    (love.graphics.setFont font)
    (love.graphics.setColor r g b a)))

(fn slider.moveto [self x y]
  (set self.x x)
  (set self.y y))

(fn slider.update [self dt])

(fn slider.click [self x y]
  (tset self :selected true)
  (self:move x y))

(fn slider.release [self ...]
  (tset self :selected false))

(fn slider.move [self x _y]
  (when self.selected
    (set self.position (math.min (- self.w (* 1 self.padding) self.cw)
                                 (math.max 0
                                           (- x self.padding self.x))))
    (let [value (+ self.min
                   (* (- self.max self.min) (/ self.position
                                               (- self.w (* 1 self.padding) self.cw))))]
    (when (~= value self.value)
      (set self.value value)
      (self:callback value))))
  (self:focus))

(fn slider.focus [self]
  (set self.focused true))

(fn slider.unfocus [self]
  (set self.focused false))

(local slider-mt {:__index slider})

(fn slider.init [b]
  (let [style (overwrite slider.style (or b.style []))
        ret (setmetatable (overwrite style (if b.init (b:init)  b)) slider-mt)]
    ret))

;; Scene
(local scene {})

(tset scene :dispatcher {:text text.init
                         :button button.init
                         :slider slider.init
                         ;; :textbox textbox.init
                         })

(fn scene.update [self dt ...]
  (each [i object (ipairs self.objects)]
    (object:update dt ...)))

(fn scene.draw [self]
  (each [i object (ipairs self.objects)]
    (object:draw)))

(fn scene.mousepressed [self x y button ...]
  (each [_ obj (ipairs (self.world:queryPoint x y))]
    (obj:click x y)))

(fn scene.mousereleased [self x y button ...]
  (each [_ obj (ipairs self.objects)]
      (obj:release)))

(fn scene.add [self name init-fun]
  (assert (not (. self.dispatcher name)))
  (tset self.dispatcher name init-fun))

(fn scene.mousemoved [self x y]
  (local new [])
  (each [_ obj (ipairs (self.world:queryPoint x y))]
    (tset new obj true)
    (obj:move x y))
  (each [_ obj (ipairs self.objects)]
    (when (not (. new obj))
      (obj:unfocus))))

(fn scene.keypressed [self key ...])

(fn scene.keyreleased [self key ...])

(fn move-part [self name x y]
  (var obj nil)
  (each [key value (ipairs self.objects)]
    (when (and value.name (= value.name name))
      (set obj value)))
  (when obj
    (self.world:update obj x y)
    (obj:moveto x y)))

(fn move-scene [self x y]
  (let [dx (- self.x x)
        dy (- self.y y)]
    (set self.x x)
    (set self.y y)
    (each [key obj (ipairs self.objects)]
      (self.world:update obj (+ obj.x dx) (+ obj.y dy))
      (obj:moveto (+ obj.x dx) (+ obj.y dy)))))

(fn scene.moveto [self name? x? y?]
  (match y?
    nil (let [x name? y x?] (move-scene self x y))
    _ (move-part self name? x? y?)))

(local scene-mt {:__index scene})

(fn scene.init [objects x? y?]
  (let [ret {:world (bump.newWorld) :objects []
             :x (or x? 0) :y (or y? 0)}]
    (each [_ object (pairs objects)]
      (let [obj ((. scene.dispatcher object.type) object)]
        (set obj.x (+ obj.x ret.x))
        (set obj.y (+ obj.y ret.y))
        (ret.world:add obj obj.x obj.y obj.w obj.h)
        (table.insert ret.objects obj )))
    (setmetatable ret scene-mt)))

scene
