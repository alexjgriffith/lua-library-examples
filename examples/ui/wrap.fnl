(local scene (require :ui))

(fn fit-height [button]
  (assert (and button.font button.text button.padding))
  (let [height
        (+ button.padding button.padding (button.font:getHeight button.text))]
    (tset button :h height)
    button))

(fn fit-width [button]
  (assert (and button.font button.text button.padding))
    (let [width
        (+ button.padding button.padding (button.font:getWidth button.text))]
      (tset button :w width)
    button))

(var sc nil)

(set sc (scene.init [{:type :button :x 10 :y 10 :w 140 :text "Hello Button"
                      :callback (fn [] (sc:moveto 30 30))
                      :focus-callback (fn [x] (print "Focused"))
                      :padding 20
                      :font (love.graphics.newFont "assets/inconsolata.otf" 30)
                      :init (fn [x] (-> x fit-height fit-width))}
                     {:type :slider :name :sample-slider :x 300 :y 200 :callback (fn [_ value] (print (fennel.view value)))}
                     {:type :slider :name :sample-slider :x 300 :y 280 :callback (fn [_ value] (print (fennel.view value)))}


                     {:type :text :x 300 :y 80 :w 140 :h 20 :text "Some Text" :colour [ 1 1 1 1]
                      :padding 20
                      :font (love.graphics.newFont "assets/inconsolata.otf" 30)
                      :init (fn [x] (-> x fit-height fit-width))
                      }]))



(fn love.draw []
  (sc:draw))

(fn love.update [dt]
  (sc:update))

(fn love.mousepressed [x y button]
  (sc:mousepressed x y button))

(fn love.mousereleased [x y button]
  (sc:mousereleased x y button))

(fn love.mousemoved [x y]
  (sc:mousemoved x y))

(fn love.resize [w h]
  (let [x (math.floor (/ (- 600 w) 2))
        y (math.floor (/ (- 400 h) 2))]
   (sc:moveto x y)))

(fn love.keydown [key] (love.event.quit))
