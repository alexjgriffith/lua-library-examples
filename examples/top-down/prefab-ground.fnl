(local state (require :state))
(local lume (require :lib.third-party.lume))

(fn rep [v n]
  (let [r []]
    (for [i 1 n]
      (table.insert r v))
    r))

(local default
       {:name :ground
        :pos {:x 0 :y 0}
        :type :ground
        :size {:w 10 :h 10}
        :tiles (rep :g 100)
        :tile-size 32
        :canvas false
        :debug-colours {:b [0 0 1 1]
                        :r [1 0 0 1]
                        :w [1 1 1 1]
                        :y [1 0 1 1]
                        :g [0 1 0 1]}})

(local ground {})

(fn ground.update [ground dt])

(fn ground.set-debug [ground]
  (fn xy-to-index [i j w]
    (+ 1 (- j 1) (* (- i 1) w)))
  (love.graphics.push :all)
  (love.graphics.setCanvas ground.canvas)
  (for [i 1 ground.size.w]
    (for [j 1 ground.size.h]
      (let [letter (. ground.tiles (xy-to-index i j 10))]
        (love.graphics.setColor (or (?. ground.debug-colours letter) [1 1 1 1]))
        (love.graphics.rectangle :fill
                                 (* (- i 1) ground.tile-size)
                                 (* (- j 1) ground.tile-size)
                                 32
                                 32))))
  (love.graphics.pop))

(fn ground.draw [{:pos {: x : y} : canvas}]
  (love.graphics.draw canvas x y))

(local ground-mt {:__index ground})

(fn [x y ?k]
  (let [return {}]
    (each [key flat-table (pairs default)]
      (match (type flat-table)
        :table (tset return key (lume.clone flat-table))
        _ (tset return key flat-table)))
    (set return.canvas (love.graphics.newCanvas (* 32 10) (* 32 10)))
    (set return.pos.x x)
    (set return.pos.y y)
    (when ?k (set return.tiles (rep ?k 100)))
    (ground.set-debug return)
    (setmetatable return ground-mt)    
    (table.insert state.objects return)
    (tset state.key-object-index :ground (length state.objects))
    return))
