(local lume (require :lib.third-party.lume))

(local default
       {:name :player
        :pos {:x 0 :y 0}
        :size {:w 32 :h 32}
        :type :player
        :state :idle
        :colour [1 1 0 1]})

(local player {})

(fn player.update [player dt]
  (local camera (require :camera))
  (local top-down (require :top-down))
  (local input (require :input))
  (local camera (require :camera))  
  (top-down player input.down-state)
  (camera.update dt :perfect-track player))

(fn player.draw [player]
  (love.graphics.push)
  (love.graphics.setColor player.colour)
  (love.graphics.rectangle :fill player.pos.x player.pos.y 32 32)
  (love.graphics.pop))

(local player-mt {:__index player})

(fn [x y]
  (local state (require :state))
  (let [return {}]
    (each [key flat-table (pairs default)]
      (match (type flat-table)
        :table (tset return key (lume.clone flat-table))
        _ (tset return key flat-table)))
    (set return.pos.x x)
    (set return.pos.x y)
    (setmetatable return player-mt)
    (table.insert state.objects return)
    (tset state.key-object-index :player (length state.objects))
    return))
