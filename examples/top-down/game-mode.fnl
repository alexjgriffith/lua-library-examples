(local input (require :input))
(local camera (require :camera))
(local state (require :state))
(local ui (require :ui))

(fn init []
  (local prefab-player (require :prefab-player))
  (local prefab-ground (require :prefab-ground))    
  (prefab-ground 0 0 :r)
  (prefab-ground 0 -320 :w)
  (prefab-ground -320 -320 :g)
  (prefab-ground -320 0 :y)
  (prefab-player 0 0))

(fn draw [obj]
  ;; need to sort before we draw
  (camera.clear)
  (camera.capture)
  (each [i k (ipairs state.objects)]    
    (k:draw))
  (camera.draw)
  (ui.draw))

(fn update [obj dt]
  (input.update dt)  
  (each [i k (ipairs state.objects)]
    (when k.update
	(k:update dt))))

(fn resize [w h]
  (camera.resize))

{: init : draw : update : resize}
