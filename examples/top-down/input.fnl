(local down-state {:down false :left false :up false :right false :action false :interact false :special false})
(local triggered-state {:down false :left false :up false :right false :action false :interact false :special false})
(local keys {:down [:down :s]
             :left [:left :a]
             :up [:up :w]
             :right [:right :d]
             :action [:space :space]
             :interact [:e :e]
             :special [:r :r]})
(local key-index [:down :left :up :right :action :interact :special])

(local mouse-pos {:x 0 :y 0})

(local mouse-down {:left false :right false})
(local mouse-triggered {:left false :right false})

(fn check-down-triggered [name [opt-1 ?opt-2]]
  (let [down (or (love.keyboard.isDown opt-1)
                 (love.keyboard.isDown (or ?opt-2 opt-1)))
        previous (. down-state name)
        triggered (and down (not previous))]
        (tset down-state name down)
    (values down triggered)))

(fn update-keys []
  (each [index key (ipairs key-index)]
    (local (down triggered) (check-down-triggered key (. keys key)))
    (tset down-state key down)
    (tset triggered-state key triggered)))

(fn update-mouse []
  (local (x y) (love.mouse.getPosition))
  (tset mouse-pos :x x)
  (tset mouse-pos :y y)
  (local left (love.mouse.isDown 1))
  (local right (love.mouse.isDown 2))
  (tset mouse-triggered :left (and left (not mouse-down.left)))
  (tset mouse-triggered :right (and right (not mouse-down.right)))
  (tset mouse-down :left left)
  (tset mouse-down :right right))

{: down-state
 : triggered-state
 : mouse-pos
 : mouse-down
 : mouse-triggered
 :update 
 (fn [obj]
   (update-keys)
   (update-mouse))}
