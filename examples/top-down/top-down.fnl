(local sqrt2 (math.sqrt 2))

(fn bool-to-number [bool]
  (if bool 1 0))

(fn simple-movement [obj down-state]
  (let [{: x : y} obj.pos
        {: left : right : up : down} down-state
        dx (- (bool-to-number right) (bool-to-number left))
        dy (- (bool-to-number down) (bool-to-number up))
        (dxp dyp)
        (match (values dx dy)
          (0 0) (values 0 0)
          (0 _) (values 0 dy)
          (_ 0) (values dx 0)
          _ (values (/ dx sqrt2) (/ dy sqrt2)))]
    (set obj.pos.x (+ x dxp))
    (set obj.pos.y (+ y dyp))
  ))

(fn [obj ...]
  (simple-movement obj ...))
