(fn draw []  
  (local (w h) (love.window.getMode))
  (love.graphics.push :all)
  (love.graphics.setColor 0.2 0.4 0.2 1)
  (love.graphics.rectangle :fill 0 0 w 30)
  (love.graphics.pop)
  )

(fn update [dt])

{: draw : update}
