(local repl (require :lib.stdio.stdio))
(local gamestate (require :lib.third-party.gamestate))
(local game-mode (require :game-mode))

(fn love.load []
  (gamestate.registerEvents)
  (gamestate.switch game-mode)
  (repl.start :lib/stdio))
