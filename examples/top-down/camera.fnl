(var scale 2)
(var x 0)
(var y 0)
(var (w h _) (love.window.getMode))
(var w2 (math.floor (/ w 2)))
(var h2 (math.floor (/ h 2)))
(var camera-canvas (love.graphics.newCanvas w h))

(fn perfect-track [dt {: pos : size}]
  (set x (- (/ w2 scale) pos.x (math.floor (/ size.w 2))))
  (set y (- (/ h2 scale) pos.y (math.floor (/ size.h 2)))))

(local camera-types {: perfect-track})

(fn update [dt camera-type ...]
  ((. camera-types camera-type) dt ...))

(fn capture []  
  (love.graphics.push :all)
  (love.graphics.setCanvas camera-canvas)
  (love.graphics.scale scale)
  (love.graphics.translate x y))

(fn draw []
  (love.graphics.pop)
  (love.graphics.setCanvas)
  (love.graphics.draw camera-canvas))

(fn clear []
  (love.graphics.push :all)
  (love.graphics.setCanvas camera-canvas)
  (love.graphics.clear)
  (love.graphics.pop))

(fn resize []
  (set (w h) (love.window.getMode))
  (set w2 (math.floor (/ w 2)))
  (set h2 (math.floor (/ h 2)))
  (set camera-canvas (love.graphics.newCanvas w h)))

(fn set-scale [s]
  (set scale s))

{: update
 : capture
 : draw
 : clear
 : resize
 : set-scale
 }
