(local repl (require :lib.stdio.stdio))

;; Really need to generalize
(local square-circle (require :square-circle))

(local points1 (square-circle.generate-points))

(local points (square-circle.generate-points 100 100 20 0 0 32 32 64 64))

(local icon (love.graphics.newImage :assets/icon-64.png))

(local icon (love.graphics.newImage :assets/icon-64.png))

(local mesh (love.graphics.newMesh points :fan))

(mesh:setTexture icon)

(fn love.load []
  (repl.start :lib/stdio))

(var r 0)

(var x 100)
(var y 100)
(local w 120)
(local h 170)

(fn b2 [x] (math.floor (/ x 2)))

(fn love.draw []
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.rectangle :fill 0 0 1000 1000)
  (love.graphics.push)
  (love.graphics.translate (+ x (b2 w)) (+ y (b2 h)))
  (love.graphics.rotate r)
  (love.graphics.translate (- (+ x (b2 w))) (- (+ y (b2 h))))
  (love.graphics.setColor 0.1 0.1 0.1 1)
  (love.graphics.rectangle :fill x y w h 10)
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.draw mesh (+ x ( - 60 32)) (+ y 10))
  (love.graphics.printf "Wheel" x (+ y 64 15) w :center)
  (love.graphics.printf "This is how we roll!" (+ x 10) (+ y 64 15 20) (- w 20) :left)
  
  (love.graphics.pop))

(fn love.update [dt]
  ;; (set r (+ r dt))
  )

(fn love.keypressed [key code]
  (love.event.quit))
