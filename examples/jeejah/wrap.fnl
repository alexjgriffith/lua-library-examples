;;(pp fennel.view)
;;(pp fennel.eval)

(local jeejah (require :lib.third-party.jeejah))
(var coro nil)

(fn love.load []
  (set coro (jeejah.start 8001 {:debug true :lib.fennel true})))

(fn love.draw [])

(fn love.update [dt]
  (coroutine.resume coro))

(fn love.keypressed [key code]
  (love.event.quit))
