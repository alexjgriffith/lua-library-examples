(local repl (require :lib.stdio.stdio))

(local noisemap (require :lib.noisemap))

(var map nil)
(var f 0.03)
(var w 200)
(var h 100)
(var key 19)
(var colour-1 "#2d5e0f")
(var colour-2 "#20501f")
(var gray false)

(fn update-and-render-noise [fin]
  (set f (math.max 0 fin))
  (set map (if gray
               (noisemap.generate-gray key w h f)
               (noisemap.generate-colour key w h f colour-1 colour-2))))

(fn love.load []
  (love.graphics.setDefaultFilter "nearest" "nearest")
  (update-and-render-noise f)
  (when  (~= "Windows" (love.system.getOS))
    ;; Getting an io error in windows with the repl
    (repl.start :lib/stdio)))

(fn love.draw []
  (love.graphics.push "all")
  (love.graphics.scale 10)
  (if map (love.graphics.draw map))
  (love.graphics.pop)
  (love.graphics.setColor 0 0 0 1)
  (love.graphics.rectangle :fill 0 0 120 40)
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.printf (.. "Frequency: " f) 10 10 100))

(fn love.update [dt])

(fn love.keypressed [key code]
  (match key
    "w" (update-and-render-noise (+ f 0.01))
    "s" (update-and-render-noise (- f 0.01))
    "p"  (when map
           (let [canvas (love.graphics.newCanvas w h)]
             (love.graphics.push)
             (love.graphics.setCanvas canvas)
             (love.graphics.draw map)
             (love.graphics.setCanvas)
             (love.graphics.pop)
             (let [image (canvas:newImageData)]
               (image:encode "png" "map.png"))))
    "escape" (love.event.quit)))
