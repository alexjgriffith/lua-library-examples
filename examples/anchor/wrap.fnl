(local repl (require :lib.stdio.stdio))

(local game-modes (require :lib.game-modes.game-modes))

(fn love.load []
  (game-modes.switch :menu)
  (repl.start :lib/stdio))

(fn love.keypressed [key code]
  (match [key]
    :escape (love.event.quit)))
