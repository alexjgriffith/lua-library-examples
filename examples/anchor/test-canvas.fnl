(local anchor (require :lib.anchor))

(local game-modes (require :lib.game-modes.game-modes))

(global _v {:x false})
(fn _G.debug-anchor []
  (pp {:size (anchor.stack-size)
       :content (anchor.stack-state)}))

(local (cw ch) (values 1000 1000))
(local canvas (love.graphics.newCanvas cw ch))
(local quad (love.graphics.newQuad 100 25 200 100 cw ch))

(local canvas2 (love.graphics.newCanvas cw ch))
(local quad2 (love.graphics.newQuad 50 0 380 280 cw ch))

(local canvas3 (love.graphics.newCanvas cw ch))
(local quad3 (love.graphics.newQuad 50 50 200 200 cw ch))

(local test {})

(var fndr true)

(local red   [1 0 0 1])
(local green [0 1 0 1])
(local blue [0 0 1 1])
(local yellow [1 1 0 1])

(fn test.window [x y]
  (local (sw sh) (love.window.getMode))
  (fn add-rectangle [color ?x ?y ?callback]
    (anchor.push-element (or ?x 0) (or ?y 0) 100 100)
    (when ?callback
      (?callback))
    (love.graphics.setColor color)
    (love.graphics.rectangle :fill 0 0 100 100)
    (love.graphics.setColor 0 0 0 1)
    (love.graphics.rectangle :line 0 0 100 100)
    (match (anchor.over x y)
      false :pass
      (true mx my) (do
                     (love.graphics.setColor 1 1 1 1)
                     (love.graphics.rectangle :fill 0 0 100 100)
                     (love.graphics.push :all)
                     (love.graphics.reset)
                     (love.graphics.setColor 1 1 1 1)
                   (love.graphics.rectangle :fill (- sw 120) 10 110 40 5)
                   (love.graphics.setColor 0 0 0 1)
                   (love.graphics.printf (.. "MX:" mx ", MY:" my) (- sw 110) 10 100)
                   (love.graphics.pop)
                   ))
  (anchor.pop))
  (anchor.push-window 50 50 canvas quad)  
  (anchor.push-column 0 0 400 200)
  (anchor.push-row 0 0 400 100)
  (add-rectangle red)
  (add-rectangle blue)
  (add-rectangle yellow)
  (add-rectangle green)
  (anchor.pop)
  (anchor.push-row 0 0 400 100)  
  (add-rectangle green)
  (add-rectangle red )
  (add-rectangle blue)
  (add-rectangle yellow)
  (anchor.pop)
  (anchor.pop)
  (anchor.pop canvas quad)


  (anchor.push-window 200 200 canvas2 quad2)  
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.rectangle :fill 0 0 1000 1000)
  (add-rectangle green)
  (anchor.push-window 100 0 canvas3 quad3)
  (love.graphics.setColor 0 0 0 1)
  (love.graphics.rectangle :fill 0 0 1000 1000)
  (add-rectangle red 100 100)
  (anchor.pop canvas3 quad3)
  (anchor.pop canvas2 quad2)
  
  
  )

(fn back [x y pressed]
  (anchor.push-anchor nil nil nil nil :bottom-left)
  (anchor.push-row 10 -10 300 20 :left 10)
  (anchor.push-element 0 0 100 20)  
  (match (anchor.over x y)
    false (love.graphics.setColor red)
    true (do (love.graphics.setColor blue)
             (when pressed
               (game-modes.switch :menu))))  
  (love.graphics.rectangle :fill 0 0 100 20 5)
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.rectangle :line 0 0 100 20 5)
  (love.graphics.printf "back" 0 2 100 :center)
  (anchor.pop)

  (anchor.push-element 0 0 100 20)  
  (match (anchor.over x y)
    false (love.graphics.setColor red)
    true (do (love.graphics.setColor blue)
             (when pressed
               (game-modes.switch :test))))  
  (love.graphics.rectangle :fill 0 0 100 20 5)
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.rectangle :line 0 0 100 20 5)
  (love.graphics.printf "test" 0 2 100 :center)
  (anchor.pop)
  (anchor.pop)
  (anchor.pop)
  )

(var pressed false)
(fn test.draw [state]
  (local (x y) (love.mouse.getPosition))
  (test.window x y)
  (back x y pressed)
  (set pressed false))

(fn test.mousepressed [state x y button]
  (set pressed true))

(fn test.update [state dt])

test
