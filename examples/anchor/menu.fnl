(local anchor (require :lib.anchor))

(local game-modes (require :lib.game-modes.game-modes))

(local menu {})

(local (cw ch) (values 600 400))
(var ui-canvas (love.graphics.newCanvas cw ch))
(local quad (love.graphics.newQuad 0 0 100 100 cw ch))

(local title-font (love.graphics.newFont :assets/inconsolata.otf 30))
(local button-font (love.graphics.newFont :assets/inconsolata.otf 20))

(fn button-1 []
  (pp "Button 1 clicked")
  (game-modes.switch :test-canvas))

(fn button-2 []
  (pp "Button 2 clicked")
  (game-modes.switch :test))

(fn button [text ox callback x y lmb-triggered rmb-triggered]
  (fn draw [text ox h background-color text-color]
    (love.graphics.setColor background-color)
    (love.graphics.rectangle :fill ox 0 300 30 5)
    (love.graphics.setColor text-color)    
    (love.graphics.printf text ox 7 300 :center))
  (anchor.push-element 0 0 300 30) ;; element
  (if (anchor.over x y)
      (do
        (draw text 0 30 [1 1 0 1] [1 1 1 1])
        (anchor.pop)
        (if (or lmb-triggered rmb-triggered)
          (do (callback)          
              (values false false))
          (values lmb-triggered rmb-triggered)))
      (do (draw text 0 30 [1 0 0 1] [1 1 1 1])
          (anchor.pop)
          (values lmb-triggered rmb-triggered))))

(fn ui [x y lmb-in rmb-in lmb-t-in rmb-t-in]
  (var (lmb rmb rmb-t lmb-t) (values lmb-in rmb-in lmb-t-in rmb-t-in))
  (let [(w h) (love.window.getMode)
        gutter 15]
    (love.graphics.push :all)
    (love.graphics.setCanvas ui-canvas)
    (anchor.push-anchor 0 0 w h [0.5 0] [0.5 0.2])    
    (anchor.push-column 0 0 300 (math.floor (* 0.6 h)))
    (anchor.push-element 0 0 300 60)
    (love.graphics.setFont title-font)
    (love.graphics.setColor 1 1 1 1)    
    (love.graphics.printf :Menu 0 0 300 :center)
    (anchor.pop)
    (anchor.push-column 0 0 300 1000 :top gutter)
    ;; (anchor.push-row 0 0 150 1000 :top gutter)
    (love.graphics.setFont button-font)
    (->> (values rmb-t lmb-t)
         (button :Button1 0 button-1 x y)
         (button :Button2 0 button-2 x y)
         (button :Button3 0 button-2 x y)
         (button :Button4 0 button-2 x y))
    ;; (anchor.pop)
    (anchor.pop)
    (anchor.pop) 
    (anchor.pop)
    (love.graphics.pop)
    )  
  (values x y lmb rmb))

(var x 0)
(var y 0)
(var lmb-triggered 0)
(var rmb-triggered 0)
(var lmb false)
(var rmb false)

(fn menu.draw [state]
  (love.graphics.draw ui-canvas))

(fn menu.update [state dt]
  (local (next-lmb next-rmb) (ui x y lmb rmb lmb-triggered rmb-triggered))
  (set (lmb-triggered rmb-triggered) (values false false)))

(fn menu.mousemoved [state in-x in-y]
  (set x in-x)
  (set y in-y))

(fn menu.mousepressed [state in-x in-y button]
  (set (x y) (values in-x in-y))
  (match button
    1 (do (set lmb true) (set lmb-triggered true))
    2 (do (set rmb true) (set rmb-triggered true))))

(fn menu.mousereleased [state x y button]
    (match button
    1 (set lmb false)
    2 (set rmb false)))

(fn menu.resize [test w h]
  (set ui-canvas (love.graphics.newCanvas w h)))

menu
