love.conf = function(t)
   t.title, t.identity = "example", "example"
   t.modules.joystick = false
   t.modules.physics = false
   t.window.width = 600
   t.window.height = 400
   t.window.vsync = true
   t.window.resizable = true
   t.version = "11.3"
end
