(local flux (require :lib.third-party.flux))

(local menu {})

(fn menu.add-title [self text]
  (let [fg self.flux-group
        params self.params
        title {:text text
               :pos {:y 20 :x 0}
               :size {:w 600 :h 30}
               :align :center
               :text-colour params.text-colour
               :padding params.padding
               :alpha 0
               :period 1
               :target-alpha 1
               : fg
               :font params.title-font
               }
        tween (fg:to title.text-colour title.period {4 title.target-alpha})]
    (tween:delay 0.2)
    (table.insert self.title title)
    title)
  )

(fn menu.add-text [self text pparams]
  (let [fg self.flux-group
        params self.params
        t {:text text
               :pos {:y 20 :x 0}
               :size {:w 600 :h 30}
               :align (or pparams.text-align params.text-align :center)
               :text-colour params.text-colour
               :padding params.padding
               :alpha 0
               :period 1
               :target-alpha 1
               :font params.text-font
               }
        tween (fg:to t.text-colour t.period {4 t.target-alpha})]
    (tween:delay 0.2)
    (table.insert self.text t)
    t)
  )

(fn menu.add-author [self text link]
  (menu.add-link self text link {:pos self.params.author-pos
                                 :align :right}))

(fn menu.add-link [self text link pparams]
  (let [fg self.flux-group
        params self.params
        title {:text text
               :pos pparams.pos
               :size {:w (+ (* 2 params.padding) (params.link-font:getWidth text))
                      :h (+ (* 2 params.padding) (params.link-font:getHeight text))}
               :hover false
               :align (or pparams.align params.align :center)
               :hover-text-colour params.hover-text-colour
               :text-colour params.text-colour
               : link
               :callback (fn []
                           (print "openurl")
                           (love.system.openURL link))
               :padding params.padding
               :alpha 0
               :period 1
               :target-alpha 1
               : fg
               :font params.link-font
               }
        tween (fg:to title.text-colour title.period {4 title.target-alpha})]
    (tween:delay 0.2)
    (table.insert self.links title)
    title))

(fn add-button-tween [fg button]
  (var ret (let [tween (fg:to
               button.pos
               button.period
               button.target)]
             (: (tween:ease :quadinout) :delay button.delay)
             tween))
  (let [tween (fg:to
               button.text-colour
               button.period
               {4 1})]
    (: (tween:ease :linear) :delay button.delay))
  (let [tween (fg:to
               button.background-colour
               button.period
               {4 1})]
    (: (tween:ease :linear) :delay button.delay))
  ret)

(fn reset-button-tween [fg button]
  ;; (print (fennel.view button.tween-x))
  (fg:remove button.tween-x)
  (let [tween (fg:to
               button.pos
               button.period
               button.target)]
    (: (tween:ease :quadinout) :delay button.delay))
  )

(fn menu.add-button [self text callback]
  (fn null-fn [])
  (let [fg self.flux-group
        buttons self.buttons
        params self.params
        width 300
        height 50
        padding params.padding
        x 100
        y (match (?. buttons (# buttons) :pos :y) nil 120 a (+ a height padding))
        target-x (- 300 (/ width 2))
        target-y y
        delay (* (# buttons) 0.1)
        button {:pos {:x x :y y}
                :background-colour params.button-background-colour
                :hover-background-colour params.button-hover-background-colour
                :hover false
                :text-colour params.button-text-colour
                :padding padding
                :size {:w width :h height}
                :align :center
                :period 0.5
                :delay delay
                :target {:x target-x}
                :text text
                :callback (or callback null-fn)
                :font params.button-font}
        ]
    (table.insert buttons button)
    (set button.tween-x (add-button-tween fg button))
    button))

(fn draw-button [button]
  (if button.hover
      (love.graphics.setColor button.hover-background-colour)
      (love.graphics.setColor button.background-colour))
  (love.graphics.rectangle "fill" button.pos.x
                           button.pos.y
                           button.size.w
                           button.size.h
                           5)
  (love.graphics.setColor button.text-colour)
  (love.graphics.setFont button.font)
  (love.graphics.printf button.text
                        (+ button.pos.x button.padding)
                        (+ button.pos.y button.padding)
                        (- button.size.w (* 2 button.padding))
                        button.align))

(fn draw-text [text]
  (love.graphics.setColor text.text-colour)
  (love.graphics.setFont text.font)
  (love.graphics.printf text.text
                        (+ text.pos.x text.padding)
                        (+ text.pos.y text.padding)
                        (- text.size.w (* 2 text.padding))
                        text.align))

(fn draw-link [text]
  (if text.hover
      (love.graphics.setColor text.hover-text-colour)
      (love.graphics.setColor text.text-colour))
  (love.graphics.setFont text.font)
  (love.graphics.printf text.text
                        (+ text.pos.x text.padding)
                        (+ text.pos.y text.padding)
                        (- text.size.w (* 2 text.padding))
                        text.align))

(fn menu.draw [self]
  (each [_ button (ipairs self.buttons)]
    (draw-button button))
  (each [_ text (ipairs self.text)]
    (draw-text text))
  (each [_ title (ipairs self.title)]
    (draw-text title))
  (each [_ text (ipairs self.links)]
    (draw-link text)))

(fn hover [self]
  (let [(x y) (love.mouse.getPosition)]
    (each [_ ty (ipairs [self.buttons self.links])]
      (each [_ button (ipairs ty)]
        (let [bx button.pos.x
              by button.pos.y
              bx2 (+ bx button.size.w)
              by2 (+ by button.size.h)]
          (set button.hover (and (> x bx) (< x bx2) (> y by) (< y by2)))
          )))
    ))

(fn menu.click [self]
  (each [_ ty (ipairs [self.buttons self.links])]
    (each [_ button (ipairs ty)]
      (if button.hover
          (button.callback)))))

(fn normalize-button-size [self win]
  (local padding self.params.padding)
  (var change false)
  (var width 0)
  (var height 0)
  (each [_ button (ipairs self.buttons)]
    (match (button.font:getWidth button.text)
      (where w (> w width)) (set width w))
    (match (button.font:getHeight button.text)
      (where h (> h height)) (set height h)))
  (each [_ button (ipairs self.buttons)]
    (when (or (~= button.size.w (+ (* 2 padding) width))
              (~= button.size.h (+ (* 2 padding) height)))
      (print "change width")
      (set button.size.w (+ (* 2 padding) width))
      (set button.size.h (+ (* 2 padding) height))
      ))
  self)

(fn reset-button-position-y [self start]
  (local padding self.params.padding)
  (var y (+ start padding))
  (var change false)
  (each [_ button (ipairs self.buttons)]
    (set button.pos.y y)
    (set y (+ y button.size.h padding))
    )
  self)

(fn center-obj-pos [obj w]
  (local x obj.pos.x)
  (set obj.pos.x (math.floor (/ (- w obj.size.w) 2)))
  (~= obj.pos.x x))

(fn center-title [self w]
  (each [_ title (ipairs self.title)]
    (center-obj-pos title w))
  self)

(fn update-button-target-x [self w]
  (each [_ button (ipairs self.buttons)]
    (let [new-target (math.floor (/ (- w button.size.w) 2))]
      (when (~= button.target.x new-target)
        (print "update x")
        (set button.target.x new-target)
        (reset-button-tween self.flux-group button)))))

(fn menu.update [self dt]
  (let [(w h _flags) (love.window.getMode)]
    (local title (?. self.title 1))
    (normalize-button-size self w)
    (reset-button-position-y self
                             (if title
                                 (+
                                  (title.font:getHeight title.text) title.pos.y (* 2 self.params.padding))
                             (* 2 self.params.padding)))
    (update-button-target-x self w)
    (center-title self w)
    (set self.params.author-pos.y (- h 40))
    (set self.params.author-pos.x (- w 160)))
  (hover self)
  (self.flux-group:update dt))

(local menu-mt {:__index menu})

(fn menu.init [structure params]
  (let [defaults {:button-font (love.graphics.newFont :assets/inconsolata.otf 30)
                  :title-font (love.graphics.newFont :assets/inconsolata.otf 70)
                  :link-font (love.graphics.newFont :assets/inconsolata.otf 20)
                  :padding 10
                  :author-pos {:x 100 :y 100}
                  :text-colour [1 1 1 0]
                  :hover-text-colour [0 0 1 1]
                  :button-text-colour [1 1 1 0]
                  :button-background-colour [0.3 0.1 0.1 0]
                  :button-hover-background-colour [0.1 0.1 0.1 1]}]
    (each [key value (pairs defaults)]
      (when (. params key)
        (tset defaults key (. params key))))
    (local ret (setmetatable {:buttons []
                            :text []
                            :links []
                            :title []
                            :sub-title []
                            :flux-group (flux.group)
                            :params defaults} menu-mt))
    (when structure.title
      (ret:add-title structure.title))
    (when structure.author
      (ret:add-author (unpack structure.author)))
    (when structure.buttons
      (each [_ button (pairs structure.buttons)]
        (ret:add-button (unpack button))))
    ret
    ))

menu
