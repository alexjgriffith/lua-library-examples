(fn null-fn [] (print "click"))

(local menu-structure
       {:title "Paused"
        :text "Some text"
        :author ["@alexjgriffith" "https://alexjgriffith.itch.io"]
        :buttons [["First Button Really Long" null-fn]
                  ["Second Button" null-fn]
                  ["Third Button" null-fn]
                  ["Quit" (fn [] (love.event.quit))]]})

(local params {})

(local menu ((. (require :menu) :init) menu-structure params))

(fn love.draw []
  (menu:draw))

(fn love.update [dt]
  (menu:update dt))

(fn love.mousepressed [x y button])

(fn love.mousereleased [x y button]
  (menu:click))

(fn love.mousemoved [x y])

(fn love.resize [w h])

(fn love.keydown [key] (match key :escape (love.event.quit)))
