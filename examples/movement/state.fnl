{:player {:x 40 :y 370 :w 10 :h 20 :vx-max 300
          :vx 0 :vy 0
          :colour [0.3 0.5 0.2 1]
          :jump-count 0
          :dash-count 0
          :state :falling}
 :platforms [{:x 30 :y 370 :w 540 :h 20 :colour [0.9 0.2 0.2 1]}
             {:x 30 :y 10 :w 540 :h 20 :colour [0.9 0.2 0.2 1]}
             {:x 10 :y 10 :w 20 :h 380 :colour [0.9 0.2 0.2 1]}
             {:x 570 :y 10 :w 20 :h 380 :colour [0.9 0.2 0.2 1]}
             {:x 100 :y 300 :w 30 :h 10 :colour [0.9 0.2 0.2 1]}
             {:x 30 :y 70 :w 300 :h 10 :colour [0.9 0.2 0.2 1]}
             ]}
