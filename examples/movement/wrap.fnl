(local {:platformer {: calculate-velocity-gravity-t
                     : update-y-pos-vel
                     : update-x-pos-vel-simple}}
       (require :lib.movement.movement))

(local bump (require :lib.third-party.bump))
(local {: map} (require :lib.third-party.lume))

(local repl (require :lib.stdio.stdio))
(local state (require :state))

(fn add-obj-to-world [obj]
  (state.world:add obj obj.x obj.y obj.w obj.h))

(fn love.load []
  (tset state :world (bump.newWorld))
  (local (vy-jump g-jump)
         (calculate-velocity-gravity-t 200 0.5))
  (tset state.player :vy-jump vy-jump)
  (tset state.player :g-jump g-jump)
  (tset state.player :g-fall (* 2 g-jump))
  (tset state.player :g-release (* 4 g-jump))
  (pp state.player)
  (add-obj-to-world state.player)
  (map state.platforms add-obj-to-world)
  (repl.start :lib))

(fn draw-obj [obj]
  (love.graphics.push)
  (love.graphics.setColor obj.colour)
  (love.graphics.rectangle :fill obj.x obj.y obj.w obj.h)
  (love.graphics.pop))

(fn draw-player []
  (let [player state.player]
    (draw-obj player)
    (love.graphics.setColor 1 1 1 1)
    (love.graphics.printf player.state (- player.x 45) (- player.y 30) 100 :center)))

(fn draw-platforms []
  (each [_ platform (ipairs state.platforms)]
    (draw-obj platform))
  (love.graphics.setColor 1 1 1 1)
  (let [(items len) (state.world:getItems)]
    (each [_ item (ipairs items)]
      (love.graphics.rectangle :line item.x item.y item.w item.h))))

(fn love.draw []
  (draw-player)
  (draw-platforms))

(fn love.update [dt]
  (let [player state.player
        world state.world
        {: x : y : vy : g-jump : g-release : g-fall : vx-max} player
        (nx nvx) (if
                  (love.keyboard.isDown :a)
                  (update-x-pos-vel-simple x (if (= player.state :on-wall) (- vx-max) (- vx-max)) dt)
                  (love.keyboard.isDown :d)
                  (update-x-pos-vel-simple x (if (= player.state :on-wall) vx-max vx-max) dt)
                  (update-x-pos-vel-simple x 0 dt))
        (ny nvy) (update-y-pos-vel y vy (match player.state
                                          :jumping g-jump
                                          :release g-release
                                          :falling g-fall
                                          _ -1)
                                   dt)
        (ax ay col len) (world:move player nx ny)]
    (tset player :y ay)
    (tset player :x ax)
    (if (> player.vy 0) (tset player :state :falling))
    (tset player :vy (if (= ay ny) nvy 0))
    (tset player :vx (if (= ax nx) nvx 0))
    (if (> len 0)
      (let [normal (. col 1 :normal)]
        (match normal
          {:y -1} (do (set player.state :on-ground)
                      (set player.jump-count 0))
          {:x -1} (do (set player.state :on-wall)
                      (set player.vy 0)
                      )
          {:x 1} (do (set player.state :on-wall)
                     (set player.vy 0))))
      (= player.state :on-ground) (set player.state :falling)
      (= player.state :on-wall) (set player.state :falling)
      )))

(fn love.keyreleased [key code]
  (match key
    :space (when (= :jumping state.player.state)
              (tset state.player :state :release))))

(fn love.keypressed [key code]
  (match key
    :space (when (< state.player.jump-count 2)
             (tset state.player :jump-count (+ 1 state.player.jump-count))
             (tset state.player :state :jumping)
             (tset state.player :vy (- state.player.vy-jump)))
    :tab (when (< state.player.dash-count 1)
           (tset state.player :vx 1000)
           )
    :escape (love.event.quit)))
