(local shader "
extern number time;
vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 pixel_coords)
{
    vec4 pixel = Texel(texture, texture_coords);
    vec4 colouration = vec4  ((1.0+sin(time))/2.0, abs(cos(time)), abs(sin(time)), 1.0) / 2.0;
    if (pixel.a > 0)
      return pixel + colouration;
    else
      return pixel;
}
")

(local shimmer {})

(var state {})

(local tg-canvas (love.graphics.newCanvas 512 512))

(fn shimmer.draw [x y t]
  (state.effect:send :time (* t 5))
  (love.graphics.push "all")
  (love.graphics.setCanvas tg-canvas)
  (each [_index {:x tx :y ty} (ipairs state.grass-pos)]
    (love.graphics.draw state.grass-image tx ty))
  (love.graphics.pop)

  (love.graphics.push "all")
  (love.graphics.scale 2)
  (love.graphics.setCanvas)
  (love.graphics.draw state.bg-image 0 0)
  (love.graphics.setCanvas)
  (love.graphics.draw tg-canvas 0 0)
  (love.graphics.setShader state.effect)
  (state.cat-animation:draw state.cat-image x y)
  (love.graphics.pop))

(fn shimmer.init [state-in]
  (set state state-in)
  (set state.effect (love.graphics.newShader shader)))

shimmer
