(local repl (require :lib.stdio.stdio))
(local anim8 (require :lib.third-party.anim8))

(local shimmer (require :shimmer))

(local tall-grass (require :tall-grass))

(var x 10)
(var y 10)
(macro incf [x by?] `(set ,x (+ ,x (or ,by? 1))))
(var t 0)

(local draw-state {:value :shimmer})

(var animation nil)

(fn love.load []
  (love.graphics.setDefaultFilter "nearest" "nearest")
  (let [bg-image (love.graphics.newImage "assets/background-512x512.png")
        grass-image (love.graphics.newImage "assets/tall-grass-64x64.png")
        grass-pos [{:x 30 :y 30} {:x 130 :y 93}]
        cat-image (love.graphics.newImage "assets/cats.png")
        g (anim8.newGrid 48 48 (cat-image:getWidth) (cat-image:getHeight))
        cat-animation (anim8.newAnimation (g "1-3" 1) 0.1)
        state {: cat-image : cat-animation
               : grass-image : grass-pos
               : bg-image}]
    (shimmer.init state)
    (tall-grass.init state)
    (set animation cat-animation)
    (pp animation)
    )
  (repl.start :lib/stdio))

(fn love.draw []
  ;; (love.graphics.setShader)
  (match draw-state.value
    :shimmer (shimmer.draw x y t)
    :tall-grass (tall-grass.draw x y t)
    )
  )

(fn love.update [dt]
  (set t (+ t dt))
  (when (love.keyboard.isDown "up") (incf y -1))
  (when (love.keyboard.isDown "down") (incf y 1))
  (when (love.keyboard.isDown "left") (incf x -1))
  (when (love.keyboard.isDown "right") (incf x 1))
  (set x (math.min (math.max x -16) (- 300 32)))
  (set y (math.min (math.max y -16) (- 200 32)))
  (animation:update dt))

(fn love.keypressed [key code]
  (match key
    "escape" (love.event.quit)
    :1 (set draw-state.value :shimmer)
    :2 (set draw-state.value :tall-grass)))
