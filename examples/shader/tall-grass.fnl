(local shader "
extern Image grass;
extern vec2 screen_size;
extern number qx, qy, qw, qh, iw, ih;
extern number y_origin;

// https://love2d.org/wiki/love.graphics.newShader
// Image = Sampler2D
// Texel(tex, uv) = tecture(tex,uv)

// color <- love.graphicsd.setColor
// texture <- texture of image currently being drawn
// texture_coords <- location inside the texture 0 0 <- top left
// screen_coords <- locatoin being drawn to 0.5,0.5 <- top left
vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords)
{
  // Get the RGBA pixel value at texture_coords
  vec4 pixel = Texel(texture, texture_coords);

  // Get the pixel value on the grass canvas at screen coordinates
  // Normalized by screen size to be between 0-1
  // 0 - 1 on screen / grass canvas
  vec4 gr = Texel(grass, screen_coords / screen_size);

  // texture_coords 0 - 1 ->
  // brought into pixel space
  // shifted to the start of the quad and
  // normalized by quad width result: 0 - 1 from left to right of quad!
  // 0 - 1 on quad
  vec2 quad_coords = vec2((texture_coords.x * iw - qx) / qw,
                          (texture_coords.y * ih - qy) / qh);

  // why not just texutre_coords.x?
  // 0 - 1 on image
  vec2 quad_central_origin = vec2((qx + quad_coords.x * qw) / iw, // left
                                  (qy + y_origin ) / ih); // bottom

  // 0 - 1 on image - 0 - 1 on image

  vec2 texture_to_origin = quad_central_origin - texture_coords;

  vec2 to_origin_pixel = vec2(texture_to_origin.x * iw,
                              texture_to_origin.y * ih);

  vec2 origin_pixel = screen_coords + to_origin_pixel;
  vec2 origin_pixel_coords = vec2(origin_pixel.x / screen_size.x,
                                  origin_pixel.y / screen_size.y);
  vec2 origin = vec2((texture_coords.x * iw - qx) / qw, (texture_coords.y * ih - qy) / qh);

  if (Texel(grass, origin_pixel_coords).a > 0) {
     if (gr.a > 0 && quad_coords.y > 0.65 && quad_coords.x >= 0) {
     pixel = vec4(1.0, 0.0, 0.0, 1.0);
     }
   }
   return pixel * color;

}
")

(local tall-grass {})

(var state {})

(local tg-canvas (love.graphics.newCanvas 512 512))

(fn tall-grass.draw [x y t]
  (love.graphics.push "all")
  (love.graphics.setCanvas tg-canvas)
  (love.graphics.scale 2)
  (each [_index {:x tx :y ty} (ipairs state.grass-pos)]
    (love.graphics.draw state.grass-image tx ty))
  (love.graphics.pop)

  (let [quad (. state.cat-animation.frames state.cat-animation.position)
        (qx qy qw qh) (quad:getViewport)
        iw (state.cat-image:getWidth)
        ih (state.cat-image:getHeight)]
    (state.effect-tg:send "grass" tg-canvas)
    (state.effect-tg:send "screen_size" [600 400])
    (state.effect-tg:send "qx" qx)
    (state.effect-tg:send "qy" qy)
    (state.effect-tg:send "qw" qw)
    (state.effect-tg:send "qh" qh)
    (state.effect-tg:send "iw" iw)
    (state.effect-tg:send "ih" ih)
    (state.effect-tg:send "y_origin" -32)
    )

  (love.graphics.push "all")
  (love.graphics.scale 2)
  (love.graphics.setCanvas)
  (love.graphics.draw state.bg-image 0 0)
  (love.graphics.pop)
  (love.graphics.push "all")
  (love.graphics.setCanvas)
  (love.graphics.draw tg-canvas 0 0)
  (love.graphics.pop)
  (love.graphics.push "all")
  (love.graphics.scale 2)
  (love.graphics.setCanvas)

  (love.graphics.setShader state.effect-tg)


  (love.graphics.draw state.cat-image (. state.cat-animation.frames state.cat-animation.position) x y)
  (love.graphics.pop))

(fn tall-grass.init [state-in]
  (set state state-in)
  (set state.effect-tg (love.graphics.newShader shader)))

tall-grass
