(local sample-mode {})

(fn sample-mode.init [mode]
  (pp "init sample mode"))

(fn sample-mode.draw [mode]
  (love.graphics.rectangle :fill 0 0 20 20))

(fn sample-mode.update [mode dt])

(fn sample-mode.keypressed [mode key]
  (match key
    :escape (love.event.quit)))

sample-mode
