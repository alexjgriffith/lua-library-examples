(local repl (require :lib.stdio.stdio))

(local game-modes (require :lib.game-modes.game-modes))

(fn love.load []
  (game-modes.switch :sample-mode)
  (game-modes.push :sample-mode)
  (game-modes.pop :sample-mode)
  (repl.start :lib/stdio))
