(love.graphics.setDefaultFilter "nearest" "nearest")

(local repl (require :lib.stdio.stdio))

(local scale 2)



(local canvas (love.graphics.newCanvas 1000 1000))


(local anim8 (require :lib.third-party.anim8))

(local lg love.graphics)

(local grass (love.graphics.newCanvas 600 400))

(var x 100)
(var y 100)

(local grass-info {:x 20  :y 20 :w 200 :h 200})

(macro incf [x by?] `(set ,x (+ ,x (or ,by? 1))))

(var t 0)

(fn love.load []
  (global image (love.graphics.newImage "assets/cats.png"))
  (local g (anim8.newGrid 48 48 (image:getWidth) (image:getHeight)))
  (global animation {:down (anim8.newAnimation (g "1-3" 1) 0.1)
                     :left   (anim8.newAnimation (g "1-3" 2) 0.1)
                     :right   (anim8.newAnimation (g "1-3" 3) 0.1)
                     :up   (anim8.newAnimation (g "1-3" 4) 0.1)
                     })
  (tset animation :current animation.down)

  (global cats [{:x 10 :y 10 :animation {:current (anim8.newAnimation (g "1-3" 1) 0.1)}}])
  (repl.start :lib/stdio))

(var camera-x (+ 150 -24 (- x)))
(var camera-y (+ 100 -24 (- y)))

(var target-x (+ 150 -24 (- x)))
(var target-y (+ 100 -24 (- y)))

(fn lerp [from to max]
  (let [increasing (< from to)
        dist (if increasing
                 (math.min to (+ from max))
                 (math.max to (- from max)))]
    dist))

(fn update-camera [dt]
  (set target-x (+ 150 -24 (- x)))
  (set target-y (+ 100 -24 (- y)))
  (set camera-x (lerp camera-x target-x (* 1.7 dt (math.max 30 (math.abs (- camera-x target-x))))))
  (set camera-y (lerp camera-y target-y (* 1.7 dt (math.max 30 (math.abs (- camera-y target-y))))))
  )

(local mask_shader (love.graphics.newShader "
   vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords) {
      if (Texel(texture, texture_coords).a == 0) {
         // a discarded pixel wont be applied as the stencil.
         discard;
      }
      return vec4(1.0);
   }
"))


(fn myStencilFunctionGrass []
  (love.graphics.rectangle :fill grass-info.x grass-info.y grass-info.w grass-info.h)
  (love.graphics.rectangle :fill (+ 20 grass-info.w grass-info.x) grass-info.y grass-info.w grass-info.h))

(fn myStencilFunctionCats []
  (when (< y (+ grass-info.y grass-info.h -48))
    (love.graphics.rectangle :fill x (+ 36 y) 48 36))
  (each [_ cat (ipairs cats)]
    (love.graphics.rectangle :fill cat.x (+ 36 cat.y) 48 36))
  )

(fn myStencilFunctionCatTops []
  ;; (love.graphics.push)
  (love.graphics.setShader mask_shader)
  (when (< y (+ grass-info.y grass-info.h -48))
    (print x)
    (love.graphics.setScissor x y 48 36)
    (animation.current:draw image x y))
  (each [_ {:animation ganimation :x gx :y gy} (ipairs cats)]
    (love.graphics.setScissor gx gy 48 36)
    (ganimation.current:draw image gx gy))
  (love.graphics.setShader)
  ;;(love.graphics.pop)
  (love.graphics.setScissor)
  )

;; (fn draw-cat []
;;   (love.graphics.setStencilTest "less" 2)
;;   (animation.current:draw image x y)
;;   (love.graphics.setColor 0 0 0 1)
;;   (love.graphics.setStencilTest "equal" 2)
;;   (love.graphics.line (+ x 8) (+ y 36) (+ x 40) (+ y 36))
;; )

(fn love.draw []
  (love.graphics.push)
  ;; (love.graphics.setCanvas {1 canvas :stencil true})
  (love.graphics.stencil myStencilFunctionCats "replace" 1 false)
  (love.graphics.stencil myStencilFunctionCatTops "replace" 0 true)
  (love.graphics.stencil myStencilFunctionGrass "increment" 1 true)
  (love.graphics.setColor 0.3 0.2 0.1 1)
  (love.graphics.rectangle :fill 0 0 600 400)
  (love.graphics.setColor 0 1 0 1)
  (love.graphics.rectangle :fill grass-info.x grass-info.y grass-info.w grass-info.h)
  (love.graphics.rectangle :fill (+ 20 grass-info.w grass-info.x) grass-info.y grass-info.w grass-info.h)
  (love.graphics.setColor 0 0.7 0.1 1)
  (love.graphics.rectangle :fill grass-info.x (+ grass-info.h grass-info.y -12) grass-info.w 12)
  (love.graphics.rectangle :fill (+ 20 grass-info.w grass-info.x) (+ grass-info.h grass-info.y -12) grass-info.w 12)
  (love.graphics.setColor 1 1 1 1)

  ;; (draw-cat)
  (each [_ {:x gx :y gy :animation ganimation} (ipairs cats)]
    (love.graphics.setStencilTest "less" 2)
    (love.graphics.setColor 1 1 1 1)
    (ganimation.current:draw image gx gy)
    (love.graphics.setColor 0 0 0 1)
    (love.graphics.setStencilTest "equal" 2)
    (love.graphics.line (+ gx 8) (+ gy 36) (+ gx 40) (+ gy 36))
    )

  (love.graphics.setColor 1 1 1 1)
  (love.graphics.setStencilTest "less" 2)
  (animation.current:draw image x y)
  (love.graphics.setColor 0 0 0 1)
  (love.graphics.setStencilTest "equal" 2)
  (love.graphics.line (+ x 8) (+ y 36) (+ x 40) (+ y 36))
  (love.graphics.pop)
  (when false
  (love.graphics.setStencilTest "equal" 0)
  (love.graphics.setColor 0 0 0 1)
  (love.graphics.rectangle :fill 0 0 600 400)

  (love.graphics.setStencilTest "equal" 1)
  (love.graphics.setColor 1 0 0 1)
  (love.graphics.rectangle :fill 0 0 600 400)

  (love.graphics.setStencilTest "equal" 2)
  (love.graphics.setColor 1 1 0 1)
  (love.graphics.rectangle :fill 0 0 600 400)
  )
  ;;(love.graphics.rectangle :line x (+ 36 y) 48 36))
  (love.graphics.setStencilTest)

  )

(fn love.update [dt]
  (update-camera dt)
  (set t (+ t dt))
  (var move false)
  (when (love.keyboard.isDown "up") (set move true)
        (tset animation :current animation.up)
        (incf y -1))
  (when (love.keyboard.isDown "down")
    (set move true)
    (tset animation :current animation.down)
    (incf y 1))
  (when (love.keyboard.isDown "left")
    (tset animation :current animation.left)
    (set move true) (incf x -1))
  (when (love.keyboard.isDown "right")
    (tset animation :current animation.right)
    (set move true) (incf x 1))
  (if move
      (animation.current:update dt)
      (animation.current:gotoFrame 2)
      )
  (each [_ {:x gx :y gy :animation ganimation} (ipairs cats)]
    (ganimation.current:gotoFrame 2))
  )

(fn love.keypressed [key code]
  ;; (love.event.quit)
  )
