(local repl (require :lib.stdio.stdio))
(local gifcat (require :gifcat))

(var curgif nil)

(fn love.load []
  (love.filesystem.setIdentity "gifcat-test")
  (gifcat.init)  
  (repl.start :lib/stdio))

(fn love.draw [])

(fn love.update [dt]
  (gifcat.update))

(fn love.quit []
  (gifcat.close))

(fn love.threaderror [_thread errorstr]
  (pp (.. "Thread error!\n" errorstr)))

(fn love.keypressed [key code repeat]
  (when (not repeat)
    (set curgif (gifcat.newGif (.. (os.time) "-new.gif") 100 100))
    (curgif:onUpdate
     (fn [gif curframes totalframes]
       (pp (string.format "Progress: %.2f%% (%d/%d)"
                          (* (gif:progress) 10)
                          curframes
                          totalframes))))
    (curgif:onFinish
     (fn [gif totalframes]
       (pp (.. totalframes " frames written"))))    ))

(fn love.keyreleased [key]
  (curgif:close)
  (set curgif nil))

(fn love.draw []
  (love.graphics.print "GIFCAT" 50 50 (* 3 (love.timer.getTime)))
  (when curgif
    (love.graphics.captureScreenshot (fn [x] (curgif:frame x)))
    (love.graphics.setColor 1 0 0 1)
    (love.graphics.circle :fill (- 600 10) 10 10))
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.print (.. "Current FPS: " (tostring (love.timer.getFPS))) 0 0))
