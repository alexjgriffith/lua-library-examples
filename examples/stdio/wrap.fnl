(local repl (require :lib.stdio.stdio))

(fn love.load []
  (repl.start :lib/stdio))

(fn love.draw [])

(fn love.update [dt])

(fn love.keypressed [key code]
  (love.event.quit))
