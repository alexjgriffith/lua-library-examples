(local  state-machine (require :lib.state-machine.state-machine))

(local {:platformer {: calculate-velocity-gravity-t
                     : update-y-pos-vel
                     : update-x-pos-vel-simple}}
       (require :lib.movement.movement))

(local bump (require :lib.third-party.bump))
(local {: map} (require :lib.third-party.lume))

(local repl (require :lib.stdio.stdio))
(local state (require :state))

(fn add-obj-to-world [obj]
  (state.world:add obj obj.x obj.y obj.w obj.h))

(fn move [dt player g]
  (var move-lr false)
  (let [player state.player
        world state.world
        {: x : y : vy : vx-max} player
        (nx nvx) (if
                  (love.keyboard.isDown :a)
                  (update-x-pos-vel-simple x (- vx-max) dt)
                  (love.keyboard.isDown :d)
                   (update-x-pos-vel-simple x (+ vx-max) dt)
                  (update-x-pos-vel-simple x 0 dt))
        (ny nvy) (update-y-pos-vel y vy g dt)
        (ax ay col len) (world:move player nx ny)]
    (tset player :y ay)
    (tset player :x ax)
    (tset player :vy (if (= ay ny) nvy 0))
    (tset player :vx (if (= ax nx) nvx 0))
    ;; (when (> len 0) (pp (. col 1 :normal :y)))
    (values
     (and (> len 0) (= -1 (. col 1 :normal :y))) ;; on ground
     (or (~= x ax) (~= y ay)) ;; moving lr
     )
    ))

(fn states [vy g player]
  [{:name :idle
    :lr false
    :on-ground true
    :jumps 0
    :enter (fn [state]
             (tset state :lr false)
             (tset state :on-ground true))
    :update (fn [state dt ...]
              (let [(on-ground lr) (move dt player -100)]
                (tset state :on-ground on-ground)
                (tset state :lr lr))
              (player.state:handle-input "update"))
    :handle-input
    (fn [state input]
      (match input
        "space_down" (values :replace-state :jump)
        "update"
          (match [state.lr state.on-ground]
              [false true] nil
              [true true] (values :replace-state :walk)
              _ (values :replace-state :falling))))
    }

   {:name :walk
    :update (fn [state dt ...]
              (let [(on-ground lr) (move dt player -100)]
                (tset state :on-ground on-ground)
                (tset state :lr lr))
              (player.state:handle-input "update"))
    :lr true
    :on-ground true
    :jumps 0
    :enter (fn [state]
             (tset state :lr true))
    :handle-input
    (fn [state input]
      (match input
        "space_down" (values :replace-state :jump)
        "update" (match [state.lr state.on-ground]
                   [false true] (values :replace-state :idle)
                   [true true] nil
                   _ (values :replace-state :falling))))
    }

   {:name :jump
    :vy-jump vy
    :jumps 0
    :g g
    :on-ground false
    :enter
    (fn [state previous]
      (pp ["previous: " previous])
      (tset state :jumps (or previous.jumps 0))
      (tset player :vy (- state.vy-jump))
      (tset state :on-ground false))
    :exit
    (fn [state]
      (tset state :jumps (+ state.jumps 1)))
    :update
    (fn [state dt ...]
      (tset state :on-ground (move dt player state.g))
      (player.state:handle-input "update"))
    :handle-input
    (fn [state input]
      (match input
        "space_up" (values :replace-state :release)
        "s_down" (values :replace-state :dive)
        "update" (if state.on-ground
                     (values :replace-state :idle)
                     (> player.vy 0)
                     (values :replace-state :falling)
                     )
        ))
    }

   {:name :release
    :g (* g 4)
    :jumps 0
    :on-ground false
    :enter
    (fn [state previous]
      (tset state :jumps (or previous.jumps 0))
      (tset state :on-ground false))
    :update
    (fn [state dt ...]
      (local (on-ground lr) (move dt player state.g))
      (tset state :on-ground on-ground)
      (player.state:handle-input "update"))
    :handle-input
    (fn [state input]
      (match [input state.on-ground (> player.vy 0)]
        ["space_down"]  (when (< state.jumps 2)
                          (values :replace-state :jump))
        ["update" true]  (values :replace-state :idle)
        ["s_down"] (values :replace-state :dive)
        ["update" _ true] (values :replace-state :falling)
        ))
    }

   {:name :falling
    :g (* g 2)
    :on-ground false
    :enter (fn [state previous]
             (tset state :jumps (or previous.jumps 0))
             (tset state :on-ground false))
    :update
    (fn [state dt ...]
      (tset state :on-ground (move dt player state.g))
      (player.state:handle-input "update"))
    :handle-input
    (fn [state input]
      (match input
        "space_down" (when (< state.jumps 2)
                       (values :replace-state :jump))
        "s_down" (values :replace-state :dive)
        "update" (when state.on-ground
                   (values :replace-state :idle))
        ))
    }

   {:name :dive
    :g (* g 10)
    :on-ground false
    :enter (fn [state previous]
             (tset state :jumps (or previous.jumps 0))
             (tset state :on-ground false))
    :update
    (fn [state dt ...]
      (tset state :on-ground (move dt player state.g))
      (player.state:handle-input "update"))
    :handle-input
    (fn [state input]
      (match input
        "space_down" (when (< state.jumps 2)
                       (values :replace-state :jump))
        "update" (when state.on-ground
                   (values :replace-state :idle))
        ))
    }
   ])

(fn love.load []
  (tset state :world (bump.newWorld))
  (local (vy-jump g-jump)
         (calculate-velocity-gravity-t 200 0.5))
  (tset state.player :state
        (state-machine.init (unpack (states vy-jump g-jump state.player))))
  (pp state.player)
  (add-obj-to-world state.player)
  (map state.platforms add-obj-to-world)
  (repl.start :lib/stdio))

(fn draw-obj [obj]
  (love.graphics.push)
  (love.graphics.setColor obj.colour)
  (love.graphics.rectangle :fill obj.x obj.y obj.w obj.h)
  (love.graphics.pop))

(fn draw-player []
  (let [player state.player]
    (draw-obj player)
    (love.graphics.setColor 1 1 1 1)
    (love.graphics.printf (player.state:get-state-name) (- player.x 45) (- player.y 30) 100 :center)

    (love.graphics.printf (. player.state.states :jump :jumps) (- player.x 45) (- player.y 50) 100 :center)))

(fn draw-platforms []
  (each [_ platform (ipairs state.platforms)]
    (draw-obj platform))
  (love.graphics.setColor 1 1 1 1)
  (let [(items len) (state.world:getItems)]
    (each [_ item (ipairs items)]
      (love.graphics.rectangle :line item.x item.y item.w item.h))))

(fn love.draw []
  (draw-player)
  (draw-platforms))

(fn love.update [dt]
  (state.player.state:update dt))

(fn love.keyreleased [key code]
  (state.player.state:handle-input (.. key "_up")))

(fn love.keypressed [key code]
  (match key
    :escape (love.event.quit)
    _ (state.player.state:handle-input (.. key "_down"))))
