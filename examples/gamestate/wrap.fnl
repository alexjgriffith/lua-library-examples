;; https://github.com/vrld/hump/blob/master/gamestate.lua
(local gamestate (require :lib.third-party.gamestate))

;; GS.new GS.switch GS.push GS.pop GS.current
;; GS.registerEvents

(local flux (require :lib.third-party.flux))

(fn transition [next-state period colour ...]
  (fn switch [to ...]

    )
  (local transition-state
         {:timer 0
          :period period
          :state :exiting
          :enter (fn [GS previous-GS]
                   (tset GS :previous-state previous-GS)
                   (tset GS :previous-draw previous-GS.draw)
                   (tset GS :previous-update previous-GS.update)
                   (tset GS :next-update next-state.update)
                   (tset GS :next-draw next-state.draw)
                   (tset GS :next-enter next-state.enter)
                   (tset next-state :enter nil))
          :update (fn [GS dt]
                    (tset GS :timer (+ GS.timer dt))
                    (match GS.state
                      :exiting (do (GS.previous-update GS.previous-state dt)
                                   (when (> GS.timer GS.period)
                                     (tset GS :state :entering)
                                   ))
                      :entering (do (GS.next-update GS.next-state dt)
                                    )
                      ))
          }))

(local other-state {:enter (fn [GS previous-GS ...] (print (fennel.view previous-GS)))
                     :draw (fn [GS]  (love.graphics.print "State 2" 100 100))
                    :update (fn [GS dt] )
                    :keypressed (fn [GS previous? key code]
                                  (match key
                                    "return" (print (fennel.view [GS previous? key code]))
                                    ))
                    :name :other-state
                     }
       )

(local sample-state {:enter (fn [GS previous? var1] (print (fennel.view previous?)))
                     :draw (fn [from]
                             (love.graphics.print "State 1" 100 100)
                             (love.graphics.rectangle "fill" 0 0 10 10))
                     :update (fn [from dt] )
                     :keypressed (fn [from previous? key code]
                                   (match key
                                     "return" (transition other-state 1 [0.5 0.5 0.5 1])
                                     ))
                     :name :sample-state
                     }
       )



(fn love.load []
  (gamestate.registerEvents)
  (gamestate.switch sample-state)
  )
