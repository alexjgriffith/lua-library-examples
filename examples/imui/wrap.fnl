(local repl (require :lib.stdio.stdio))

(global lume (require :lib.third-party.lume))

(local err {:state nil
            :font (love.graphics.newFont :assets/inconsolata.otf 16)})

(fn safely-require [filename]
  (match (xpcall #(do (pp filename)
                      (require filename))
                 #(err.enter filename $ (fennel.traceback)))
    (true t) t
    (false err) nil))


(fn err.draw []
  (local explanation "Press escape to quit.
Press space to return to the previous mode after reloading in the repl.")
  (love.graphics.push "all")
  (love.graphics.setFont err.font)
  (love.graphics.clear 0.1 0.1 0.1)
  (love.graphics.setColor 0.9 0.9 0.9)
  (love.graphics.print (.. err.state.msg "\n\n" explanation "\n" err.state.traceback) 10 10)
  (love.graphics.pop))

(fn err.keypressed [mode key set-mode]
  (match key
    :escape (love.event.quit)
    :space (do
             (lume.hotswap err.state.old-mode)
             (lume.hotswap :wrap))))

(fn err.enter [old-mode msg traceback]
  (print msg)
  (print traceback)
  (tset err :state {: old-mode : msg : traceback})
  (set love.draw err.draw)
  (set love.keypressed err.keypressed)
  )

(local game-modes (safely-require :lib.game-modes.game-modes))

(when game-modes (game-modes.switch :mode-ui-test-clean))

(fn love.load [] (repl.start :lib/stdio))
