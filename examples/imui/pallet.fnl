(local pallet {:turquoise "#1abc9c"
               :greensea "#16a085"
               :amethyst "#9b59b6"
               :wisteria "#8e44ad"
               :sunflower "#f1c40f"
               :orange "#f39c12"
               :charcoal "#36454F"
               :rasinblack "#242124"})

pallet
