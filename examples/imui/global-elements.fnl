(local text ["DIV" {:anchor :top-left
                    :alignment :top-left
                    :text-alignment :left
                    :padding [10 10 10 10]
                    :width 280
                    :height 280
                    :background false
                    :text-color "#FF0000FF"}

             "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce facilisis nunc ut dui tempus, at porta nunc efficitur. Quisque fermentum dignissim congue. Morbi efficitur mattis convallis. Nulla ullamcorper, lorem quis pellentesque facilisis, dolor sem consectetur urna, aliquam ultrices libero ligula in dui. Sed justo nulla, viverra quis vulputate eu, cursus vel odio. Nullam nec volutpat tellus. Sed molestie enim ut ligula imperdiet pharetra. Quisque semper mi dui, in mattis dolor vestibulum id. Nam interdum imperdiet arcu, eu congue dolor. Vestibulum hendrerit, justo sit amet euismod egestas, tortor odio porttitor risus, vel pretium erat libero placerat magna. Sed tempor porttitor metus eget maximus.

Etiam luctus dui arcu, id pretium quam egestas et. In placerat orci eget risus dictum hendrerit. Mauris sed euismod nibh, sodales interdum dolor. Ut ornare efficitur tellus, a lobortis tortor facilisis at. Sed quis suscipit mauris. Nullam mattis, ipsum eu commodo convallis, urna est imperdiet nisi, vitae suscipit elit lacus a nunc. Ut maximus pharetra porttitor. Nam eget placerat nibh. Donec mauris massa, suscipit ut ipsum ac, interdum posuere nibh. Phasellus eget nisi in tortor dignissim tincidunt. Quisque eget varius velit. Sed quis blandit leo, in pharetra odio. Maecenas feugiat iaculis sagittis. Nulla turpis elit, hendrerit ut varius in, hendrerit vitae lorem. Fusce tempus dui nec ullamcorper gravida.

Praesent ullamcorper tristique tempus. Proin elementum sapien est, non aliquet sem feugiat quis. Integer laoreet nisl vitae dignissim sagittis. Fusce auctor eu urna vitae pulvinar. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Praesent maximus ipsum nec est tempor consectetur. Integer et ante sit amet orci pharetra gravida. Mauris eget est imperdiet diam imperdiet tristique nec eu nisi. Curabitur id sapien porttitor, dignissim turpis nec, posuere sem. Morbi dignissim porttitor tellus, in accumsan tortor hendrerit eget. Donec ac elit est. Mauris tempus nisl eget dictum efficitur. Ut dolor augue, tempor eu augue ut, pellentesque tempor velit.

Nulla luctus in mi vel porta. Maecenas et imperdiet ante. Morbi consectetur luctus egestas. Suspendisse augue massa, ultricies vel nunc id, laoreet laoreet lacus. Mauris sodales risus quis posuere dapibus. Pellentesque ac maximus elit. Aliquam et sem id nulla rhoncus viverra ac vel urna. Sed dapibus libero sed velit convallis pharetra. Donec ut convallis orci. Quisque volutpat dolor urna, ut volutpat nibh sodales nec.

Morbi pharetra interdum quam quis porta. Nulla sed nisi sed eros eleifend suscipit. Aliquam luctus lacinia condimentum. Nulla fermentum metus vitae nulla bibendum lacinia. Sed porttitor elit ut enim faucibus, quis tempor massa luctus. Cras in aliquet magna, non sodales eros. Nulla blandit volutpat lectus, ac mattis nunc varius pharetra. Donec ut quam consequat ante tincidunt sagittis vitae vitae augue. "])

(global left-aligned
        ["CONTAINER" {:width 300
                      :height 300
                      :anchor :center
                      :alignment :center}
        text
        ])
