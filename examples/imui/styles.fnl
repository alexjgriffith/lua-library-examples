(local pallet (require :pallet))

{
 :CONTAINER
 {:color pallet.charcoal
  :radius 5}

 :ROW
 {:arrangement :row
  :gutter [4 0]
  :inherits :CONTAINER}

 :COL
 {:arrangement :column
  :gutter [0 4]
  :inherits :CONTAINER}

 :COLCLEAR
 {:background false
  :inherits :COL}

 :TEXT
 {:width 50 :height 16 :alignment :right
  :background false
  :text-alignment :right
  :padding [2 2 2 2]}

 :TEXTBOX
 {:padding [5 5 5 5]
  :text-alignment :center
  :margin [0 0 0 0]
  :width 20
  :height 20
  :radius 3
  :color pallet.turquoise
  :color-hover [1 1 1 1]
  :shadow true
  :shadow-color pallet.rasinblack
  :shadow-color-hover [1 1 1 0]
  :shadow-dx -3
  :shadow-dy 5
  :catch-click true
  :on-click :textbox-click
  :on-focus :textbox-focus
  }}
