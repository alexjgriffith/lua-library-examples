(local test (lume.clone (require :interface-defaults)))

(local fml-imui (require :lib.fml-imui.fml-imui))

(var dom nil)

(global data {})

(local my-styles (require :styles))

(local hud (require :hud-elements))

(var ui-canvas (love.graphics.newCanvas 600 400))

(local alphabet ["a" "b" "c" "d" "e" "f" "g" "h" "i" "j" "k" "l" "m"
                 "n" "o" "p" "q" "r" "s" "t" "u" "v" "w" "x" "y" "z"])

(require :global-elements)

(var current-index {:button-a 1
                    :button-b 2
                    :button-c 3
                    :button-d 4
                    :button-e 5
                    :button-f 6
                    :button-g 7
                    :button-h 8
                    :button-i 9
                    })

(var command-stack {})

(var menu-clicked false)

(fn test.init [test]
  (fml-imui.set-canvas ui-canvas)
  (fml-imui.add-image :assets/icon-64.png)
  (fml-imui.add-click-callback
   :textbox-click
   (fn [element button x y]
     (when element.id
       (tset current-index element.id
             (if (<= (+ (. current-index element.id) 1) 26)
                 (+ (. current-index element.id) 1)
                 1))
       (tset command-stack element.id
             ["text" (. alphabet (. current-index element.id))])
       ;; (pp command-stack)
       )
     (pp (.. "CLICK!(" (or element.id "NA") ") x:" x ", y:" y)))
   )

  (fml-imui.add-click-callback
   :menu-click
   (fn [element button x y]
     (when (not menu-clicked)
       (set menu-clicked true)
       (data.dom:add-element _G.left-aligned 1 (require :styles))
       )
     (pp (.. "CLICK!(" (or element.id "NA") ") x:" x ", y:" y)))
   )

  (fml-imui.add-custom-render
   :custom-render
   (fn [x y _w _h]
     (local canvas (love.graphics.getCanvas))
     (local w (canvas:getWidth))
     (local h (canvas:getHeight))
     (love.graphics.rectangle :fill x y w h)))

  (fml-imui.add-focus-callback
   :textbox-focus (fn [element]))

  (set dom (fml-imui.ml-to-dom hud 600 400 my-styles))
  (tset data :dom dom)
  (love.window.setTitle "UI TEST")
  (let [(x y flags) (love.window.getMode)]
    (tset flags :resizable true)
    (love.window.setMode x y flags)
    )
  )

(fn test.draw [test]
  (love.graphics.draw ui-canvas))

(fn test.mousepressed [test x y button ]
  (fml-imui.mousepressed-update button x y))

(fn test.released [test x y button ]
  (fml-imui.mousereleased-update button x y))

(fn test.resize [test w h]
  (set ui-canvas (love.graphics.newCanvas w h))
  (dom:resize-screen ui-canvas w h))

(fn test.update [test dt]
  (dom:update command-stack))

test
