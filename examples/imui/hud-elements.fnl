(local pallet (require :pallet))

[
 ;; ["DIV" {:anchor :top-left
 ;;         :alignment :top-left
 ;;         :color [1 1 1 1]
 ;;         :width 600
 ;;         :height 400
 ;;         :custom-render :custom-render
 ;;         }]

 ["ROW"
  {:anchor :bottom
   :alignment :bottom
   :width 300 :height 40
   :padding [5 10 10 10]
   :y 5}
  ["TEXTBOX" {:value "A" :id :button-a} "A"]
  ["TEXTBOX" {:id :button-b} "B"]
  ["TEXTBOX" {:id :button-c} "C"]
  ["TEXTBOX" {:id :button-d} "D"]
  ["TEXTBOX" {:id :button-e} "E"]
  ["TEXTBOX" {:id :button-f} "F"]
  ["TEXTBOX" {:id :button-g} "G"]
  ["TEXTBOX" {:id :button-h} "H"]
  ["TEXTBOX" {:id :button-i} "I"]]

 ["CONTAINER" {:anchor :top-left
               :alignment :top-left
               :width 170 :height 60
               :padding [5 10 5 10]
               :margin [5 5 5 5]}
  ["ROW"
   {:width 170 :height 60
    :color pallet.turquoise}
   ["COLCLEAR"
    {:width 50
     :height 40
     :background false
     :padding [2 2 2 10]}
    ["TEXT" {} "HEALTH"]
    ["TEXT" {} "MANA"]]
   ["COLCLEAR"
    {:width 100
     :height 40}
    ["BAR" {:width 100 :height 20
            ;; :margin [2 2 2 2]
            :x -12
            :radius 5
            :y -4
            :shadow true
            :shadow-color pallet.blackraisin
            :shadow-dx -3
            :shadow-dy 5
            :color pallet.orange
            :alignment :left}]
    ["BAR" {:width 100 :height 16
            :x -12
            :y 0
            :radius 5
            :color pallet.orange
            :shadow true
            :shadow-dx -3
            :shadow-dy 5
            :shadow-color pallet.blackraisin
            :margin [2 2 2 2]
            :alignment :left}
     ]]]]

 ["CONTAINER"
  {:alignment :top-right
   :anchor :top-right
   :padding [10 10 10 10]
   :margin [5 5 5 5]
   :width 100
   :height 40
   :radius 5
   :color pallet.charcoal
   :text-alignment :center
   }
  ["BUTTON" {:radius 3
             :color pallet.turquoise
             :color-hover [1 1 1 1]
             :shadow-color-hover [1 1 1 0]
             :shadow true
             :shadow-dx -3
             :shadow-dy 5
             :width 100
             :height 40
             :arrangement :stacked
             :id :button-menu
             :on-click :menu-click
             }
   ["TEXT" {:background false
            :colour pallet.orange
            :height 15
            :width 100
            :anchor :center
            :text-alignment :center
            :alignment :center}
    "MENU"]
   ]]
 ["CONTAINER"
  {
   :padding [10 10 5 10]
   :height 80
   :width 64
   :anchor :center
   :alignment :center
   :arrangement :column
   :gutter [0 2]}
  ["IMAGE" {:background false
            :alignment :center
            :radius 15
            :height 64
            :width 64
            :image true
            :image-quad [0 0 64 64]
            :image-src "assets/icon-64.png"
            :image-type :rounded-quad
            :shadow true
            :shadow-color pallet.blackraisin
            :shadow-dx -3
            :shadow-dy 5
            }]
  ["DIV" {:alignment :center
          :text-alignment :center
          :background false
          :shadow true
          :shadow-color pallet.blackraisin
          :shadow-dx 0
          :shadow-dy 7
          :radius 30
          :width 64
          :x 0
          :text-color pallet.turquoise
          :height 10} "Wheel"]
  ]
 ]
