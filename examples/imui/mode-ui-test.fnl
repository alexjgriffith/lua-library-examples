(local test (lume.clone (require :interface-defaults)))

(local bump (require :lib.third-party.bump))

(local fml-imui (require :fml-imui))

(var dom nil)

(local sample-ml-2
       [["TEXTBOX" {:anchor :center
                    :padding [5 5 10 5]
                    :width 100
                    :height 100}
         ["9PATCH" {:image :image-string :quad [0 0 10 10]
                    :width 70
                    :height 70}
          ["TEXT" {:width 100 :height 20} "Text" "More Text" "Even MORE!"]
          ["TEXT" {:width 100 :height 30} "Text2"]]]
        ["TEXT" {:width 100 :height 20
                 :anchor :bottom} "Text3"]
        ])

(local pallet {:turquoise "#1abc9c"
               :greensea "#16a085"
               :amethyst "#9b59b6"
               :wisteria "#8e44ad"
               :sunflower "#f1c40f"
               :orange "#f39c12"
               :charcoal "#36454F"
               :rasinblack "#242124"})

(local my-styles {:TESTBOX {:padding [30 30 30 30]
                            :margin [30 30 30 30]
                            :width 100 :height 15
                            :text-alignment :center
                            :alignment :center}
                  :TESTBOXSMALL {:padding [10 10 10 10]
                                 :margin [10 10 10 10]
                                 :width 20 :height 20
                                 :text-alignment :center
                                 }
                  :TEXTBOX {:padding [5 5 5 5]
                            :text-alignment :center
                            :margin [0 0 0 0]
                            :width 20
                            :height 20
                            :radius 3
                            :color pallet.turquoise
                            :shadow true
                            :shadow-color pallet.rasinblack
                            :shadow-dx -3
                            :shadow-dy 5
                            }
                  }
       )


(local sample-ml-4 [["HOTBAR" {:anchor :bottom
                               :alignment :bottom
                               :width 300 :height 40
                               :padding [5 10 10 10]
                               :y 5
                               :arrangement :row
                               :gutter [4 0]
                               :color pallet.charcoal
                               :shadow true
                               :shadow-color pallet.rasinblack
                               :radius 5
                               }
                     ["TEXTBOX" {} "A"]
                     ["TEXTBOX" {} "B"]
                     ["TEXTBOX" {} "C"]
                     ["TEXTBOX" {} "D"]
                     ["TEXTBOX" {} "E"]
                     ["TEXTBOX" {} "F"]
                     ["TEXTBOX" {} "G"]
                     ["TEXTBOX" {} "H"]
                     ["TEXTBOX" {} "G"]]
                    ["BARS" {:anchor :top-left
                             :alignment :top-left
                             :x 0
                             :y 0
                             :width 170
                             :height 60
                             :color pallet.charcoal
                             :radius 5
                             :padding [5 10 5 10]
                             :margin [5 5 5 5]
                             }
                     [ "CONTAINER"
                      {:width 170
                       :height 60
                       :arrangement :row
                       :gutter [4 0]
                       :color pallet.turquoise
                       :radius 5}
                      ["CONTAINER"
                      {:width 50
                       :height 40
                       :color pallet.turquoise
                       :radius 5
                       :background false
                       :padding [2 2 2 10]
                       :arrangement :column
                       :gutter [0 4]}
                      ["TEXT" {:width 50 :height 16 :alignment :right
                               :background false
                               :text-alignment :right
                               :padding [2 2 2 2]}
                       "HEALTH"]
                      ["TEXT" {:width 50 :height 16 :alignment :right
                               :background false
                               :text-alignment :right
                               :padding [2 2 2 2]}
                       "MANA"
                       ]]
                     ["CONTAINER"
                      {:width 100
                       :height 40
                       :background false
                       :arrangement :column
                       :gutter [0 4]}
                      ["BAR" {:width 100 :height 20
                              :margin [2 2 2 2]
                              :x -12
                              :radius 5
                              :y -4
                              :shadow true
                              :shadow-color pallet.blackraisin
                              :shadow-dx -3
                              :shadow-dy 5
                              :color pallet.orange
                               :alignment :left}]
                      ["BAR" {:width 100 :height 16
                              :x -12
                              :y 0
                              :radius 5
                              :color pallet.orange
                              :shadow true
                              :shadow-dx -3
                              :shadow-dy 5
                              :shadow-color pallet.blackraisin
                              :margin [2 2 2 2]
                              :alignment :left}
                       ]]]]
                     ["CONTAINER"
                      {:alignment :top-right
                       :anchor :top-right
                       :padding [10 10 10 10]
                       :margin [5 5 5 5]
                       :width 100
                       :height 40
                       :radius 5
                       :color pallet.charcoal
                       :text-alignment :center
                       }
                      ["BUTTON" {:radius 3
                                 :color pallet.turquoise
                                 :shadow true
                                 :shadow-dx -3
                                 :shadow-dy 5
                                 :width 100
                                 :height 40
                                 :arrangement :stacked
                               }
                       ["TEXT" {:background false
                                :colour pallet.orange
                                :height 15
                                :width 100
                                :anchor :center
                                :text-alignment :center
                                :alignment :center}
                        "MENU"]
                       ]]

                    ])

(local sample-ml-3 [["CONTAINER" {:anchor :top-left
                                  :alignment :top-left
                                  :width 300 :height 80
                                  :padding [10 10 10 10]
                                  :margin [20 20 20 20]
                                  :arrangement :row
                                  :gutter [20 0]}
                     ["TEXTBOX" {:alignment :top} "A"]
                     ["TEXTBOX" {:alignment :bottom} "B"]
                     ["TEXTBOX" {:alignment :center} "C"]
                     ["TEXTBOX" {} "D"]
                     ["TEXTBOX" {} "E"]
                     ["TEXTBOX" {} "F"]
                     ["TEXTBOX" {} "G"]
                     ["TEXTBOX" {} "H"]
                     ["TEXTBOX" {} "I"]
                     ]

                    ["CONTAINER" {:anchor :top-right
                                  :alignment :top-right
                                  :width 80 :height 300
                                  :padding [10 10 10 10]
                                  :margin [20 20 20 20]
                                  :arrangement :column
                                  :gutter [20 20]}
                     ["TEXTBOX" {} "A"]
                     ["TEXTBOX" {:alignment :left} "B"]
                     ["TEXTBOX" {:alignment :right} "C"]
                     ["TEXTBOX" {} "D"]
                     ["TEXTBOX" {} "E"]
                     ["TEXTBOX" {} "F"]
                     ["TEXTBOX" {} "G"]
                     ["TEXTBOX" {} "H"]
                     ["TEXTBOX" {} "I"]
                     ]

                    ["CONTAINER" {:anchor :bottom-left
                                  :alignment :bottom-left
                                  :x 0
                                  :y 0
                                  :width 300 :height 220
                                  :padding [10 10 10 10]
                                  :margin [10 10 10 10]
                                  :arrangement :stacked}
                    ["CONTAINER" {:anchor :center
                                  :alignment :center
                                  :x 0
                                  :y 0
                                  :width 180 :height 120
                                  :padding [10 10 10 10]
                                  :margin [20 20 20 20]
                                  :arrangement :grid
                                  :gutter [20 20]}
                     ["TEXTBOX" {} "A"]
                     ["TEXTBOX" {:alignment :left} "B"]
                     ["TEXTBOX" {:alignment :right} "C"]
                     ["TEXTBOX" {} "D"]
                     ["TEXTBOX" {} "E"]
                     ["TEXTBOX" {} "F"]
                     ["TEXTBOX" {} "G"]
                     ["TEXTBOX" {} "H"]
                     ["TEXTBOX" {} "I"]
                     ]
                    ]]

       )

(local sample-ml-1
       [["TESTBOX" {:anchor :top-right}
         "TOP RIGHT"]
        ["TESTBOX" {:anchor :top-left}
         "TOP LEFT"]
        ["TESTBOX" {:anchor :bottom-right}
         "BOTTOM RIGHT"]
        ["TESTBOX" {:anchor :bottom-left}
         "BOTTOM LEFT"]
        ["TESTBOX" {:anchor :left}
         "LEFT"]
        ["TESTBOX" {:anchor :right}
         "LEFT"]
        ["TESTBOX" {:anchor :bottom}
         "BOTTOM"]
        ["TESTBOX" {:anchor :top}
         "TOP"]
        ["TESTBOX" {:anchor :center
                    :width 250
                    :height 300
                    }
         ["TESTBOXSMALL" {:anchor :center
                          :alignment :center
                          }
          ["TESTBOXSMALL" {:width 5
                           :height 5
                           :anchor :center
                          :alignment :center
                          }
          ]
          ]
         ["TESTBOXSMALL" {:anchor :top-left
                          :alignment :top-left
                          }]
         ["TESTBOXSMALL" {:anchor :top-right
                          :alignment :top-right
                          }]
         ["TESTBOXSMALL" {:anchor :top :alignment :top}]
         ["TESTBOXSMALL" {:anchor :left :alignment :left}]
         ["TESTBOXSMALL" {:anchor :right :alignment :right}]
         ["TESTBOXSMALL" {:anchor :bottom :alignment :bottom}]
         ["TESTBOXSMALL" {:anchor :bottom-right :alignment :bottom-right}]
         ["TESTBOXSMALL" {:anchor :bottom-left :alignment :bottom-left}]
         "CENTER"]
        ]
       )

(fn test.init [test]
  (set dom (fml-imui.ml-to-dom sample-ml-4 600 400 my-styles
                               ))
  (love.window.setTitle "UI TEST"))

(local {: render-callbacks : mouse-callback}
       (require :fml-love-callbacks))

(fn test.draw [test]
  (dom:draw
   render-callbacks
   mouse-callback)
  )
(fn test.update [test dt])

test
