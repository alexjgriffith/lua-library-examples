(fn pp [x] (print (fennel.view x)))
;;(pp fennel.eval)
(local repl (require :lib.stdio.stdio))

(local state (require state))

(local HC (require :lib.third-party.HC))

(fn generateWorld []
  (tset state :rect (HC.rectangle 200, 400, 400, 20))
  (tset state :mouse (HC.circle 400 300 20))
  (mouse:moveTo (love.mousegetPosition)))


(local do-once-hash {})
(fn do-once [function]
  (when (not (. do-once-hash function))
    (tset do-once-hash function (function))))


(fn love.load []
  (generateWorld)
  (repl.start :lib/stdio))

(fn love.draw [])

(fn love.update [dt]
  )

(fn love.keypressed [key code]
  (love.event.quit))
