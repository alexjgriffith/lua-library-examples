(local repl (require :lib.stdio.stdio))

(fn love.draw []
  (love.graphics.print "Map Lib"))

(fn love.load []
  (repl.start :lib/stdio))

(fn love.update [])

(fn love.keydown [key]
  (love.event.quit))
