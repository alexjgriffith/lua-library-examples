(local bump (require :lib.third-party.bump))

(local state {:text-1 "" :text-2 "" :x 0 :y 0})

(local (_width _height _flags) (love.window.getMode))

(local height (- _height 30 64))

(local colour-background [1 1 1 1])

(local colour-1 [1 0 0 1])

(local colour-2 [0 0 1 1])

(local colour-text [0 0 0 1])

(local width (- _width 64))

(fn love.load []
  (love.graphics.setBackgroundColor colour-background)
  (tset state :world-1 (bump.newWorld))
  (tset state :world-2 (bump.newWorld))
  (tset state :boxes [])
  (tset state :objects [])
  (for [i 1 20]
    (table.insert state.boxes {:x (* width (math.random))
                               :y (* height (math.random))
                               :w 64 :h 64})

    (table.insert state.objects {:x (* width (math.random))
                                 :y (* height (math.random))
                                 :w 64 :h 64}))
  (each [_ {: x : y : w : h &as obj} (ipairs state.boxes)]
    (state.world-1:add obj x y w h))
  (each [_ {: x : y : w : h &as obj} (ipairs state.objects)]
    (state.world-2:add obj x y w h)))

(fn love.draw []
  (love.graphics.setColor colour-text)
  (love.graphics.printf "Multiple Bump Worlds test" 0 (-  _height 20) width :left)
  (love.graphics.printf (.. state.text-1 " " state.text-2) 0 (-  _height 20) width :right)
  (each [i {: x : y : w : h : hover &as obj} (ipairs (state.world-1:getItems))]
    (love.graphics.setColor colour-1)
    (love.graphics.printf (.. "1," i) x (+ y 0) 64 :left)
    (when hover (love.graphics.setColor colour-text)
          (tset obj :hover nil))
    (love.graphics.rectangle "line" x y w h))
  (each [i {: x : y : w : h : hover &as obj} (ipairs (state.world-2:getItems))]
    (love.graphics.setColor colour-2)
    (love.graphics.printf (.. "2," i) x (+ y 0) 64 :left)
    (when hover (love.graphics.setColor colour-text)
          (tset obj :hover nil))
    (love.graphics.rectangle "line" x y w h))
  (love.graphics.setColor colour-text)
  (love.graphics.circle "line" (- state.x 0) (- state.y 0) 10)
  )

(fn love.update [dt]
  (local (x y) (love.mouse.getPosition))
  (tset state :x x)
  (tset state :y y)
  (each [key value (pairs (state.world-1:queryPoint x y))]
    (tset value :hover true)
    )
  (each [key value (pairs (state.world-2:queryPoint x y))]
    (tset value :hover true)
    )
  )

(fn love.mousepressed [x y button]
  (local (items-1 len-1) (state.world-1:queryPoint x y))
  (local (items-2 len-2) (state.world-2:queryPoint x y))
  (when (> len-1 0) (tset state :text-1 "World 1 collision! "))
  (when (> len-2 0) (tset state :text-2 "World 2 collision! ")))

(fn love.mousemoved [x y]
  (when (love.mouse.isDown 1 2 3)
    (love.mousereleased)
    (love.mousepressed x y))
)

(fn love.mousereleased []
  (tset state :text-1 "")
  (tset state :text-2 ""))

(fn love.keydown [key]
  (love.event.quit))
